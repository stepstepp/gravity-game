using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerEffectManager : MonoBehaviour
{
    public GameObject LandEffect;
    public GameObject windTrails;
    public GameObject jetPropulsion;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void PlayLandEffect()
    {
        GameObject effect = Instantiate(LandEffect, new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z),
            transform.rotation);

        Destroy(effect, 2);
    }

    public void ToggleShootingEffects(bool isShooting)
    {
        windTrails.SetActive(isShooting);
        jetPropulsion.SetActive(isShooting);
    }


}
