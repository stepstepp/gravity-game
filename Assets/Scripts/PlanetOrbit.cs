using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetOrbit : MonoBehaviour
{
    Vector3 OriginPoint;
    public float orbitSpeed = 20f;
    // Start is called before the first frame update
    void Start()
    {
        OriginPoint = transform.parent.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.RotateAround(OriginPoint, Vector3.up, orbitSpeed * Time.deltaTime);
    }
}
