﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};
template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.IntPtr[]
struct IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// AnimationManager
struct AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.Animator
struct Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// CameraManager
struct CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A;
// CameraMovement
struct CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// Character_control
struct Character_control_tEB187B80DE857A5EE58CD99757188637F71C8EF2;
// ChargeManager
struct ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0;
// UnityEngine.Collider
struct Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// GravityOrbit
struct GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2;
// System.Collections.IDictionary
struct IDictionary_t99871C56B8EC2452AC5C4CF3831695E617B89D3A;
// System.Collections.IEnumerator
struct IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// InputManager
struct InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A;
// LevelManager
struct LevelManager_t010B312A2B35B45291F58195216ABB5673174961;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// System.NotSupportedException
struct NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// PlanetOrbit
struct PlanetOrbit_tDB68D6FC93B55A78AC923A046D6E61E722E2542A;
// PlayerEffectManager
struct PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D;
// PlayerLocomotion
struct PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795;
// PlayerManager
struct PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// ResetBool
struct ResetBool_t4D03DEE6EEDE94D9EA59BF9BA8FE231B8BF18894;
// ResetIsJumping
struct ResetIsJumping_t41CFA6F9D05CFC07776121B3C64616CBF95F9D59;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// Rotatator
struct Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// SliderScripts
struct SliderScripts_t01CD33323C1390AABFEA3597355DD017703D404F;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.StateMachineBehaviour
struct StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F;
// System.String
struct String_t;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;
// PlayerLocomotion/<ShootTowards>d__37
struct U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F;
// UnityEngine.UI.Slider/SliderEvent
struct SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780;

IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0;
IL2CPP_EXTERN_C String_t* _stringLiteral1963A02C9DEBD760AC7D980330C0CC094F7B3211;
IL2CPP_EXTERN_C String_t* _stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A;
IL2CPP_EXTERN_C String_t* _stringLiteral440FACB401E8D64062B80C3C27A6A7C79F132229;
IL2CPP_EXTERN_C String_t* _stringLiteral4FA73C839BE532F9F38159AA62F306192A226448;
IL2CPP_EXTERN_C String_t* _stringLiteral54ADB4B256E220AC1660EEB0B81E40D259DD1203;
IL2CPP_EXTERN_C String_t* _stringLiteral62012883D3A13EAB8473757C089740CE05DD45CC;
IL2CPP_EXTERN_C String_t* _stringLiteral6C23924C4F221C5F81332E79B4BD5A5AF61B9AF7;
IL2CPP_EXTERN_C String_t* _stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C;
IL2CPP_EXTERN_C String_t* _stringLiteral7598EFBF4A029BF0B3B286271D8C3B865F7A66FA;
IL2CPP_EXTERN_C String_t* _stringLiteral781843FF3C5C4AE705E16F522171558E9CB73B4C;
IL2CPP_EXTERN_C String_t* _stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E;
IL2CPP_EXTERN_C String_t* _stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7;
IL2CPP_EXTERN_C String_t* _stringLiteral99654BA65919C1D4F705ADE00AD912C711685A9F;
IL2CPP_EXTERN_C String_t* _stringLiteral9C39D1EEC1890B28711700FDEE2AC9BED5AB7172;
IL2CPP_EXTERN_C String_t* _stringLiteral9E4CF29136E2EF978D3759CB6AAA6935843FC102;
IL2CPP_EXTERN_C String_t* _stringLiteralA6A16FA6FFFE7CF75CED271F9B06FD471503AB8C;
IL2CPP_EXTERN_C String_t* _stringLiteralB1915CC31B6BC62A90E9FD4F876A00AA4B73C4ED;
IL2CPP_EXTERN_C String_t* _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70;
IL2CPP_EXTERN_C String_t* _stringLiteralD139F540024B68AE2F2EECE8C05093886A02BAB5;
IL2CPP_EXTERN_C String_t* _stringLiteralDF953B1BAAE56A3E1998B6A1DC98AC472A4DAF75;
IL2CPP_EXTERN_C String_t* _stringLiteralE16EFE13C5C08096A869677E0912595D5D6C1C03;
IL2CPP_EXTERN_C String_t* _stringLiteralFBC1FBDF3F91C0637B6624C6C526B3718C7E46A2;
IL2CPP_EXTERN_C String_t* _stringLiteralFC6687DC37346CD2569888E29764F727FAF530E0;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m67C66D5AA4AA16A3448B21AF2EC03FFEAD4F03B9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponentInChildren_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m654193278BB56948AB820313E9D35E6D6A89798F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m76E4330A66259F770A0F73017C967C9EFAFE9993_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0_m14E499548B982EF24D66C845F258B527D0319A41_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisGravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2_m7275FAA26EDDE37A27FC6C9AB643DF22D79B8789_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m6CCCFD6651416AD5F042C75E8B8CF01C82631CA9_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D_mE88BF86CEFAE820AB756B8FACF1C9CF3E50142AA_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m58B42920D67789AB6096BAB8D957FB64B4DDAA16_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisCameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_m1BDBA1D013C9E2C431C864109ADEA485E4716CF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m49DE97E09E1BC5C2FF6DCD755C601B4B730E2B42_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_FindObjectOfType_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m14B304841D56F2822E0E096CB90F01FAFEFC2877_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CShootTowardsU3Ed__37_System_Collections_IEnumerator_Reset_mD79DF504A1584917D6E9E7BF7988BCC1FE6670E8_RuntimeMethod_var;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object

struct Il2CppArrayBounds;

// System.Array


// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct  ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.YieldInstruction
struct  YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.YieldInstruction
struct YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
};

// UnityEngine.AnimatorStateInfo
struct  AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA 
{
public:
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Name
	int32_t ___m_Name_0;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Path
	int32_t ___m_Path_1;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_FullPath
	int32_t ___m_FullPath_2;
	// System.Single UnityEngine.AnimatorStateInfo::m_NormalizedTime
	float ___m_NormalizedTime_3;
	// System.Single UnityEngine.AnimatorStateInfo::m_Length
	float ___m_Length_4;
	// System.Single UnityEngine.AnimatorStateInfo::m_Speed
	float ___m_Speed_5;
	// System.Single UnityEngine.AnimatorStateInfo::m_SpeedMultiplier
	float ___m_SpeedMultiplier_6;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Tag
	int32_t ___m_Tag_7;
	// System.Int32 UnityEngine.AnimatorStateInfo::m_Loop
	int32_t ___m_Loop_8;

public:
	inline static int32_t get_offset_of_m_Name_0() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Name_0)); }
	inline int32_t get_m_Name_0() const { return ___m_Name_0; }
	inline int32_t* get_address_of_m_Name_0() { return &___m_Name_0; }
	inline void set_m_Name_0(int32_t value)
	{
		___m_Name_0 = value;
	}

	inline static int32_t get_offset_of_m_Path_1() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Path_1)); }
	inline int32_t get_m_Path_1() const { return ___m_Path_1; }
	inline int32_t* get_address_of_m_Path_1() { return &___m_Path_1; }
	inline void set_m_Path_1(int32_t value)
	{
		___m_Path_1 = value;
	}

	inline static int32_t get_offset_of_m_FullPath_2() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_FullPath_2)); }
	inline int32_t get_m_FullPath_2() const { return ___m_FullPath_2; }
	inline int32_t* get_address_of_m_FullPath_2() { return &___m_FullPath_2; }
	inline void set_m_FullPath_2(int32_t value)
	{
		___m_FullPath_2 = value;
	}

	inline static int32_t get_offset_of_m_NormalizedTime_3() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_NormalizedTime_3)); }
	inline float get_m_NormalizedTime_3() const { return ___m_NormalizedTime_3; }
	inline float* get_address_of_m_NormalizedTime_3() { return &___m_NormalizedTime_3; }
	inline void set_m_NormalizedTime_3(float value)
	{
		___m_NormalizedTime_3 = value;
	}

	inline static int32_t get_offset_of_m_Length_4() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Length_4)); }
	inline float get_m_Length_4() const { return ___m_Length_4; }
	inline float* get_address_of_m_Length_4() { return &___m_Length_4; }
	inline void set_m_Length_4(float value)
	{
		___m_Length_4 = value;
	}

	inline static int32_t get_offset_of_m_Speed_5() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Speed_5)); }
	inline float get_m_Speed_5() const { return ___m_Speed_5; }
	inline float* get_address_of_m_Speed_5() { return &___m_Speed_5; }
	inline void set_m_Speed_5(float value)
	{
		___m_Speed_5 = value;
	}

	inline static int32_t get_offset_of_m_SpeedMultiplier_6() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_SpeedMultiplier_6)); }
	inline float get_m_SpeedMultiplier_6() const { return ___m_SpeedMultiplier_6; }
	inline float* get_address_of_m_SpeedMultiplier_6() { return &___m_SpeedMultiplier_6; }
	inline void set_m_SpeedMultiplier_6(float value)
	{
		___m_SpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_m_Tag_7() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Tag_7)); }
	inline int32_t get_m_Tag_7() const { return ___m_Tag_7; }
	inline int32_t* get_address_of_m_Tag_7() { return &___m_Tag_7; }
	inline void set_m_Tag_7(int32_t value)
	{
		___m_Tag_7 = value;
	}

	inline static int32_t get_offset_of_m_Loop_8() { return static_cast<int32_t>(offsetof(AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA, ___m_Loop_8)); }
	inline int32_t get_m_Loop_8() const { return ___m_Loop_8; }
	inline int32_t* get_address_of_m_Loop_8() { return &___m_Loop_8; }
	inline void set_m_Loop_8(int32_t value)
	{
		___m_Loop_8 = value;
	}
};


// System.Boolean
struct  Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct  Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// UnityEngine.DrivenRectTransformTracker
struct  DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 
{
public:
	union
	{
		struct
		{
		};
		uint8_t DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2__padding[1];
	};

public:
};


// System.Enum
struct  Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct  Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.LayerMask
struct  LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};


// UnityEngine.Quaternion
struct  Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct  Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct  SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector2
struct  Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct  Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct  Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct  Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;

public:
	inline static int32_t get_offset_of_m_Seconds_0() { return static_cast<int32_t>(offsetof(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013, ___m_Seconds_0)); }
	inline float get_m_Seconds_0() const { return ___m_Seconds_0; }
	inline float* get_address_of_m_Seconds_0() { return &___m_Seconds_0; }
	inline void set_m_Seconds_0(float value)
	{
		___m_Seconds_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	float ___m_Seconds_0;
};
// Native definition for COM marshalling of UnityEngine.WaitForSeconds
struct WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	float ___m_Seconds_0;
};

// UnityEngine.UI.ColorBlock
struct  ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Coroutine
struct  Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7  : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF
{
public:
	// System.IntPtr UnityEngine.Coroutine::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_pinvoke : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_pinvoke
{
	intptr_t ___m_Ptr_0;
};
// Native definition for COM marshalling of UnityEngine.Coroutine
struct Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7_marshaled_com : public YieldInstruction_tB0B4E05316710E51ECCC1E57174C27FE6DEBBEAF_marshaled_com
{
	intptr_t ___m_Ptr_0;
};

// UnityEngine.CursorLockMode
struct  CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04 
{
public:
	// System.Int32 UnityEngine.CursorLockMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CursorLockMode_t247B41EE9632E4AD759EDADDB351AE0075162D04, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t27FC72B0409D75AAF33EC42498E8094E95FEE9A6* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_tDE44F029589A028F8A3053C5C06153FAB4AAE29F * ____safeSerializationManager_13;
	StackTraceU5BU5D_t4AD999C288CB6D1F38A299D12B1598D606588971* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// UnityEngine.KeyCode
struct  KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_t1D303F7D061BF4429872E9F109ADDBCB431671F4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct  Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.RaycastHit
struct  RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Point_0)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Normal_1)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_UV_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};


// UnityEngine.Space
struct  Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct  FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct  Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct  Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// PlayerLocomotion/<ShootTowards>d__37
struct  U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F  : public RuntimeObject
{
public:
	// System.Int32 PlayerLocomotion/<ShootTowards>d__37::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object PlayerLocomotion/<ShootTowards>d__37::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// PlayerLocomotion PlayerLocomotion/<ShootTowards>d__37::<>4__this
	PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * ___U3CU3E4__this_2;
	// UnityEngine.Vector3 PlayerLocomotion/<ShootTowards>d__37::point
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point_3;
	// UnityEngine.Vector3 PlayerLocomotion/<ShootTowards>d__37::<direction>5__2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___U3CdirectionU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F, ___U3CU3E4__this_2)); }
	inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_point_3() { return static_cast<int32_t>(offsetof(U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F, ___point_3)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_point_3() const { return ___point_3; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_point_3() { return &___point_3; }
	inline void set_point_3(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___point_3 = value;
	}

	inline static int32_t get_offset_of_U3CdirectionU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F, ___U3CdirectionU3E5__2_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_U3CdirectionU3E5__2_4() const { return ___U3CdirectionU3E5__2_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_U3CdirectionU3E5__2_4() { return &___U3CdirectionU3E5__2_4; }
	inline void set_U3CdirectionU3E5__2_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___U3CdirectionU3E5__2_4 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct  Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Slider/Direction
struct  Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961 
{
public:
	// System.Int32 UnityEngine.UI.Slider/Direction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Direction_tFC329DCFF9844C052301C90100CA0F5FA9C65961, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct  Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.UI.Navigation
struct  Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.ScriptableObject
struct  ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// System.SystemException
struct  SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62  : public Exception_t
{
public:

public:
};


// UnityEngine.Behaviour
struct  Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Collider
struct  Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// System.NotSupportedException
struct  NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339  : public SystemException_tC551B4D6EE3772B5F32C71EE8C719F4B43ECCC62
{
public:

public:
};


// UnityEngine.Rigidbody
struct  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.StateMachineBehaviour
struct  StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:

public:
};


// UnityEngine.Transform
struct  Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Animator
struct  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// UnityEngine.Camera
struct  Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// ResetBool
struct  ResetBool_t4D03DEE6EEDE94D9EA59BF9BA8FE231B8BF18894  : public StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F
{
public:
	// System.String ResetBool::isInteractingBool
	String_t* ___isInteractingBool_4;
	// System.Boolean ResetBool::isInteractingStatus
	bool ___isInteractingStatus_5;
	// System.String ResetBool::isUsingRootMotionBool
	String_t* ___isUsingRootMotionBool_6;
	// System.Boolean ResetBool::isUsingRootMotionStatus
	bool ___isUsingRootMotionStatus_7;

public:
	inline static int32_t get_offset_of_isInteractingBool_4() { return static_cast<int32_t>(offsetof(ResetBool_t4D03DEE6EEDE94D9EA59BF9BA8FE231B8BF18894, ___isInteractingBool_4)); }
	inline String_t* get_isInteractingBool_4() const { return ___isInteractingBool_4; }
	inline String_t** get_address_of_isInteractingBool_4() { return &___isInteractingBool_4; }
	inline void set_isInteractingBool_4(String_t* value)
	{
		___isInteractingBool_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isInteractingBool_4), (void*)value);
	}

	inline static int32_t get_offset_of_isInteractingStatus_5() { return static_cast<int32_t>(offsetof(ResetBool_t4D03DEE6EEDE94D9EA59BF9BA8FE231B8BF18894, ___isInteractingStatus_5)); }
	inline bool get_isInteractingStatus_5() const { return ___isInteractingStatus_5; }
	inline bool* get_address_of_isInteractingStatus_5() { return &___isInteractingStatus_5; }
	inline void set_isInteractingStatus_5(bool value)
	{
		___isInteractingStatus_5 = value;
	}

	inline static int32_t get_offset_of_isUsingRootMotionBool_6() { return static_cast<int32_t>(offsetof(ResetBool_t4D03DEE6EEDE94D9EA59BF9BA8FE231B8BF18894, ___isUsingRootMotionBool_6)); }
	inline String_t* get_isUsingRootMotionBool_6() const { return ___isUsingRootMotionBool_6; }
	inline String_t** get_address_of_isUsingRootMotionBool_6() { return &___isUsingRootMotionBool_6; }
	inline void set_isUsingRootMotionBool_6(String_t* value)
	{
		___isUsingRootMotionBool_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___isUsingRootMotionBool_6), (void*)value);
	}

	inline static int32_t get_offset_of_isUsingRootMotionStatus_7() { return static_cast<int32_t>(offsetof(ResetBool_t4D03DEE6EEDE94D9EA59BF9BA8FE231B8BF18894, ___isUsingRootMotionStatus_7)); }
	inline bool get_isUsingRootMotionStatus_7() const { return ___isUsingRootMotionStatus_7; }
	inline bool* get_address_of_isUsingRootMotionStatus_7() { return &___isUsingRootMotionStatus_7; }
	inline void set_isUsingRootMotionStatus_7(bool value)
	{
		___isUsingRootMotionStatus_7 = value;
	}
};


// ResetIsJumping
struct  ResetIsJumping_t41CFA6F9D05CFC07776121B3C64616CBF95F9D59  : public StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F
{
public:

public:
};


// AnimationManager
struct  AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Animator AnimationManager::animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator_4;
	// PlayerManager AnimationManager::playerManager
	PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * ___playerManager_5;
	// PlayerLocomotion AnimationManager::playerLocomotion
	PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * ___playerLocomotion_6;
	// System.Int32 AnimationManager::horizontal
	int32_t ___horizontal_7;
	// System.Int32 AnimationManager::vertical
	int32_t ___vertical_8;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709, ___animator_4)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_animator_4() const { return ___animator_4; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_4), (void*)value);
	}

	inline static int32_t get_offset_of_playerManager_5() { return static_cast<int32_t>(offsetof(AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709, ___playerManager_5)); }
	inline PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * get_playerManager_5() const { return ___playerManager_5; }
	inline PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 ** get_address_of_playerManager_5() { return &___playerManager_5; }
	inline void set_playerManager_5(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * value)
	{
		___playerManager_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerManager_5), (void*)value);
	}

	inline static int32_t get_offset_of_playerLocomotion_6() { return static_cast<int32_t>(offsetof(AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709, ___playerLocomotion_6)); }
	inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * get_playerLocomotion_6() const { return ___playerLocomotion_6; }
	inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 ** get_address_of_playerLocomotion_6() { return &___playerLocomotion_6; }
	inline void set_playerLocomotion_6(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * value)
	{
		___playerLocomotion_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerLocomotion_6), (void*)value);
	}

	inline static int32_t get_offset_of_horizontal_7() { return static_cast<int32_t>(offsetof(AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709, ___horizontal_7)); }
	inline int32_t get_horizontal_7() const { return ___horizontal_7; }
	inline int32_t* get_address_of_horizontal_7() { return &___horizontal_7; }
	inline void set_horizontal_7(int32_t value)
	{
		___horizontal_7 = value;
	}

	inline static int32_t get_offset_of_vertical_8() { return static_cast<int32_t>(offsetof(AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709, ___vertical_8)); }
	inline int32_t get_vertical_8() const { return ___vertical_8; }
	inline int32_t* get_address_of_vertical_8() { return &___vertical_8; }
	inline void set_vertical_8(int32_t value)
	{
		___vertical_8 = value;
	}
};


// CameraManager
struct  CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// InputManager CameraManager::inputManager
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * ___inputManager_4;
	// UnityEngine.Transform CameraManager::targetTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___targetTransform_5;
	// UnityEngine.Transform CameraManager::cameraPivot
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___cameraPivot_6;
	// UnityEngine.Transform CameraManager::cameraTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___cameraTransform_7;
	// UnityEngine.LayerMask CameraManager::collisionLayers
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___collisionLayers_8;
	// System.Single CameraManager::defaultPosition
	float ___defaultPosition_9;
	// UnityEngine.Vector3 CameraManager::cameraFollowVelocity
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cameraFollowVelocity_10;
	// UnityEngine.Vector3 CameraManager::cameraVectorPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___cameraVectorPosition_11;
	// System.Single CameraManager::cameraCollisionOffset
	float ___cameraCollisionOffset_12;
	// System.Single CameraManager::minimumCollisionOffset
	float ___minimumCollisionOffset_13;
	// System.Single CameraManager::cameraCollisionRadius
	float ___cameraCollisionRadius_14;
	// System.Single CameraManager::cameraFollowspeed
	float ___cameraFollowspeed_15;
	// System.Single CameraManager::cameraLookSpeed
	float ___cameraLookSpeed_16;
	// System.Single CameraManager::cameraPivotSpeed
	float ___cameraPivotSpeed_17;
	// System.Single CameraManager::lookAngle
	float ___lookAngle_18;
	// System.Single CameraManager::pivotAngle
	float ___pivotAngle_19;
	// System.Single CameraManager::minimumPivotAngle
	float ___minimumPivotAngle_20;
	// System.Single CameraManager::maximumPivotAngle
	float ___maximumPivotAngle_21;
	// System.Single CameraManager::shoulderMinimumPivotAngle
	float ___shoulderMinimumPivotAngle_22;
	// System.Single CameraManager::shoulderMaximumPivotAngle
	float ___shoulderMaximumPivotAngle_23;
	// System.Single CameraManager::topDownMinimumPivotAngle
	float ___topDownMinimumPivotAngle_24;
	// System.Single CameraManager::topDownMaximumPivotAngle
	float ___topDownMaximumPivotAngle_25;
	// UnityEngine.GameObject[] CameraManager::cameras
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___cameras_26;
	// System.String CameraManager::currentCamera
	String_t* ___currentCamera_27;

public:
	inline static int32_t get_offset_of_inputManager_4() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___inputManager_4)); }
	inline InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * get_inputManager_4() const { return ___inputManager_4; }
	inline InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A ** get_address_of_inputManager_4() { return &___inputManager_4; }
	inline void set_inputManager_4(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * value)
	{
		___inputManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_targetTransform_5() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___targetTransform_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_targetTransform_5() const { return ___targetTransform_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_targetTransform_5() { return &___targetTransform_5; }
	inline void set_targetTransform_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___targetTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_cameraPivot_6() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraPivot_6)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_cameraPivot_6() const { return ___cameraPivot_6; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_cameraPivot_6() { return &___cameraPivot_6; }
	inline void set_cameraPivot_6(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___cameraPivot_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraPivot_6), (void*)value);
	}

	inline static int32_t get_offset_of_cameraTransform_7() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraTransform_7)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_cameraTransform_7() const { return ___cameraTransform_7; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_cameraTransform_7() { return &___cameraTransform_7; }
	inline void set_cameraTransform_7(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___cameraTransform_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraTransform_7), (void*)value);
	}

	inline static int32_t get_offset_of_collisionLayers_8() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___collisionLayers_8)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_collisionLayers_8() const { return ___collisionLayers_8; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_collisionLayers_8() { return &___collisionLayers_8; }
	inline void set_collisionLayers_8(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___collisionLayers_8 = value;
	}

	inline static int32_t get_offset_of_defaultPosition_9() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___defaultPosition_9)); }
	inline float get_defaultPosition_9() const { return ___defaultPosition_9; }
	inline float* get_address_of_defaultPosition_9() { return &___defaultPosition_9; }
	inline void set_defaultPosition_9(float value)
	{
		___defaultPosition_9 = value;
	}

	inline static int32_t get_offset_of_cameraFollowVelocity_10() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraFollowVelocity_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cameraFollowVelocity_10() const { return ___cameraFollowVelocity_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cameraFollowVelocity_10() { return &___cameraFollowVelocity_10; }
	inline void set_cameraFollowVelocity_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cameraFollowVelocity_10 = value;
	}

	inline static int32_t get_offset_of_cameraVectorPosition_11() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraVectorPosition_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_cameraVectorPosition_11() const { return ___cameraVectorPosition_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_cameraVectorPosition_11() { return &___cameraVectorPosition_11; }
	inline void set_cameraVectorPosition_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___cameraVectorPosition_11 = value;
	}

	inline static int32_t get_offset_of_cameraCollisionOffset_12() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraCollisionOffset_12)); }
	inline float get_cameraCollisionOffset_12() const { return ___cameraCollisionOffset_12; }
	inline float* get_address_of_cameraCollisionOffset_12() { return &___cameraCollisionOffset_12; }
	inline void set_cameraCollisionOffset_12(float value)
	{
		___cameraCollisionOffset_12 = value;
	}

	inline static int32_t get_offset_of_minimumCollisionOffset_13() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___minimumCollisionOffset_13)); }
	inline float get_minimumCollisionOffset_13() const { return ___minimumCollisionOffset_13; }
	inline float* get_address_of_minimumCollisionOffset_13() { return &___minimumCollisionOffset_13; }
	inline void set_minimumCollisionOffset_13(float value)
	{
		___minimumCollisionOffset_13 = value;
	}

	inline static int32_t get_offset_of_cameraCollisionRadius_14() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraCollisionRadius_14)); }
	inline float get_cameraCollisionRadius_14() const { return ___cameraCollisionRadius_14; }
	inline float* get_address_of_cameraCollisionRadius_14() { return &___cameraCollisionRadius_14; }
	inline void set_cameraCollisionRadius_14(float value)
	{
		___cameraCollisionRadius_14 = value;
	}

	inline static int32_t get_offset_of_cameraFollowspeed_15() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraFollowspeed_15)); }
	inline float get_cameraFollowspeed_15() const { return ___cameraFollowspeed_15; }
	inline float* get_address_of_cameraFollowspeed_15() { return &___cameraFollowspeed_15; }
	inline void set_cameraFollowspeed_15(float value)
	{
		___cameraFollowspeed_15 = value;
	}

	inline static int32_t get_offset_of_cameraLookSpeed_16() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraLookSpeed_16)); }
	inline float get_cameraLookSpeed_16() const { return ___cameraLookSpeed_16; }
	inline float* get_address_of_cameraLookSpeed_16() { return &___cameraLookSpeed_16; }
	inline void set_cameraLookSpeed_16(float value)
	{
		___cameraLookSpeed_16 = value;
	}

	inline static int32_t get_offset_of_cameraPivotSpeed_17() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameraPivotSpeed_17)); }
	inline float get_cameraPivotSpeed_17() const { return ___cameraPivotSpeed_17; }
	inline float* get_address_of_cameraPivotSpeed_17() { return &___cameraPivotSpeed_17; }
	inline void set_cameraPivotSpeed_17(float value)
	{
		___cameraPivotSpeed_17 = value;
	}

	inline static int32_t get_offset_of_lookAngle_18() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___lookAngle_18)); }
	inline float get_lookAngle_18() const { return ___lookAngle_18; }
	inline float* get_address_of_lookAngle_18() { return &___lookAngle_18; }
	inline void set_lookAngle_18(float value)
	{
		___lookAngle_18 = value;
	}

	inline static int32_t get_offset_of_pivotAngle_19() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___pivotAngle_19)); }
	inline float get_pivotAngle_19() const { return ___pivotAngle_19; }
	inline float* get_address_of_pivotAngle_19() { return &___pivotAngle_19; }
	inline void set_pivotAngle_19(float value)
	{
		___pivotAngle_19 = value;
	}

	inline static int32_t get_offset_of_minimumPivotAngle_20() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___minimumPivotAngle_20)); }
	inline float get_minimumPivotAngle_20() const { return ___minimumPivotAngle_20; }
	inline float* get_address_of_minimumPivotAngle_20() { return &___minimumPivotAngle_20; }
	inline void set_minimumPivotAngle_20(float value)
	{
		___minimumPivotAngle_20 = value;
	}

	inline static int32_t get_offset_of_maximumPivotAngle_21() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___maximumPivotAngle_21)); }
	inline float get_maximumPivotAngle_21() const { return ___maximumPivotAngle_21; }
	inline float* get_address_of_maximumPivotAngle_21() { return &___maximumPivotAngle_21; }
	inline void set_maximumPivotAngle_21(float value)
	{
		___maximumPivotAngle_21 = value;
	}

	inline static int32_t get_offset_of_shoulderMinimumPivotAngle_22() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___shoulderMinimumPivotAngle_22)); }
	inline float get_shoulderMinimumPivotAngle_22() const { return ___shoulderMinimumPivotAngle_22; }
	inline float* get_address_of_shoulderMinimumPivotAngle_22() { return &___shoulderMinimumPivotAngle_22; }
	inline void set_shoulderMinimumPivotAngle_22(float value)
	{
		___shoulderMinimumPivotAngle_22 = value;
	}

	inline static int32_t get_offset_of_shoulderMaximumPivotAngle_23() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___shoulderMaximumPivotAngle_23)); }
	inline float get_shoulderMaximumPivotAngle_23() const { return ___shoulderMaximumPivotAngle_23; }
	inline float* get_address_of_shoulderMaximumPivotAngle_23() { return &___shoulderMaximumPivotAngle_23; }
	inline void set_shoulderMaximumPivotAngle_23(float value)
	{
		___shoulderMaximumPivotAngle_23 = value;
	}

	inline static int32_t get_offset_of_topDownMinimumPivotAngle_24() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___topDownMinimumPivotAngle_24)); }
	inline float get_topDownMinimumPivotAngle_24() const { return ___topDownMinimumPivotAngle_24; }
	inline float* get_address_of_topDownMinimumPivotAngle_24() { return &___topDownMinimumPivotAngle_24; }
	inline void set_topDownMinimumPivotAngle_24(float value)
	{
		___topDownMinimumPivotAngle_24 = value;
	}

	inline static int32_t get_offset_of_topDownMaximumPivotAngle_25() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___topDownMaximumPivotAngle_25)); }
	inline float get_topDownMaximumPivotAngle_25() const { return ___topDownMaximumPivotAngle_25; }
	inline float* get_address_of_topDownMaximumPivotAngle_25() { return &___topDownMaximumPivotAngle_25; }
	inline void set_topDownMaximumPivotAngle_25(float value)
	{
		___topDownMaximumPivotAngle_25 = value;
	}

	inline static int32_t get_offset_of_cameras_26() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___cameras_26)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_cameras_26() const { return ___cameras_26; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_cameras_26() { return &___cameras_26; }
	inline void set_cameras_26(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___cameras_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameras_26), (void*)value);
	}

	inline static int32_t get_offset_of_currentCamera_27() { return static_cast<int32_t>(offsetof(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A, ___currentCamera_27)); }
	inline String_t* get_currentCamera_27() const { return ___currentCamera_27; }
	inline String_t** get_address_of_currentCamera_27() { return &___currentCamera_27; }
	inline void set_currentCamera_27(String_t* value)
	{
		___currentCamera_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentCamera_27), (void*)value);
	}
};


// CameraMovement
struct  CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CameraMovement::lookSpeedH
	float ___lookSpeedH_4;
	// System.Single CameraMovement::lookSpeedV
	float ___lookSpeedV_5;
	// System.Single CameraMovement::zoomSpeed
	float ___zoomSpeed_6;
	// System.Single CameraMovement::dragSpeed
	float ___dragSpeed_7;
	// System.Single CameraMovement::yaw
	float ___yaw_8;
	// System.Single CameraMovement::pitch
	float ___pitch_9;

public:
	inline static int32_t get_offset_of_lookSpeedH_4() { return static_cast<int32_t>(offsetof(CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35, ___lookSpeedH_4)); }
	inline float get_lookSpeedH_4() const { return ___lookSpeedH_4; }
	inline float* get_address_of_lookSpeedH_4() { return &___lookSpeedH_4; }
	inline void set_lookSpeedH_4(float value)
	{
		___lookSpeedH_4 = value;
	}

	inline static int32_t get_offset_of_lookSpeedV_5() { return static_cast<int32_t>(offsetof(CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35, ___lookSpeedV_5)); }
	inline float get_lookSpeedV_5() const { return ___lookSpeedV_5; }
	inline float* get_address_of_lookSpeedV_5() { return &___lookSpeedV_5; }
	inline void set_lookSpeedV_5(float value)
	{
		___lookSpeedV_5 = value;
	}

	inline static int32_t get_offset_of_zoomSpeed_6() { return static_cast<int32_t>(offsetof(CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35, ___zoomSpeed_6)); }
	inline float get_zoomSpeed_6() const { return ___zoomSpeed_6; }
	inline float* get_address_of_zoomSpeed_6() { return &___zoomSpeed_6; }
	inline void set_zoomSpeed_6(float value)
	{
		___zoomSpeed_6 = value;
	}

	inline static int32_t get_offset_of_dragSpeed_7() { return static_cast<int32_t>(offsetof(CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35, ___dragSpeed_7)); }
	inline float get_dragSpeed_7() const { return ___dragSpeed_7; }
	inline float* get_address_of_dragSpeed_7() { return &___dragSpeed_7; }
	inline void set_dragSpeed_7(float value)
	{
		___dragSpeed_7 = value;
	}

	inline static int32_t get_offset_of_yaw_8() { return static_cast<int32_t>(offsetof(CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35, ___yaw_8)); }
	inline float get_yaw_8() const { return ___yaw_8; }
	inline float* get_address_of_yaw_8() { return &___yaw_8; }
	inline void set_yaw_8(float value)
	{
		___yaw_8 = value;
	}

	inline static int32_t get_offset_of_pitch_9() { return static_cast<int32_t>(offsetof(CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35, ___pitch_9)); }
	inline float get_pitch_9() const { return ___pitch_9; }
	inline float* get_address_of_pitch_9() { return &___pitch_9; }
	inline void set_pitch_9(float value)
	{
		___pitch_9 = value;
	}
};


// Character_control
struct  Character_control_tEB187B80DE857A5EE58CD99757188637F71C8EF2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 Character_control::animation_num
	int32_t ___animation_num_4;
	// UnityEngine.Animator Character_control::animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator_5;

public:
	inline static int32_t get_offset_of_animation_num_4() { return static_cast<int32_t>(offsetof(Character_control_tEB187B80DE857A5EE58CD99757188637F71C8EF2, ___animation_num_4)); }
	inline int32_t get_animation_num_4() const { return ___animation_num_4; }
	inline int32_t* get_address_of_animation_num_4() { return &___animation_num_4; }
	inline void set_animation_num_4(int32_t value)
	{
		___animation_num_4 = value;
	}

	inline static int32_t get_offset_of_animator_5() { return static_cast<int32_t>(offsetof(Character_control_tEB187B80DE857A5EE58CD99757188637F71C8EF2, ___animator_5)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_animator_5() const { return ___animator_5; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_animator_5() { return &___animator_5; }
	inline void set_animator_5(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___animator_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_5), (void*)value);
	}
};


// ChargeManager
struct  ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single ChargeManager::chargeCounter
	float ___chargeCounter_4;
	// System.Single ChargeManager::chargeSpeed
	float ___chargeSpeed_5;
	// System.Single ChargeManager::drainSpeed
	float ___drainSpeed_6;
	// UnityEngine.UI.Text ChargeManager::textBox
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___textBox_7;
	// UnityEngine.UI.Slider ChargeManager::slider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___slider_8;

public:
	inline static int32_t get_offset_of_chargeCounter_4() { return static_cast<int32_t>(offsetof(ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0, ___chargeCounter_4)); }
	inline float get_chargeCounter_4() const { return ___chargeCounter_4; }
	inline float* get_address_of_chargeCounter_4() { return &___chargeCounter_4; }
	inline void set_chargeCounter_4(float value)
	{
		___chargeCounter_4 = value;
	}

	inline static int32_t get_offset_of_chargeSpeed_5() { return static_cast<int32_t>(offsetof(ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0, ___chargeSpeed_5)); }
	inline float get_chargeSpeed_5() const { return ___chargeSpeed_5; }
	inline float* get_address_of_chargeSpeed_5() { return &___chargeSpeed_5; }
	inline void set_chargeSpeed_5(float value)
	{
		___chargeSpeed_5 = value;
	}

	inline static int32_t get_offset_of_drainSpeed_6() { return static_cast<int32_t>(offsetof(ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0, ___drainSpeed_6)); }
	inline float get_drainSpeed_6() const { return ___drainSpeed_6; }
	inline float* get_address_of_drainSpeed_6() { return &___drainSpeed_6; }
	inline void set_drainSpeed_6(float value)
	{
		___drainSpeed_6 = value;
	}

	inline static int32_t get_offset_of_textBox_7() { return static_cast<int32_t>(offsetof(ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0, ___textBox_7)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_textBox_7() const { return ___textBox_7; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_textBox_7() { return &___textBox_7; }
	inline void set_textBox_7(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___textBox_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___textBox_7), (void*)value);
	}

	inline static int32_t get_offset_of_slider_8() { return static_cast<int32_t>(offsetof(ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0, ___slider_8)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_slider_8() const { return ___slider_8; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_slider_8() { return &___slider_8; }
	inline void set_slider_8(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___slider_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slider_8), (void*)value);
	}
};


// GravityOrbit
struct  GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single GravityOrbit::gravityForce
	float ___gravityForce_4;

public:
	inline static int32_t get_offset_of_gravityForce_4() { return static_cast<int32_t>(offsetof(GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2, ___gravityForce_4)); }
	inline float get_gravityForce_4() const { return ___gravityForce_4; }
	inline float* get_address_of_gravityForce_4() { return &___gravityForce_4; }
	inline void set_gravityForce_4(float value)
	{
		___gravityForce_4 = value;
	}
};


// InputManager
struct  InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// AnimationManager InputManager::animationManager
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * ___animationManager_4;
	// PlayerLocomotion InputManager::playerLocomotion
	PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * ___playerLocomotion_5;
	// UnityEngine.Vector2 InputManager::movementInput
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___movementInput_6;
	// UnityEngine.Vector2 InputManager::cameraInput
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___cameraInput_7;
	// System.Single InputManager::moveAmount
	float ___moveAmount_8;
	// System.Single InputManager::verticalInput
	float ___verticalInput_9;
	// System.Single InputManager::horizontalInput
	float ___horizontalInput_10;
	// System.Single InputManager::cameraInputX
	float ___cameraInputX_11;
	// System.Single InputManager::cameraInputY
	float ___cameraInputY_12;
	// System.Boolean InputManager::sprintPressed
	bool ___sprintPressed_13;
	// System.Boolean InputManager::jumpPressed
	bool ___jumpPressed_14;
	// System.Boolean InputManager::shootPressed
	bool ___shootPressed_15;
	// System.Boolean InputManager::aimPressed
	bool ___aimPressed_16;

public:
	inline static int32_t get_offset_of_animationManager_4() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___animationManager_4)); }
	inline AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * get_animationManager_4() const { return ___animationManager_4; }
	inline AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 ** get_address_of_animationManager_4() { return &___animationManager_4; }
	inline void set_animationManager_4(AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * value)
	{
		___animationManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_playerLocomotion_5() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___playerLocomotion_5)); }
	inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * get_playerLocomotion_5() const { return ___playerLocomotion_5; }
	inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 ** get_address_of_playerLocomotion_5() { return &___playerLocomotion_5; }
	inline void set_playerLocomotion_5(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * value)
	{
		___playerLocomotion_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerLocomotion_5), (void*)value);
	}

	inline static int32_t get_offset_of_movementInput_6() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___movementInput_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_movementInput_6() const { return ___movementInput_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_movementInput_6() { return &___movementInput_6; }
	inline void set_movementInput_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___movementInput_6 = value;
	}

	inline static int32_t get_offset_of_cameraInput_7() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___cameraInput_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_cameraInput_7() const { return ___cameraInput_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_cameraInput_7() { return &___cameraInput_7; }
	inline void set_cameraInput_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___cameraInput_7 = value;
	}

	inline static int32_t get_offset_of_moveAmount_8() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___moveAmount_8)); }
	inline float get_moveAmount_8() const { return ___moveAmount_8; }
	inline float* get_address_of_moveAmount_8() { return &___moveAmount_8; }
	inline void set_moveAmount_8(float value)
	{
		___moveAmount_8 = value;
	}

	inline static int32_t get_offset_of_verticalInput_9() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___verticalInput_9)); }
	inline float get_verticalInput_9() const { return ___verticalInput_9; }
	inline float* get_address_of_verticalInput_9() { return &___verticalInput_9; }
	inline void set_verticalInput_9(float value)
	{
		___verticalInput_9 = value;
	}

	inline static int32_t get_offset_of_horizontalInput_10() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___horizontalInput_10)); }
	inline float get_horizontalInput_10() const { return ___horizontalInput_10; }
	inline float* get_address_of_horizontalInput_10() { return &___horizontalInput_10; }
	inline void set_horizontalInput_10(float value)
	{
		___horizontalInput_10 = value;
	}

	inline static int32_t get_offset_of_cameraInputX_11() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___cameraInputX_11)); }
	inline float get_cameraInputX_11() const { return ___cameraInputX_11; }
	inline float* get_address_of_cameraInputX_11() { return &___cameraInputX_11; }
	inline void set_cameraInputX_11(float value)
	{
		___cameraInputX_11 = value;
	}

	inline static int32_t get_offset_of_cameraInputY_12() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___cameraInputY_12)); }
	inline float get_cameraInputY_12() const { return ___cameraInputY_12; }
	inline float* get_address_of_cameraInputY_12() { return &___cameraInputY_12; }
	inline void set_cameraInputY_12(float value)
	{
		___cameraInputY_12 = value;
	}

	inline static int32_t get_offset_of_sprintPressed_13() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___sprintPressed_13)); }
	inline bool get_sprintPressed_13() const { return ___sprintPressed_13; }
	inline bool* get_address_of_sprintPressed_13() { return &___sprintPressed_13; }
	inline void set_sprintPressed_13(bool value)
	{
		___sprintPressed_13 = value;
	}

	inline static int32_t get_offset_of_jumpPressed_14() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___jumpPressed_14)); }
	inline bool get_jumpPressed_14() const { return ___jumpPressed_14; }
	inline bool* get_address_of_jumpPressed_14() { return &___jumpPressed_14; }
	inline void set_jumpPressed_14(bool value)
	{
		___jumpPressed_14 = value;
	}

	inline static int32_t get_offset_of_shootPressed_15() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___shootPressed_15)); }
	inline bool get_shootPressed_15() const { return ___shootPressed_15; }
	inline bool* get_address_of_shootPressed_15() { return &___shootPressed_15; }
	inline void set_shootPressed_15(bool value)
	{
		___shootPressed_15 = value;
	}

	inline static int32_t get_offset_of_aimPressed_16() { return static_cast<int32_t>(offsetof(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A, ___aimPressed_16)); }
	inline bool get_aimPressed_16() const { return ___aimPressed_16; }
	inline bool* get_address_of_aimPressed_16() { return &___aimPressed_16; }
	inline void set_aimPressed_16(bool value)
	{
		___aimPressed_16 = value;
	}
};


// LevelManager
struct  LevelManager_t010B312A2B35B45291F58195216ABB5673174961  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// PlanetOrbit
struct  PlanetOrbit_tDB68D6FC93B55A78AC923A046D6E61E722E2542A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 PlanetOrbit::OriginPoint
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___OriginPoint_4;
	// System.Single PlanetOrbit::orbitSpeed
	float ___orbitSpeed_5;

public:
	inline static int32_t get_offset_of_OriginPoint_4() { return static_cast<int32_t>(offsetof(PlanetOrbit_tDB68D6FC93B55A78AC923A046D6E61E722E2542A, ___OriginPoint_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_OriginPoint_4() const { return ___OriginPoint_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_OriginPoint_4() { return &___OriginPoint_4; }
	inline void set_OriginPoint_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___OriginPoint_4 = value;
	}

	inline static int32_t get_offset_of_orbitSpeed_5() { return static_cast<int32_t>(offsetof(PlanetOrbit_tDB68D6FC93B55A78AC923A046D6E61E722E2542A, ___orbitSpeed_5)); }
	inline float get_orbitSpeed_5() const { return ___orbitSpeed_5; }
	inline float* get_address_of_orbitSpeed_5() { return &___orbitSpeed_5; }
	inline void set_orbitSpeed_5(float value)
	{
		___orbitSpeed_5 = value;
	}
};


// PlayerEffectManager
struct  PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject PlayerEffectManager::LandEffect
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___LandEffect_4;
	// UnityEngine.GameObject PlayerEffectManager::windTrails
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___windTrails_5;
	// UnityEngine.GameObject PlayerEffectManager::jetPropulsion
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___jetPropulsion_6;

public:
	inline static int32_t get_offset_of_LandEffect_4() { return static_cast<int32_t>(offsetof(PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D, ___LandEffect_4)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_LandEffect_4() const { return ___LandEffect_4; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_LandEffect_4() { return &___LandEffect_4; }
	inline void set_LandEffect_4(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___LandEffect_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___LandEffect_4), (void*)value);
	}

	inline static int32_t get_offset_of_windTrails_5() { return static_cast<int32_t>(offsetof(PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D, ___windTrails_5)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_windTrails_5() const { return ___windTrails_5; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_windTrails_5() { return &___windTrails_5; }
	inline void set_windTrails_5(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___windTrails_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___windTrails_5), (void*)value);
	}

	inline static int32_t get_offset_of_jetPropulsion_6() { return static_cast<int32_t>(offsetof(PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D, ___jetPropulsion_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_jetPropulsion_6() const { return ___jetPropulsion_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_jetPropulsion_6() { return &___jetPropulsion_6; }
	inline void set_jetPropulsion_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___jetPropulsion_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___jetPropulsion_6), (void*)value);
	}
};


// PlayerLocomotion
struct  PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// PlayerManager PlayerLocomotion::playerManager
	PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * ___playerManager_4;
	// AnimationManager PlayerLocomotion::animationManager
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * ___animationManager_5;
	// InputManager PlayerLocomotion::inputManager
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * ___inputManager_6;
	// ChargeManager PlayerLocomotion::chargeManager
	ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * ___chargeManager_7;
	// CameraManager PlayerLocomotion::cameraManager
	CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * ___cameraManager_8;
	// UnityEngine.Vector3 PlayerLocomotion::moveDirection
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___moveDirection_9;
	// UnityEngine.Transform PlayerLocomotion::cameraObject
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___cameraObject_10;
	// UnityEngine.Rigidbody PlayerLocomotion::rb
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___rb_11;
	// System.Single PlayerLocomotion::inAirTimer
	float ___inAirTimer_12;
	// System.Single PlayerLocomotion::leapingVelocity
	float ___leapingVelocity_13;
	// System.Single PlayerLocomotion::fallingVelocity
	float ___fallingVelocity_14;
	// System.Single PlayerLocomotion::raycastHeightOffset
	float ___raycastHeightOffset_15;
	// UnityEngine.LayerMask PlayerLocomotion::groundLayer
	LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___groundLayer_16;
	// System.Boolean PlayerLocomotion::isSprinting
	bool ___isSprinting_17;
	// System.Boolean PlayerLocomotion::isGrounded
	bool ___isGrounded_18;
	// System.Boolean PlayerLocomotion::isJumping
	bool ___isJumping_19;
	// System.Boolean PlayerLocomotion::isShooting
	bool ___isShooting_20;
	// System.Single PlayerLocomotion::shootCheckDistance
	float ___shootCheckDistance_21;
	// GravityOrbit PlayerLocomotion::currentGravity
	GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * ___currentGravity_22;
	// UnityEngine.Vector3 PlayerLocomotion::localUp
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___localUp_23;
	// System.Single PlayerLocomotion::walkingspeed
	float ___walkingspeed_24;
	// System.Single PlayerLocomotion::runningspeed
	float ___runningspeed_25;
	// System.Single PlayerLocomotion::sprintingSpeed
	float ___sprintingSpeed_26;
	// System.Single PlayerLocomotion::rotationSpeed
	float ___rotationSpeed_27;
	// System.Single PlayerLocomotion::jumpHeight
	float ___jumpHeight_28;
	// System.Single PlayerLocomotion::gravityIntensity
	float ___gravityIntensity_29;

public:
	inline static int32_t get_offset_of_playerManager_4() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___playerManager_4)); }
	inline PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * get_playerManager_4() const { return ___playerManager_4; }
	inline PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 ** get_address_of_playerManager_4() { return &___playerManager_4; }
	inline void set_playerManager_4(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * value)
	{
		___playerManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_animationManager_5() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___animationManager_5)); }
	inline AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * get_animationManager_5() const { return ___animationManager_5; }
	inline AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 ** get_address_of_animationManager_5() { return &___animationManager_5; }
	inline void set_animationManager_5(AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * value)
	{
		___animationManager_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animationManager_5), (void*)value);
	}

	inline static int32_t get_offset_of_inputManager_6() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___inputManager_6)); }
	inline InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * get_inputManager_6() const { return ___inputManager_6; }
	inline InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A ** get_address_of_inputManager_6() { return &___inputManager_6; }
	inline void set_inputManager_6(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * value)
	{
		___inputManager_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputManager_6), (void*)value);
	}

	inline static int32_t get_offset_of_chargeManager_7() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___chargeManager_7)); }
	inline ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * get_chargeManager_7() const { return ___chargeManager_7; }
	inline ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 ** get_address_of_chargeManager_7() { return &___chargeManager_7; }
	inline void set_chargeManager_7(ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * value)
	{
		___chargeManager_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chargeManager_7), (void*)value);
	}

	inline static int32_t get_offset_of_cameraManager_8() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___cameraManager_8)); }
	inline CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * get_cameraManager_8() const { return ___cameraManager_8; }
	inline CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A ** get_address_of_cameraManager_8() { return &___cameraManager_8; }
	inline void set_cameraManager_8(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * value)
	{
		___cameraManager_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraManager_8), (void*)value);
	}

	inline static int32_t get_offset_of_moveDirection_9() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___moveDirection_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_moveDirection_9() const { return ___moveDirection_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_moveDirection_9() { return &___moveDirection_9; }
	inline void set_moveDirection_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___moveDirection_9 = value;
	}

	inline static int32_t get_offset_of_cameraObject_10() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___cameraObject_10)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_cameraObject_10() const { return ___cameraObject_10; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_cameraObject_10() { return &___cameraObject_10; }
	inline void set_cameraObject_10(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___cameraObject_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraObject_10), (void*)value);
	}

	inline static int32_t get_offset_of_rb_11() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___rb_11)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_rb_11() const { return ___rb_11; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_rb_11() { return &___rb_11; }
	inline void set_rb_11(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___rb_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rb_11), (void*)value);
	}

	inline static int32_t get_offset_of_inAirTimer_12() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___inAirTimer_12)); }
	inline float get_inAirTimer_12() const { return ___inAirTimer_12; }
	inline float* get_address_of_inAirTimer_12() { return &___inAirTimer_12; }
	inline void set_inAirTimer_12(float value)
	{
		___inAirTimer_12 = value;
	}

	inline static int32_t get_offset_of_leapingVelocity_13() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___leapingVelocity_13)); }
	inline float get_leapingVelocity_13() const { return ___leapingVelocity_13; }
	inline float* get_address_of_leapingVelocity_13() { return &___leapingVelocity_13; }
	inline void set_leapingVelocity_13(float value)
	{
		___leapingVelocity_13 = value;
	}

	inline static int32_t get_offset_of_fallingVelocity_14() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___fallingVelocity_14)); }
	inline float get_fallingVelocity_14() const { return ___fallingVelocity_14; }
	inline float* get_address_of_fallingVelocity_14() { return &___fallingVelocity_14; }
	inline void set_fallingVelocity_14(float value)
	{
		___fallingVelocity_14 = value;
	}

	inline static int32_t get_offset_of_raycastHeightOffset_15() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___raycastHeightOffset_15)); }
	inline float get_raycastHeightOffset_15() const { return ___raycastHeightOffset_15; }
	inline float* get_address_of_raycastHeightOffset_15() { return &___raycastHeightOffset_15; }
	inline void set_raycastHeightOffset_15(float value)
	{
		___raycastHeightOffset_15 = value;
	}

	inline static int32_t get_offset_of_groundLayer_16() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___groundLayer_16)); }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  get_groundLayer_16() const { return ___groundLayer_16; }
	inline LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8 * get_address_of_groundLayer_16() { return &___groundLayer_16; }
	inline void set_groundLayer_16(LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  value)
	{
		___groundLayer_16 = value;
	}

	inline static int32_t get_offset_of_isSprinting_17() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___isSprinting_17)); }
	inline bool get_isSprinting_17() const { return ___isSprinting_17; }
	inline bool* get_address_of_isSprinting_17() { return &___isSprinting_17; }
	inline void set_isSprinting_17(bool value)
	{
		___isSprinting_17 = value;
	}

	inline static int32_t get_offset_of_isGrounded_18() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___isGrounded_18)); }
	inline bool get_isGrounded_18() const { return ___isGrounded_18; }
	inline bool* get_address_of_isGrounded_18() { return &___isGrounded_18; }
	inline void set_isGrounded_18(bool value)
	{
		___isGrounded_18 = value;
	}

	inline static int32_t get_offset_of_isJumping_19() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___isJumping_19)); }
	inline bool get_isJumping_19() const { return ___isJumping_19; }
	inline bool* get_address_of_isJumping_19() { return &___isJumping_19; }
	inline void set_isJumping_19(bool value)
	{
		___isJumping_19 = value;
	}

	inline static int32_t get_offset_of_isShooting_20() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___isShooting_20)); }
	inline bool get_isShooting_20() const { return ___isShooting_20; }
	inline bool* get_address_of_isShooting_20() { return &___isShooting_20; }
	inline void set_isShooting_20(bool value)
	{
		___isShooting_20 = value;
	}

	inline static int32_t get_offset_of_shootCheckDistance_21() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___shootCheckDistance_21)); }
	inline float get_shootCheckDistance_21() const { return ___shootCheckDistance_21; }
	inline float* get_address_of_shootCheckDistance_21() { return &___shootCheckDistance_21; }
	inline void set_shootCheckDistance_21(float value)
	{
		___shootCheckDistance_21 = value;
	}

	inline static int32_t get_offset_of_currentGravity_22() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___currentGravity_22)); }
	inline GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * get_currentGravity_22() const { return ___currentGravity_22; }
	inline GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 ** get_address_of_currentGravity_22() { return &___currentGravity_22; }
	inline void set_currentGravity_22(GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * value)
	{
		___currentGravity_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentGravity_22), (void*)value);
	}

	inline static int32_t get_offset_of_localUp_23() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___localUp_23)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_localUp_23() const { return ___localUp_23; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_localUp_23() { return &___localUp_23; }
	inline void set_localUp_23(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___localUp_23 = value;
	}

	inline static int32_t get_offset_of_walkingspeed_24() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___walkingspeed_24)); }
	inline float get_walkingspeed_24() const { return ___walkingspeed_24; }
	inline float* get_address_of_walkingspeed_24() { return &___walkingspeed_24; }
	inline void set_walkingspeed_24(float value)
	{
		___walkingspeed_24 = value;
	}

	inline static int32_t get_offset_of_runningspeed_25() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___runningspeed_25)); }
	inline float get_runningspeed_25() const { return ___runningspeed_25; }
	inline float* get_address_of_runningspeed_25() { return &___runningspeed_25; }
	inline void set_runningspeed_25(float value)
	{
		___runningspeed_25 = value;
	}

	inline static int32_t get_offset_of_sprintingSpeed_26() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___sprintingSpeed_26)); }
	inline float get_sprintingSpeed_26() const { return ___sprintingSpeed_26; }
	inline float* get_address_of_sprintingSpeed_26() { return &___sprintingSpeed_26; }
	inline void set_sprintingSpeed_26(float value)
	{
		___sprintingSpeed_26 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_27() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___rotationSpeed_27)); }
	inline float get_rotationSpeed_27() const { return ___rotationSpeed_27; }
	inline float* get_address_of_rotationSpeed_27() { return &___rotationSpeed_27; }
	inline void set_rotationSpeed_27(float value)
	{
		___rotationSpeed_27 = value;
	}

	inline static int32_t get_offset_of_jumpHeight_28() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___jumpHeight_28)); }
	inline float get_jumpHeight_28() const { return ___jumpHeight_28; }
	inline float* get_address_of_jumpHeight_28() { return &___jumpHeight_28; }
	inline void set_jumpHeight_28(float value)
	{
		___jumpHeight_28 = value;
	}

	inline static int32_t get_offset_of_gravityIntensity_29() { return static_cast<int32_t>(offsetof(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795, ___gravityIntensity_29)); }
	inline float get_gravityIntensity_29() const { return ___gravityIntensity_29; }
	inline float* get_address_of_gravityIntensity_29() { return &___gravityIntensity_29; }
	inline void set_gravityIntensity_29(float value)
	{
		___gravityIntensity_29 = value;
	}
};


// PlayerManager
struct  PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// InputManager PlayerManager::inputManager
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * ___inputManager_4;
	// CameraManager PlayerManager::cameraManager
	CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * ___cameraManager_5;
	// UnityEngine.Animator PlayerManager::animator
	Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator_6;
	// PlayerLocomotion PlayerManager::playerLocomotion
	PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * ___playerLocomotion_7;
	// ChargeManager PlayerManager::chargeManager
	ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * ___chargeManager_8;
	// PlayerEffectManager PlayerManager::playerEffectManager
	PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * ___playerEffectManager_9;
	// System.Boolean PlayerManager::isInteracting
	bool ___isInteracting_10;
	// System.Boolean PlayerManager::isUsingRootMotion
	bool ___isUsingRootMotion_11;

public:
	inline static int32_t get_offset_of_inputManager_4() { return static_cast<int32_t>(offsetof(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48, ___inputManager_4)); }
	inline InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * get_inputManager_4() const { return ___inputManager_4; }
	inline InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A ** get_address_of_inputManager_4() { return &___inputManager_4; }
	inline void set_inputManager_4(InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * value)
	{
		___inputManager_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___inputManager_4), (void*)value);
	}

	inline static int32_t get_offset_of_cameraManager_5() { return static_cast<int32_t>(offsetof(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48, ___cameraManager_5)); }
	inline CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * get_cameraManager_5() const { return ___cameraManager_5; }
	inline CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A ** get_address_of_cameraManager_5() { return &___cameraManager_5; }
	inline void set_cameraManager_5(CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * value)
	{
		___cameraManager_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cameraManager_5), (void*)value);
	}

	inline static int32_t get_offset_of_animator_6() { return static_cast<int32_t>(offsetof(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48, ___animator_6)); }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * get_animator_6() const { return ___animator_6; }
	inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 ** get_address_of_animator_6() { return &___animator_6; }
	inline void set_animator_6(Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * value)
	{
		___animator_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___animator_6), (void*)value);
	}

	inline static int32_t get_offset_of_playerLocomotion_7() { return static_cast<int32_t>(offsetof(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48, ___playerLocomotion_7)); }
	inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * get_playerLocomotion_7() const { return ___playerLocomotion_7; }
	inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 ** get_address_of_playerLocomotion_7() { return &___playerLocomotion_7; }
	inline void set_playerLocomotion_7(PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * value)
	{
		___playerLocomotion_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerLocomotion_7), (void*)value);
	}

	inline static int32_t get_offset_of_chargeManager_8() { return static_cast<int32_t>(offsetof(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48, ___chargeManager_8)); }
	inline ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * get_chargeManager_8() const { return ___chargeManager_8; }
	inline ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 ** get_address_of_chargeManager_8() { return &___chargeManager_8; }
	inline void set_chargeManager_8(ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * value)
	{
		___chargeManager_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___chargeManager_8), (void*)value);
	}

	inline static int32_t get_offset_of_playerEffectManager_9() { return static_cast<int32_t>(offsetof(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48, ___playerEffectManager_9)); }
	inline PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * get_playerEffectManager_9() const { return ___playerEffectManager_9; }
	inline PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D ** get_address_of_playerEffectManager_9() { return &___playerEffectManager_9; }
	inline void set_playerEffectManager_9(PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * value)
	{
		___playerEffectManager_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___playerEffectManager_9), (void*)value);
	}

	inline static int32_t get_offset_of_isInteracting_10() { return static_cast<int32_t>(offsetof(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48, ___isInteracting_10)); }
	inline bool get_isInteracting_10() const { return ___isInteracting_10; }
	inline bool* get_address_of_isInteracting_10() { return &___isInteracting_10; }
	inline void set_isInteracting_10(bool value)
	{
		___isInteracting_10 = value;
	}

	inline static int32_t get_offset_of_isUsingRootMotion_11() { return static_cast<int32_t>(offsetof(PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48, ___isUsingRootMotion_11)); }
	inline bool get_isUsingRootMotion_11() const { return ___isUsingRootMotion_11; }
	inline bool* get_address_of_isUsingRootMotion_11() { return &___isUsingRootMotion_11; }
	inline void set_isUsingRootMotion_11(bool value)
	{
		___isUsingRootMotion_11 = value;
	}
};


// Rotatator
struct  Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector3 Rotatator::rotation
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rotation_4;
	// UnityEngine.Transform Rotatator::meshObject
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___meshObject_5;
	// System.Single Rotatator::rotationSpeed
	float ___rotationSpeed_6;
	// System.Boolean Rotatator::randomize
	bool ___randomize_7;
	// System.Single Rotatator::maxSpeed
	float ___maxSpeed_8;
	// System.Single Rotatator::minSpeed
	float ___minSpeed_9;

public:
	inline static int32_t get_offset_of_rotation_4() { return static_cast<int32_t>(offsetof(Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D, ___rotation_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rotation_4() const { return ___rotation_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rotation_4() { return &___rotation_4; }
	inline void set_rotation_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rotation_4 = value;
	}

	inline static int32_t get_offset_of_meshObject_5() { return static_cast<int32_t>(offsetof(Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D, ___meshObject_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_meshObject_5() const { return ___meshObject_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_meshObject_5() { return &___meshObject_5; }
	inline void set_meshObject_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___meshObject_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___meshObject_5), (void*)value);
	}

	inline static int32_t get_offset_of_rotationSpeed_6() { return static_cast<int32_t>(offsetof(Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D, ___rotationSpeed_6)); }
	inline float get_rotationSpeed_6() const { return ___rotationSpeed_6; }
	inline float* get_address_of_rotationSpeed_6() { return &___rotationSpeed_6; }
	inline void set_rotationSpeed_6(float value)
	{
		___rotationSpeed_6 = value;
	}

	inline static int32_t get_offset_of_randomize_7() { return static_cast<int32_t>(offsetof(Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D, ___randomize_7)); }
	inline bool get_randomize_7() const { return ___randomize_7; }
	inline bool* get_address_of_randomize_7() { return &___randomize_7; }
	inline void set_randomize_7(bool value)
	{
		___randomize_7 = value;
	}

	inline static int32_t get_offset_of_maxSpeed_8() { return static_cast<int32_t>(offsetof(Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D, ___maxSpeed_8)); }
	inline float get_maxSpeed_8() const { return ___maxSpeed_8; }
	inline float* get_address_of_maxSpeed_8() { return &___maxSpeed_8; }
	inline void set_maxSpeed_8(float value)
	{
		___maxSpeed_8 = value;
	}

	inline static int32_t get_offset_of_minSpeed_9() { return static_cast<int32_t>(offsetof(Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D, ___minSpeed_9)); }
	inline float get_minSpeed_9() const { return ___minSpeed_9; }
	inline float* get_address_of_minSpeed_9() { return &___minSpeed_9; }
	inline void set_minSpeed_9(float value)
	{
		___minSpeed_9 = value;
	}
};


// SliderScripts
struct  SliderScripts_t01CD33323C1390AABFEA3597355DD017703D404F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.UI.Slider SliderScripts::slider
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___slider_4;
	// UnityEngine.UI.Image SliderScripts::fill
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___fill_5;

public:
	inline static int32_t get_offset_of_slider_4() { return static_cast<int32_t>(offsetof(SliderScripts_t01CD33323C1390AABFEA3597355DD017703D404F, ___slider_4)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_slider_4() const { return ___slider_4; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_slider_4() { return &___slider_4; }
	inline void set_slider_4(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___slider_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___slider_4), (void*)value);
	}

	inline static int32_t get_offset_of_fill_5() { return static_cast<int32_t>(offsetof(SliderScripts_t01CD33323C1390AABFEA3597355DD017703D404F, ___fill_5)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_fill_5() const { return ___fill_5; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_fill_5() { return &___fill_5; }
	inline void set_fill_5(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___fill_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___fill_5), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct  UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Graphic
struct  Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct  Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.MaskableGraphic
struct  MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Slider
struct  Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillRect_20;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleRect_21;
	// UnityEngine.UI.Slider/Direction UnityEngine.UI.Slider::m_Direction
	int32_t ___m_Direction_22;
	// System.Single UnityEngine.UI.Slider::m_MinValue
	float ___m_MinValue_23;
	// System.Single UnityEngine.UI.Slider::m_MaxValue
	float ___m_MaxValue_24;
	// System.Boolean UnityEngine.UI.Slider::m_WholeNumbers
	bool ___m_WholeNumbers_25;
	// System.Single UnityEngine.UI.Slider::m_Value
	float ___m_Value_26;
	// UnityEngine.UI.Slider/SliderEvent UnityEngine.UI.Slider::m_OnValueChanged
	SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * ___m_OnValueChanged_27;
	// UnityEngine.UI.Image UnityEngine.UI.Slider::m_FillImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_FillImage_28;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_FillTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_FillTransform_29;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_FillContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_FillContainerRect_30;
	// UnityEngine.Transform UnityEngine.UI.Slider::m_HandleTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___m_HandleTransform_31;
	// UnityEngine.RectTransform UnityEngine.UI.Slider::m_HandleContainerRect
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_HandleContainerRect_32;
	// UnityEngine.Vector2 UnityEngine.UI.Slider::m_Offset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___m_Offset_33;
	// UnityEngine.DrivenRectTransformTracker UnityEngine.UI.Slider::m_Tracker
	DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  ___m_Tracker_34;
	// System.Boolean UnityEngine.UI.Slider::m_DelayedUpdateVisuals
	bool ___m_DelayedUpdateVisuals_35;

public:
	inline static int32_t get_offset_of_m_FillRect_20() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillRect_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillRect_20() const { return ___m_FillRect_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillRect_20() { return &___m_FillRect_20; }
	inline void set_m_FillRect_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillRect_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillRect_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleRect_21() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleRect_21)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleRect_21() const { return ___m_HandleRect_21; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleRect_21() { return &___m_HandleRect_21; }
	inline void set_m_HandleRect_21(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleRect_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleRect_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_Direction_22() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Direction_22)); }
	inline int32_t get_m_Direction_22() const { return ___m_Direction_22; }
	inline int32_t* get_address_of_m_Direction_22() { return &___m_Direction_22; }
	inline void set_m_Direction_22(int32_t value)
	{
		___m_Direction_22 = value;
	}

	inline static int32_t get_offset_of_m_MinValue_23() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MinValue_23)); }
	inline float get_m_MinValue_23() const { return ___m_MinValue_23; }
	inline float* get_address_of_m_MinValue_23() { return &___m_MinValue_23; }
	inline void set_m_MinValue_23(float value)
	{
		___m_MinValue_23 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_24() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_MaxValue_24)); }
	inline float get_m_MaxValue_24() const { return ___m_MaxValue_24; }
	inline float* get_address_of_m_MaxValue_24() { return &___m_MaxValue_24; }
	inline void set_m_MaxValue_24(float value)
	{
		___m_MaxValue_24 = value;
	}

	inline static int32_t get_offset_of_m_WholeNumbers_25() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_WholeNumbers_25)); }
	inline bool get_m_WholeNumbers_25() const { return ___m_WholeNumbers_25; }
	inline bool* get_address_of_m_WholeNumbers_25() { return &___m_WholeNumbers_25; }
	inline void set_m_WholeNumbers_25(bool value)
	{
		___m_WholeNumbers_25 = value;
	}

	inline static int32_t get_offset_of_m_Value_26() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Value_26)); }
	inline float get_m_Value_26() const { return ___m_Value_26; }
	inline float* get_address_of_m_Value_26() { return &___m_Value_26; }
	inline void set_m_Value_26(float value)
	{
		___m_Value_26 = value;
	}

	inline static int32_t get_offset_of_m_OnValueChanged_27() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_OnValueChanged_27)); }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * get_m_OnValueChanged_27() const { return ___m_OnValueChanged_27; }
	inline SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 ** get_address_of_m_OnValueChanged_27() { return &___m_OnValueChanged_27; }
	inline void set_m_OnValueChanged_27(SliderEvent_t312D89AE02E00DD965D68D6F7F813BDF455FD780 * value)
	{
		___m_OnValueChanged_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillImage_28() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillImage_28)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_FillImage_28() const { return ___m_FillImage_28; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_FillImage_28() { return &___m_FillImage_28; }
	inline void set_m_FillImage_28(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_FillImage_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillImage_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillTransform_29() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillTransform_29)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_FillTransform_29() const { return ___m_FillTransform_29; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_FillTransform_29() { return &___m_FillTransform_29; }
	inline void set_m_FillTransform_29(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_FillTransform_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillTransform_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_FillContainerRect_30() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_FillContainerRect_30)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_FillContainerRect_30() const { return ___m_FillContainerRect_30; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_FillContainerRect_30() { return &___m_FillContainerRect_30; }
	inline void set_m_FillContainerRect_30(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_FillContainerRect_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_FillContainerRect_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleTransform_31() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleTransform_31)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_m_HandleTransform_31() const { return ___m_HandleTransform_31; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_m_HandleTransform_31() { return &___m_HandleTransform_31; }
	inline void set_m_HandleTransform_31(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___m_HandleTransform_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleTransform_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_HandleContainerRect_32() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_HandleContainerRect_32)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_HandleContainerRect_32() const { return ___m_HandleContainerRect_32; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_HandleContainerRect_32() { return &___m_HandleContainerRect_32; }
	inline void set_m_HandleContainerRect_32(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_HandleContainerRect_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HandleContainerRect_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_Offset_33() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Offset_33)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_m_Offset_33() const { return ___m_Offset_33; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_m_Offset_33() { return &___m_Offset_33; }
	inline void set_m_Offset_33(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___m_Offset_33 = value;
	}

	inline static int32_t get_offset_of_m_Tracker_34() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_Tracker_34)); }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  get_m_Tracker_34() const { return ___m_Tracker_34; }
	inline DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2 * get_address_of_m_Tracker_34() { return &___m_Tracker_34; }
	inline void set_m_Tracker_34(DrivenRectTransformTracker_t7DAF937E47C63B899C7BA0E9B0F206AAB4D85AC2  value)
	{
		___m_Tracker_34 = value;
	}

	inline static int32_t get_offset_of_m_DelayedUpdateVisuals_35() { return static_cast<int32_t>(offsetof(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A, ___m_DelayedUpdateVisuals_35)); }
	inline bool get_m_DelayedUpdateVisuals_35() const { return ___m_DelayedUpdateVisuals_35; }
	inline bool* get_address_of_m_DelayedUpdateVisuals_35() { return &___m_DelayedUpdateVisuals_35; }
	inline void set_m_DelayedUpdateVisuals_35(bool value)
	{
		___m_DelayedUpdateVisuals_35 = value;
	}
};


// UnityEngine.UI.Image
struct  Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared (const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared (RuntimeObject * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method);

// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponentInChildren_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m654193278BB56948AB820313E9D35E6D6A89798F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<PlayerManager>()
inline PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * Component_GetComponent_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m58B42920D67789AB6096BAB8D957FB64B4DDAA16 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<PlayerLocomotion>()
inline PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668 (String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetBool(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43 (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, bool ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Animator::CrossFade(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_CrossFade_mD3F99D6835AA415C0B32AE0C574B1815CC07586F (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___stateName0, float ___normalizedTransitionDuration1, const RuntimeMethod* method);
// System.Single UnityEngine.Time::get_deltaTime()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290 (const RuntimeMethod* method);
// System.Void UnityEngine.Animator::SetFloat(System.Int32,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Animator_SetFloat_mB6D249371976254CDD71F4205E541945C66F887B (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, int32_t ___id0, float ___value1, float ___dampTime2, float ___deltaTime3, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_drag(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_drag_m60E39BE31529DE5163116785A69FACC77C52DA98 (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, float ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Animator::get_deltaPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Animator_get_deltaPosition_m064ACBB4845CFE50050B838DC5F7ADD98E7C38AD (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<PlayerManager>()
inline PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * Object_FindObjectOfType_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m14B304841D56F2822E0E096CB90F01FAFEFC2877 (const RuntimeMethod* method)
{
	return ((  PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared)(method);
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::FindObjectOfType<InputManager>()
inline InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * Object_FindObjectOfType_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m49DE97E09E1BC5C2FF6DCD755C601B4B730E2B42 (const RuntimeMethod* method)
{
	return ((  InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared)(method);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void CameraManager::SwitchCameras(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, String_t* ___cameraName0, const RuntimeMethod* method);
// System.Void UnityEngine.Cursor::set_lockState(UnityEngine.CursorLockMode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217 (int32_t ___value0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void CameraManager::HandleShoulderCam()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_HandleShoulderCam_mD187D8FB9D6CD4C27FF02E76F8C1843D1D5E91BB (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method);
// System.Void CameraManager::HandleTopDownCam()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_HandleTopDownCam_m9F9BE9182993AABDE5F0B9B0E97E0E42B05EA8C7 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method);
// System.Void CameraManager::FollowTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_FollowTarget_m3B321F6088D72138EB27285283F8E849613ED10E (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method);
// System.Void CameraManager::RotateCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_RotateCamera_m2CDBD3C9BA768CE220369FD4956F27583D5D7066 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method);
// System.Void CameraManager::HandleCameraCollisions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_HandleCameraCollisions_mBC0FF6768EBC02EDDE8575444230061EDEC7C2FA (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::SmoothDamp(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_SmoothDamp_m4655944DBD5B44125F8F4B5A15E31DE94CB0F627 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___current0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * ___currentVelocity2, float ___smoothTime3, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87 (float ___value0, float ___min1, float ___max2, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_zero()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6 (const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___euler0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::Normalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Int32 UnityEngine.LayerMask::op_Implicit(UnityEngine.LayerMask)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0 (LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  ___mask0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m9943744EA02A42417719F4A0CBE5822DA12F15BC (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, float ___radius1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction2, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo3, float ___maxDistance4, int32_t ___layerMask5, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Vector3::Distance(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Lerp(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616 (float ___a0, float ___b1, float ___t2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// UnityEngine.GameObject CameraManager::GetCurrentCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * CameraManager_GetCurrentCamera_m69A5FB8797F2209A65B33A8179AD7925F5ECCDD6 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3 (float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_eulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKey(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429 (int32_t ___key0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1 (int32_t ___button0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxis(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326 (String_t* ___axisName0, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_eulerAngles(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B (String_t* ___axisName0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Translate_mC9343E1E646DA8FD42BE37137ACCBB4B52093F5C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Translate(System.Single,System.Single,System.Single,UnityEngine.Space)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Translate_m22737F202DE67AAAAC408ADE91BD44F5BAF3DD6B (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, float ___x0, float ___y1, float ___z2, int32_t ___relativeTo3, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Animator>()
inline Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411 (int32_t* __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B (String_t* ___str00, String_t* ___str11, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___exists0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<GravityOrbit>()
inline GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * Component_GetComponent_TisGravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2_m7275FAA26EDDE37A27FC6C9AB643DF22D79B8789 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<AnimationManager>()
inline AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * Component_GetComponent_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m76E4330A66259F770A0F73017C967C9EFAFE9993 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void InputManager::ReadInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_ReadInput_mCC0FE5A9B269D19F1A7A3ED6BC6D3969D1076801 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method);
// System.Void InputManager::HandleMovementInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleMovementInput_mBCE591A731DBF1E30D86801664DFA838CA788D79 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method);
// System.Void InputManager::HandleSprintingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleSprintingInput_m7DFEE167DC1AA1463421F97619E8F28438FD8BFB (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method);
// System.Void InputManager::HandleJumpingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleJumpingInput_m5CBBCEB3CA4F46A490AB6A66A081BA59FF55337D (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method);
// System.Void InputManager::HandleShootInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleShootInput_m3C3B262CA06D3DD5A68CD85FB7C33CA7A5E0A7F0 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method);
// System.Void InputManager::HandleAimInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleAimInput_mCD4043BE6196AE2B6C067B3F05C25AEFAD928ECC (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector2::.ctor(System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButton(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButton_m95EE8314087068F3AA9CEF3C3F6A246D55C4734C (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Single UnityEngine.Mathf::Clamp01(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C (float ___value0, const RuntimeMethod* method);
// System.Void AnimationManager::UpdateAnimator(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationManager_UpdateAnimator_m3F09B8D6E2E97AF94A0296FF32F5F23AEF011D94 (AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * __this, float ___horizontalMovement0, float ___verticalMovement1, const RuntimeMethod* method);
// System.Void PlayerLocomotion::HandleJump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleJump_m067C8500AFA029CCFC1A96A37B10A6A05B7FF88C (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method);
// System.Void PlayerLocomotion::HandleShoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleShoot_mC851DE55C6118DBCF3AC0CDEC10DAD7DF6A3A23D (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method);
// System.Void PlayerLocomotion::HandleAim()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleAim_m332863775AA4450513DA1775494A88D6F571EA27 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetButtonUp(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetButtonUp_m15AA6B42BD0DDCC7802346E49F30653D750260DD (String_t* ___buttonName0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetKeyDown(UnityEngine.KeyCode)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220 (int32_t ___key0, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.GameObject::FindGameObjectWithTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F (String_t* ___tag0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.SceneManagement.SceneManager::LoadScene(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5 (int32_t ___sceneBuildIndex0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Component::CompareTag(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Component_CompareTag_m17D74EDCC81A10B18A0A588519F522E8DF1D7879 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, String_t* ___tag0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::get_parent()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50 (const RuntimeMethod* method);
// System.Void UnityEngine.Transform::RotateAround(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis1, float ___angle2, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0,UnityEngine.Vector3,UnityEngine.Quaternion)
inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___original0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___position1, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rotation2, const RuntimeMethod* method)
{
	return ((  GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E , Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 , const RuntimeMethod*))Object_Instantiate_TisRuntimeObject_mB05DEC51C29EF5BB8BD17D055E80217F11E571AA_gshared)(___original0, ___position1, ___rotation2, method);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_mAAAA103F4911E9FA18634BF9605C28559F5E2AC7 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___obj0, float ___t1, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<InputManager>()
inline InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * Component_GetComponent_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m6CCCFD6651416AD5F042C75E8B8CF01C82631CA9 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponentInChildren<AnimationManager>()
inline AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * Component_GetComponentInChildren_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m67C66D5AA4AA16A3448B21AF2EC03FFEAD4F03B9 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponentInChildren_TisRuntimeObject_mB377B32275A969E0D1A738DBC693DE8EB3593642_gshared)(__this, method);
}
// !!0 UnityEngine.Component::GetComponent<ChargeManager>()
inline ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * Component_GetComponent_TisChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0_m14E499548B982EF24D66C845F258B527D0319A41 (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// !!0 UnityEngine.Object::FindObjectOfType<CameraManager>()
inline CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * Object_FindObjectOfType_TisCameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_m1BDBA1D013C9E2C431C864109ADEA485E4716CF2 (const RuntimeMethod* method)
{
	return ((  CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * (*) (const RuntimeMethod*))Object_FindObjectOfType_TisRuntimeObject_m25AA6DB6AABFD5D66AFA1A8C0E91A7AF61429C37_gshared)(method);
}
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void PlayerLocomotion::HandleGravity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleGravity_m945C350309BD0A0A68A43DA7F145D45FCE41089E (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method);
// System.Void PlayerLocomotion::HandleFallingAndLanding()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleFallingAndLanding_m6CE12DB3EA39F414944DE9E69BC427D9B9269F6E (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method);
// System.Void PlayerLocomotion::HandleMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleMovement_m7DF7D7FE2A854C2DE4D9C275CFEC16E5432ED84D (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method);
// System.Void PlayerLocomotion::HandleRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleRotation_mD5676E3D897F7D7943BD5FA06043F9224DCABB50 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_normalized()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Inequality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// System.Void PlayerLocomotion::RotateOnPlanet(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_RotateOnPlanet_m3E3C7A8C083C82F729DDBC075D4F61397E51CB61 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___MoveDirection0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Translate(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___translation0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_right()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Addition(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 PlayerLocomotion::ChangeSpeed(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  PlayerLocomotion_ChangeSpeed_mEB80118161B66A84693381F16D6BAA3E58B4DC89 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Rigidbody::get_velocity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::set_velocity(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Vector3::op_Equality(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lhs0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rhs1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::LookRotation(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forward0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Slerp(UnityEngine.Quaternion,UnityEngine.Quaternion,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Slerp_m6D2BD18286254E28D2288B51962EC71F85C7B5C8 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___a0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___b1, float ___t2, const RuntimeMethod* method);
// System.Void AnimationManager::PlayTargetAnimation(System.String,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationManager_PlayTargetAnimation_m47BDB6536609430A331D8E9D0FBFCC56F1DD9DEE (AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * __this, String_t* ___targetAnimation0, bool ___isInteracting1, bool ___useRootMotion2, const RuntimeMethod* method);
// System.Void UnityEngine.Rigidbody::AddForce(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rigidbody_AddForce_mDFB0D57C25682B826999B0074F5C0FD399C6401D (Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___force0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_UnaryNegation(UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::SphereCast(UnityEngine.Vector3,System.Single,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_SphereCast_m2E200DFD71A597968D4F50B8A6284F2E622B4C57 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, float ___radius1, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction2, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo3, float ___maxDistance4, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<PlayerEffectManager>()
inline PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * Component_GetComponent_TisPlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D_mE88BF86CEFAE820AB756B8FACF1C9CF3E50142AA (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method)
{
	return ((  PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * (*) (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m69D9C576D6DD024C709E29EEADBC8041299A3AA7_gshared)(__this, method);
}
// System.Void PlayerEffectManager::PlayLandEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerEffectManager_PlayLandEffect_m926080EC179C8C53EF8EB69F9BD22B1C3C3174C3 (PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___p0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_up()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::FromToRotation(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_FromToRotation_mD0EBB9993FC7C6A45724D0365B09F11F1CEADB80 (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___fromDirection0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___toDirection1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___lhs0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rhs1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___origin0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction1, RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * ___hitInfo2, float ___maxDistance3, int32_t ___layerMask4, const RuntimeMethod* method);
// System.Collections.IEnumerator PlayerLocomotion::ShootTowards(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlayerLocomotion_ShootTowards_mB986A2FD3364C113CDDAA80D800C4B812976E6B3 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, const RuntimeMethod* method);
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719 (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, RuntimeObject* ___routine0, const RuntimeMethod* method);
// System.Boolean System.String::op_Inequality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2 (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, int32_t ___index0, const RuntimeMethod* method);
// System.Void PlayerLocomotion/<ShootTowards>d__37::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CShootTowardsU3Ed__37__ctor_mE9BC491319E93765A987C60044EB75C14949AD81 (U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method);
// System.Void InputManager::HandleAllInputs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleAllInputs_mCDF44B0217785B2D7D4D6702B18DC531D7B0FC75 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method);
// System.Void ChargeManager::UpdateChargeCounter(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChargeManager_UpdateChargeCounter_m55E422F13E6183B524F031B4455E9374F0B8CB8F (ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * __this, bool ___isGrounded0, const RuntimeMethod* method);
// System.Void PlayerLocomotion::HandleAllMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleAllMovement_m3AA12244C0EBE67CD8D4A92D077D8058849BD2F1 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method);
// System.Void CameraManager::HandleAllCameraMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_HandleAllCameraMovement_m58737EA7EFF6EFEECC7F56CB66B7254E5BBB48F7 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Animator::GetBool(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Animator_GetBool_m69AFEA8176E7FB312C264773784D6D6B08A80C0A (Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.StateMachineBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void StateMachineBehaviour__ctor_mDB0650FD738799E5880150E656D4A88524D0EBE0 (StateMachineBehaviour_tBEDE439261DEB4C7334646339BC6F1E7958F095F * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Transform::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, String_t* ___n0, const RuntimeMethod* method);
// System.Single Rotatator::RandFloat()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rotatator_RandFloat_m2C7FA1F9E460B203C0185389414813034AF1103F (Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D * __this, const RuntimeMethod* method);
// System.Single UnityEngine.Random::Range(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2 (float ___minInclusive0, float ___maxInclusive1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(UnityEngine.Vector3,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___axis0, float ___angle1, const RuntimeMethod* method);
// System.Void SliderScripts::FillSlider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SliderScripts_FillSlider_m427E4FF54E88039DF974427DED78E5DCC46439C6 (SliderScripts_t01CD33323C1390AABFEA3597355DD017703D404F * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_fillAmount(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, float ___value0, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void PlayerEffectManager::ToggleShootingEffects(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerEffectManager_ToggleShootingEffects_m3091CE05E2C37AF30BF81F8DAA06E40B5A8FCE85 (PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * __this, bool ___isShooting0, const RuntimeMethod* method);
// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4 (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * __this, float ___seconds0, const RuntimeMethod* method);
// System.Single UnityEngine.RaycastHit::get_distance()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 * __this, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_localEulerAngles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AnimationManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationManager_Awake_m627DE07938651E02F0A44463872756F99CEBE1A0 (AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m654193278BB56948AB820313E9D35E6D6A89798F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m58B42920D67789AB6096BAB8D957FB64B4DDAA16_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator = GetComponentInChildren<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0;
		L_0 = Component_GetComponentInChildren_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m654193278BB56948AB820313E9D35E6D6A89798F(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m654193278BB56948AB820313E9D35E6D6A89798F_RuntimeMethod_var);
		__this->set_animator_4(L_0);
		// playerManager = GetComponent<PlayerManager>();
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_1;
		L_1 = Component_GetComponent_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m58B42920D67789AB6096BAB8D957FB64B4DDAA16(__this, /*hidden argument*/Component_GetComponent_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m58B42920D67789AB6096BAB8D957FB64B4DDAA16_RuntimeMethod_var);
		__this->set_playerManager_5(L_1);
		// playerLocomotion = GetComponent<PlayerLocomotion>();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_2;
		L_2 = Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5(__this, /*hidden argument*/Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		__this->set_playerLocomotion_6(L_2);
		// horizontal = Animator.StringToHash("Horizontal");
		int32_t L_3;
		L_3 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		__this->set_horizontal_7(L_3);
		// vertical = Animator.StringToHash("Vertical");
		int32_t L_4;
		L_4 = Animator_StringToHash_mA351F39D53E2AEFCF0BBD50E4FA92B7E1C99A668(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, /*hidden argument*/NULL);
		__this->set_vertical_8(L_4);
		// }
		return;
	}
}
// System.Void AnimationManager::PlayTargetAnimation(System.String,System.Boolean,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationManager_PlayTargetAnimation_m47BDB6536609430A331D8E9D0FBFCC56F1DD9DEE (AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * __this, String_t* ___targetAnimation0, bool ___isInteracting1, bool ___useRootMotion2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral54ADB4B256E220AC1660EEB0B81E40D259DD1203);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1915CC31B6BC62A90E9FD4F876A00AA4B73C4ED);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.SetBool("isInteracting", isInteracting);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = __this->get_animator_4();
		bool L_1 = ___isInteracting1;
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_0, _stringLiteral54ADB4B256E220AC1660EEB0B81E40D259DD1203, L_1, /*hidden argument*/NULL);
		// animator.SetBool("isUsingRootMotion", useRootMotion);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2 = __this->get_animator_4();
		bool L_3 = ___useRootMotion2;
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_2, _stringLiteralB1915CC31B6BC62A90E9FD4F876A00AA4B73C4ED, L_3, /*hidden argument*/NULL);
		// animator.CrossFade(targetAnimation, 0.1f);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4 = __this->get_animator_4();
		String_t* L_5 = ___targetAnimation0;
		Animator_CrossFade_mD3F99D6835AA415C0B32AE0C574B1815CC07586F(L_4, L_5, (0.100000001f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AnimationManager::UpdateAnimator(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationManager_UpdateAnimator_m3F09B8D6E2E97AF94A0296FF32F5F23AEF011D94 (AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * __this, float ___horizontalMovement0, float ___verticalMovement1, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		// if (horizontalMovement > 0 && horizontalMovement < 0.55f)
		float L_0 = ___horizontalMovement0;
		if ((!(((float)L_0) > ((float)(0.0f)))))
		{
			goto IL_0018;
		}
	}
	{
		float L_1 = ___horizontalMovement0;
		if ((!(((float)L_1) < ((float)(0.550000012f)))))
		{
			goto IL_0018;
		}
	}
	{
		// snappedHorizontal = 0.5f;
		V_0 = (0.5f);
		// }
		goto IL_0056;
	}

IL_0018:
	{
		// else if (horizontalMovement > 0.55f)
		float L_2 = ___horizontalMovement0;
		if ((!(((float)L_2) > ((float)(0.550000012f)))))
		{
			goto IL_0028;
		}
	}
	{
		// snappedHorizontal = 1;
		V_0 = (1.0f);
		// }
		goto IL_0056;
	}

IL_0028:
	{
		// else if (horizontalMovement < 0 && horizontalMovement > -0.55f)
		float L_3 = ___horizontalMovement0;
		if ((!(((float)L_3) < ((float)(0.0f)))))
		{
			goto IL_0040;
		}
	}
	{
		float L_4 = ___horizontalMovement0;
		if ((!(((float)L_4) > ((float)(-0.550000012f)))))
		{
			goto IL_0040;
		}
	}
	{
		// snappedHorizontal = -0.5f;
		V_0 = (-0.5f);
		// }
		goto IL_0056;
	}

IL_0040:
	{
		// else if (horizontalMovement < -0.55f)
		float L_5 = ___horizontalMovement0;
		if ((!(((float)L_5) < ((float)(-0.550000012f)))))
		{
			goto IL_0050;
		}
	}
	{
		// snappedHorizontal = -1;
		V_0 = (-1.0f);
		// }
		goto IL_0056;
	}

IL_0050:
	{
		// snappedHorizontal = 0;
		V_0 = (0.0f);
	}

IL_0056:
	{
		// if (verticalMovement > 0 && verticalMovement < 0.55f)
		float L_6 = ___verticalMovement1;
		if ((!(((float)L_6) > ((float)(0.0f)))))
		{
			goto IL_006e;
		}
	}
	{
		float L_7 = ___verticalMovement1;
		if ((!(((float)L_7) < ((float)(0.550000012f)))))
		{
			goto IL_006e;
		}
	}
	{
		// snappedVertical = 0.5f;
		V_1 = (0.5f);
		// }
		goto IL_00ac;
	}

IL_006e:
	{
		// else if (verticalMovement > 0.55f)
		float L_8 = ___verticalMovement1;
		if ((!(((float)L_8) > ((float)(0.550000012f)))))
		{
			goto IL_007e;
		}
	}
	{
		// snappedVertical = 1;
		V_1 = (1.0f);
		// }
		goto IL_00ac;
	}

IL_007e:
	{
		// else if (verticalMovement < 0 && verticalMovement > -0.55f)
		float L_9 = ___verticalMovement1;
		if ((!(((float)L_9) < ((float)(0.0f)))))
		{
			goto IL_0096;
		}
	}
	{
		float L_10 = ___verticalMovement1;
		if ((!(((float)L_10) > ((float)(-0.550000012f)))))
		{
			goto IL_0096;
		}
	}
	{
		// snappedVertical = -0.5f;
		V_1 = (-0.5f);
		// }
		goto IL_00ac;
	}

IL_0096:
	{
		// else if (verticalMovement < -0.55f)
		float L_11 = ___verticalMovement1;
		if ((!(((float)L_11) < ((float)(-0.550000012f)))))
		{
			goto IL_00a6;
		}
	}
	{
		// snappedVertical = -1;
		V_1 = (-1.0f);
		// }
		goto IL_00ac;
	}

IL_00a6:
	{
		// snappedVertical = 0;
		V_1 = (0.0f);
	}

IL_00ac:
	{
		// animator.SetFloat(horizontal, snappedHorizontal, 0.1f, Time.deltaTime);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_12 = __this->get_animator_4();
		int32_t L_13 = __this->get_horizontal_7();
		float L_14 = V_0;
		float L_15;
		L_15 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Animator_SetFloat_mB6D249371976254CDD71F4205E541945C66F887B(L_12, L_13, L_14, (0.100000001f), L_15, /*hidden argument*/NULL);
		// animator.SetFloat(vertical, snappedVertical, 0.1f, Time.deltaTime);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_16 = __this->get_animator_4();
		int32_t L_17 = __this->get_vertical_8();
		float L_18 = V_1;
		float L_19;
		L_19 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Animator_SetFloat_mB6D249371976254CDD71F4205E541945C66F887B(L_16, L_17, L_18, (0.100000001f), L_19, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void AnimationManager::OnAnimatorMove()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationManager_OnAnimatorMove_mAC9F008B4AD7DF4D2A31F87D4D9044CED4677F00 (AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * __this, const RuntimeMethod* method)
{
	{
		// if (playerManager.isUsingRootMotion)
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_0 = __this->get_playerManager_5();
		bool L_1 = L_0->get_isUsingRootMotion_11();
		if (!L_1)
		{
			goto IL_002e;
		}
	}
	{
		// playerLocomotion.rb.drag = 0;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_2 = __this->get_playerLocomotion_6();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_3 = L_2->get_rb_11();
		Rigidbody_set_drag_m60E39BE31529DE5163116785A69FACC77C52DA98(L_3, (0.0f), /*hidden argument*/NULL);
		// Vector3 deltaPosition = animator.deltaPosition;
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_4 = __this->get_animator_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Animator_get_deltaPosition_m064ACBB4845CFE50050B838DC5F7ADD98E7C38AD(L_4, /*hidden argument*/NULL);
	}

IL_002e:
	{
		// }
		return;
	}
}
// System.Void AnimationManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AnimationManager__ctor_m0C4622326DC614221E503860BBCC73F91BFB9F20 (AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_Awake_m9FA8A0FE2F4DE2E70076A4A15C9885BE5E4775A4 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m49DE97E09E1BC5C2FF6DCD755C601B4B730E2B42_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m14B304841D56F2822E0E096CB90F01FAFEFC2877_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// targetTransform = FindObjectOfType<PlayerManager>().transform;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_0;
		L_0 = Object_FindObjectOfType_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m14B304841D56F2822E0E096CB90F01FAFEFC2877(/*hidden argument*/Object_FindObjectOfType_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m14B304841D56F2822E0E096CB90F01FAFEFC2877_RuntimeMethod_var);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		__this->set_targetTransform_5(L_1);
		// inputManager = FindObjectOfType<InputManager>();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_2;
		L_2 = Object_FindObjectOfType_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m49DE97E09E1BC5C2FF6DCD755C601B4B730E2B42(/*hidden argument*/Object_FindObjectOfType_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m49DE97E09E1BC5C2FF6DCD755C601B4B730E2B42_RuntimeMethod_var);
		__this->set_inputManager_4(L_2);
		// }
		return;
	}
}
// System.Void CameraManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_Start_m3A8F66B836B9A9BC419B62EF88451549BBC9CCD0 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral440FACB401E8D64062B80C3C27A6A7C79F132229);
		s_Il2CppMethodInitialized = true;
	}
	{
		// cameraTransform = Camera.main.transform;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_0;
		L_0 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_0, /*hidden argument*/NULL);
		__this->set_cameraTransform_7(L_1);
		// defaultPosition = cameraTransform.localPosition.z;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_cameraTransform_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2(L_2, /*hidden argument*/NULL);
		float L_4 = L_3.get_z_4();
		__this->set_defaultPosition_9(L_4);
		// SwitchCameras("FollowCam");
		CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05(__this, _stringLiteral440FACB401E8D64062B80C3C27A6A7C79F132229, /*hidden argument*/NULL);
		// Cursor.lockState = CursorLockMode.Locked;
		Cursor_set_lockState_mC0739186A04F4C278F02E8C1714D99B491E3A217(1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CameraManager::HandleAllCameraMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_HandleAllCameraMovement_m58737EA7EFF6EFEECC7F56CB66B7254E5BBB48F7 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1963A02C9DEBD760AC7D980330C0CC094F7B3211);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9E4CF29136E2EF978D3759CB6AAA6935843FC102);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (currentCamera == "ShoulderCam")
		String_t* L_0 = __this->get_currentCamera_27();
		bool L_1;
		L_1 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_0, _stringLiteral1963A02C9DEBD760AC7D980330C0CC094F7B3211, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		// HandleShoulderCam();
		CameraManager_HandleShoulderCam_mD187D8FB9D6CD4C27FF02E76F8C1843D1D5E91BB(__this, /*hidden argument*/NULL);
		// } else if (currentCamera == "TopDownCam")
		return;
	}

IL_0019:
	{
		// } else if (currentCamera == "TopDownCam")
		String_t* L_2 = __this->get_currentCamera_27();
		bool L_3;
		L_3 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_2, _stringLiteral9E4CF29136E2EF978D3759CB6AAA6935843FC102, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0032;
		}
	}
	{
		// HandleTopDownCam();
		CameraManager_HandleTopDownCam_m9F9BE9182993AABDE5F0B9B0E97E0E42B05EA8C7(__this, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0032:
	{
		// FollowTarget();
		CameraManager_FollowTarget_m3B321F6088D72138EB27285283F8E849613ED10E(__this, /*hidden argument*/NULL);
		// RotateCamera();
		CameraManager_RotateCamera_m2CDBD3C9BA768CE220369FD4956F27583D5D7066(__this, /*hidden argument*/NULL);
		// HandleCameraCollisions();
		CameraManager_HandleCameraCollisions_mBC0FF6768EBC02EDDE8575444230061EDEC7C2FA(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CameraManager::FollowTarget()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_FollowTarget_m3B321F6088D72138EB27285283F8E849613ED10E (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// Vector3 targetPosition = Vector3.SmoothDamp(transform.position, targetTransform.position, ref cameraFollowVelocity, cameraFollowspeed);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_targetTransform_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_4 = __this->get_address_of_cameraFollowVelocity_10();
		float L_5 = __this->get_cameraFollowspeed_15();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		L_6 = Vector3_SmoothDamp_m4655944DBD5B44125F8F4B5A15E31DE94CB0F627(L_1, L_3, (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_4, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		// transform.position = targetPosition;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = V_0;
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_7, L_8, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CameraManager::RotateCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_RotateCamera_m2CDBD3C9BA768CE220369FD4956F27583D5D7066 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// lookAngle = lookAngle + (inputManager.cameraInputX * cameraLookSpeed);
		float L_0 = __this->get_lookAngle_18();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_1 = __this->get_inputManager_4();
		float L_2 = L_1->get_cameraInputX_11();
		float L_3 = __this->get_cameraLookSpeed_16();
		__this->set_lookAngle_18(((float)il2cpp_codegen_add((float)L_0, (float)((float)il2cpp_codegen_multiply((float)L_2, (float)L_3)))));
		// pivotAngle = pivotAngle - (inputManager.cameraInputY * cameraPivotSpeed);
		float L_4 = __this->get_pivotAngle_19();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_5 = __this->get_inputManager_4();
		float L_6 = L_5->get_cameraInputY_12();
		float L_7 = __this->get_cameraPivotSpeed_17();
		__this->set_pivotAngle_19(((float)il2cpp_codegen_subtract((float)L_4, (float)((float)il2cpp_codegen_multiply((float)L_6, (float)L_7)))));
		// pivotAngle = Mathf.Clamp(pivotAngle, minimumPivotAngle, maximumPivotAngle);
		float L_8 = __this->get_pivotAngle_19();
		float L_9 = __this->get_minimumPivotAngle_20();
		float L_10 = __this->get_maximumPivotAngle_21();
		float L_11;
		L_11 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_8, L_9, L_10, /*hidden argument*/NULL);
		__this->set_pivotAngle_19(L_11);
		// rotation = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_0 = L_12;
		// rotation.y = lookAngle;
		float L_13 = __this->get_lookAngle_18();
		(&V_0)->set_y_3(L_13);
		// targetRotation = Quaternion.Euler(rotation);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14 = V_0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_15;
		L_15 = Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB(L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		// transform.rotation = targetRotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_17 = V_1;
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_16, L_17, /*hidden argument*/NULL);
		// rotation = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_0 = L_18;
		// rotation.x = pivotAngle;
		float L_19 = __this->get_pivotAngle_19();
		(&V_0)->set_x_2(L_19);
		// targetRotation = Quaternion.Euler(rotation);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = V_0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_21;
		L_21 = Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB(L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		// cameraPivot.localRotation = targetRotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22 = __this->get_cameraPivot_6();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_23 = V_1;
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_22, L_23, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CameraManager::HandleCameraCollisions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_HandleCameraCollisions_mBC0FF6768EBC02EDDE8575444230061EDEC7C2FA (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// float targetPosition = defaultPosition;
		float L_0 = __this->get_defaultPosition_9();
		V_0 = L_0;
		// Vector3 direction = cameraTransform.position - cameraPivot.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1 = __this->get_cameraTransform_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_cameraPivot_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_2, L_4, /*hidden argument*/NULL);
		V_2 = L_5;
		// direction.Normalize();
		Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), /*hidden argument*/NULL);
		// if (Physics.SphereCast(cameraPivot.transform.position, cameraCollisionRadius, direction, out hit, Mathf.Abs(targetPosition), collisionLayers))
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = __this->get_cameraPivot_6();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_cameraCollisionRadius_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_2;
		float L_11 = V_0;
		float L_12;
		L_12 = fabsf(L_11);
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_13 = __this->get_collisionLayers_8();
		int32_t L_14;
		L_14 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_13, /*hidden argument*/NULL);
		bool L_15;
		L_15 = Physics_SphereCast_m9943744EA02A42417719F4A0CBE5822DA12F15BC(L_8, L_9, L_10, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_1), L_12, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007b;
		}
	}
	{
		// float distance = Vector3.Distance(cameraPivot.position, hit.point);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16 = __this->get_cameraPivot_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_16, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_1), /*hidden argument*/NULL);
		float L_19;
		L_19 = Vector3_Distance_mB648A79E4A1BAAFBF7B029644638C0D715480677(L_17, L_18, /*hidden argument*/NULL);
		// targetPosition = -(distance - cameraCollisionOffset);
		float L_20 = __this->get_cameraCollisionOffset_12();
		V_0 = ((-((float)il2cpp_codegen_subtract((float)L_19, (float)L_20))));
	}

IL_007b:
	{
		// if (Mathf.Abs(targetPosition) < minimumCollisionOffset)
		float L_21 = V_0;
		float L_22;
		L_22 = fabsf(L_21);
		float L_23 = __this->get_minimumCollisionOffset_13();
		if ((!(((float)L_22) < ((float)L_23))))
		{
			goto IL_0092;
		}
	}
	{
		// targetPosition = targetPosition - minimumCollisionOffset;
		float L_24 = V_0;
		float L_25 = __this->get_minimumCollisionOffset_13();
		V_0 = ((float)il2cpp_codegen_subtract((float)L_24, (float)L_25));
	}

IL_0092:
	{
		// cameraVectorPosition.z = Mathf.Lerp(cameraTransform.localPosition.z, targetPosition, 0.2f);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_26 = __this->get_address_of_cameraVectorPosition_11();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27 = __this->get_cameraTransform_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Transform_get_localPosition_m527B8B5B625DA9A61E551E0FBCD3BE8CA4539FC2(L_27, /*hidden argument*/NULL);
		float L_29 = L_28.get_z_4();
		float L_30 = V_0;
		float L_31;
		L_31 = Mathf_Lerp_m8A2A50B945F42D579EDF44D5EE79E85A4DA59616(L_29, L_30, (0.200000003f), /*hidden argument*/NULL);
		L_26->set_z_4(L_31);
		// cameraTransform.localPosition = cameraVectorPosition;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_32 = __this->get_cameraTransform_7();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33 = __this->get_cameraVectorPosition_11();
		Transform_set_localPosition_m2A2B0033EF079077FAE7C65196078EAF5D041AFC(L_32, L_33, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CameraManager::HandleShoulderCam()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_HandleShoulderCam_mD187D8FB9D6CD4C27FF02E76F8C1843D1D5E91BB (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	{
		// GameObject camera = GetCurrentCamera();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = CameraManager_GetCurrentCamera_m69A5FB8797F2209A65B33A8179AD7925F5ECCDD6(__this, /*hidden argument*/NULL);
		// lookAngle = lookAngle + inputManager.cameraInputX * cameraPivotSpeed;
		float L_1 = __this->get_lookAngle_18();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_2 = __this->get_inputManager_4();
		float L_3 = L_2->get_cameraInputX_11();
		float L_4 = __this->get_cameraPivotSpeed_17();
		__this->set_lookAngle_18(((float)il2cpp_codegen_add((float)L_1, (float)((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)))));
		// pivotAngle = pivotAngle - inputManager.cameraInputY * cameraLookSpeed;
		float L_5 = __this->get_pivotAngle_19();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_6 = __this->get_inputManager_4();
		float L_7 = L_6->get_cameraInputY_12();
		float L_8 = __this->get_cameraLookSpeed_16();
		__this->set_pivotAngle_19(((float)il2cpp_codegen_subtract((float)L_5, (float)((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)))));
		// lookAngle = Mathf.Clamp(lookAngle, minimumPivotAngle * 2, maximumPivotAngle * 2);
		float L_9 = __this->get_lookAngle_18();
		float L_10 = __this->get_minimumPivotAngle_20();
		float L_11 = __this->get_maximumPivotAngle_21();
		float L_12;
		L_12 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_9, ((float)il2cpp_codegen_multiply((float)L_10, (float)(2.0f))), ((float)il2cpp_codegen_multiply((float)L_11, (float)(2.0f))), /*hidden argument*/NULL);
		__this->set_lookAngle_18(L_12);
		// pivotAngle = Mathf.Clamp(pivotAngle, shoulderMinimumPivotAngle, shoulderMaximumPivotAngle);
		float L_13 = __this->get_pivotAngle_19();
		float L_14 = __this->get_shoulderMinimumPivotAngle_22();
		float L_15 = __this->get_shoulderMaximumPivotAngle_23();
		float L_16;
		L_16 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_13, L_14, L_15, /*hidden argument*/NULL);
		__this->set_pivotAngle_19(L_16);
		// camera.transform.localRotation = Quaternion.Euler(pivotAngle, lookAngle, 0f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		float L_18 = __this->get_pivotAngle_19();
		float L_19 = __this->get_lookAngle_18();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_20;
		L_20 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3(L_18, L_19, (0.0f), /*hidden argument*/NULL);
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_17, L_20, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CameraManager::HandleTopDownCam()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_HandleTopDownCam_m9F9BE9182993AABDE5F0B9B0E97E0E42B05EA8C7 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	{
		// GameObject camera = GetCurrentCamera();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = CameraManager_GetCurrentCamera_m69A5FB8797F2209A65B33A8179AD7925F5ECCDD6(__this, /*hidden argument*/NULL);
		// lookAngle = lookAngle + inputManager.cameraInputX * cameraPivotSpeed;
		float L_1 = __this->get_lookAngle_18();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_2 = __this->get_inputManager_4();
		float L_3 = L_2->get_cameraInputX_11();
		float L_4 = __this->get_cameraPivotSpeed_17();
		__this->set_lookAngle_18(((float)il2cpp_codegen_add((float)L_1, (float)((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)))));
		// pivotAngle = pivotAngle - inputManager.cameraInputY * cameraLookSpeed;
		float L_5 = __this->get_pivotAngle_19();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_6 = __this->get_inputManager_4();
		float L_7 = L_6->get_cameraInputY_12();
		float L_8 = __this->get_cameraLookSpeed_16();
		__this->set_pivotAngle_19(((float)il2cpp_codegen_subtract((float)L_5, (float)((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)))));
		// lookAngle = Mathf.Clamp(lookAngle, minimumPivotAngle, maximumPivotAngle);
		float L_9 = __this->get_lookAngle_18();
		float L_10 = __this->get_minimumPivotAngle_20();
		float L_11 = __this->get_maximumPivotAngle_21();
		float L_12;
		L_12 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_9, L_10, L_11, /*hidden argument*/NULL);
		__this->set_lookAngle_18(L_12);
		// pivotAngle = Mathf.Clamp(pivotAngle, topDownMinimumPivotAngle, topDownMaximumPivotAngle);
		float L_13 = __this->get_pivotAngle_19();
		float L_14 = __this->get_topDownMinimumPivotAngle_24();
		float L_15 = __this->get_topDownMaximumPivotAngle_25();
		float L_16;
		L_16 = Mathf_Clamp_m2416F3B785C8F135863E3D17E5B0CB4174797B87(L_13, L_14, L_15, /*hidden argument*/NULL);
		__this->set_pivotAngle_19(L_16);
		// camera.transform.localRotation = Quaternion.Euler(pivotAngle, lookAngle, 0f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_0, /*hidden argument*/NULL);
		float L_18 = __this->get_pivotAngle_19();
		float L_19 = __this->get_lookAngle_18();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_20;
		L_20 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3(L_18, L_19, (0.0f), /*hidden argument*/NULL);
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_17, L_20, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CameraManager::SwitchCameras(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, String_t* ___cameraName0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < cameras.Length; i++)
		V_0 = 0;
		goto IL_004c;
	}

IL_0004:
	{
		// cameras[i].SetActive(false);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_cameras_26();
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// if (cameras[i].name == cameraName)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_4 = __this->get_cameras_26();
		int32_t L_5 = V_0;
		int32_t L_6 = L_5;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		String_t* L_8;
		L_8 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_7, /*hidden argument*/NULL);
		String_t* L_9 = ___cameraName0;
		bool L_10;
		L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_8, L_9, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0048;
		}
	}
	{
		// cameras[i].SetActive(true);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_11 = __this->get_cameras_26();
		int32_t L_12 = V_0;
		int32_t L_13 = L_12;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_14 = (L_11)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_13));
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_14, (bool)1, /*hidden argument*/NULL);
		// currentCamera = cameras[i].name;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_15 = __this->get_cameras_26();
		int32_t L_16 = V_0;
		int32_t L_17 = L_16;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_18 = (L_15)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_17));
		String_t* L_19;
		L_19 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_18, /*hidden argument*/NULL);
		__this->set_currentCamera_27(L_19);
	}

IL_0048:
	{
		// for (int i = 0; i < cameras.Length; i++)
		int32_t L_20 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_20, (int32_t)1));
	}

IL_004c:
	{
		// for (int i = 0; i < cameras.Length; i++)
		int32_t L_21 = V_0;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_22 = __this->get_cameras_26();
		if ((((int32_t)L_21) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_22)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		// }
		return;
	}
}
// UnityEngine.GameObject CameraManager::GetCurrentCamera()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * CameraManager_GetCurrentCamera_m69A5FB8797F2209A65B33A8179AD7925F5ECCDD6 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		// for (int i = 0; i < cameras.Length; i++)
		V_0 = 0;
		goto IL_002b;
	}

IL_0004:
	{
		// if (cameras[i].name == currentCamera)
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_0 = __this->get_cameras_26();
		int32_t L_1 = V_0;
		int32_t L_2 = L_1;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = (L_0)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_2));
		String_t* L_4;
		L_4 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_3, /*hidden argument*/NULL);
		String_t* L_5 = __this->get_currentCamera_27();
		bool L_6;
		L_6 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_4, L_5, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0027;
		}
	}
	{
		// return cameras[i];
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_7 = __this->get_cameras_26();
		int32_t L_8 = V_0;
		int32_t L_9 = L_8;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = (L_7)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_9));
		return L_10;
	}

IL_0027:
	{
		// for (int i = 0; i < cameras.Length; i++)
		int32_t L_11 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_11, (int32_t)1));
	}

IL_002b:
	{
		// for (int i = 0; i < cameras.Length; i++)
		int32_t L_12 = V_0;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_13 = __this->get_cameras_26();
		if ((((int32_t)L_12) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_13)->max_length))))))
		{
			goto IL_0004;
		}
	}
	{
		// return null;
		return (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *)NULL;
	}
}
// System.Void CameraManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraManager__ctor_m932EDBCBAE89115380AA929ED5BC9823B9371185 (CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * __this, const RuntimeMethod* method)
{
	{
		// private Vector3 cameraFollowVelocity = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0;
		L_0 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		__this->set_cameraFollowVelocity_10(L_0);
		// public float cameraCollisionOffset = 0.2f;
		__this->set_cameraCollisionOffset_12((0.200000003f));
		// public float minimumCollisionOffset = 0.2f;
		__this->set_minimumCollisionOffset_13((0.200000003f));
		// public float cameraCollisionRadius = 2;
		__this->set_cameraCollisionRadius_14((2.0f));
		// public float cameraFollowspeed = 0.2f; 
		__this->set_cameraFollowspeed_15((0.200000003f));
		// public float cameraLookSpeed = 2f; 
		__this->set_cameraLookSpeed_16((2.0f));
		// public float cameraPivotSpeed = 2f; 
		__this->set_cameraPivotSpeed_17((2.0f));
		// public float minimumPivotAngle = -35;
		__this->set_minimumPivotAngle_20((-35.0f));
		// public float maximumPivotAngle = 35;
		__this->set_maximumPivotAngle_21((35.0f));
		// public float shoulderMinimumPivotAngle = -35;
		__this->set_shoulderMinimumPivotAngle_22((-35.0f));
		// public float shoulderMaximumPivotAngle = 35;
		__this->set_shoulderMaximumPivotAngle_23((35.0f));
		// public float topDownMinimumPivotAngle = 50;
		__this->set_topDownMinimumPivotAngle_24((50.0f));
		// public float topDownMaximumPivotAngle = 75;
		__this->set_topDownMaximumPivotAngle_25((75.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CameraMovement::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41 (CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35 * __this, const RuntimeMethod* method)
{
	{
		// this.yaw = this.transform.eulerAngles.y;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F(L_0, /*hidden argument*/NULL);
		float L_2 = L_1.get_y_3();
		__this->set_yaw_8(L_2);
		// this.pitch = this.transform.eulerAngles.x;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_eulerAngles_mCF1E10C36ED1F03804A1D10A9BAB272E0EA8766F(L_3, /*hidden argument*/NULL);
		float L_5 = L_4.get_x_2();
		__this->set_pitch_9(L_5);
		// }
		return;
	}
}
// System.Void CameraMovement::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraMovement_Update_m02A5D16421ED8C060C06A72144FEA9250C55CE2B (CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFC6687DC37346CD2569888E29764F727FAF530E0);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetKey(KeyCode.LeftAlt))
		bool L_0;
		L_0 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(((int32_t)308), /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0118;
		}
	}
	{
		// if (Input.GetMouseButton(0))
		bool L_1;
		L_1 = Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1(0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0074;
		}
	}
	{
		// this.yaw += this.lookSpeedH * Input.GetAxis("Mouse X");
		float L_2 = __this->get_yaw_8();
		float L_3 = __this->get_lookSpeedH_4();
		float L_4;
		L_4 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, /*hidden argument*/NULL);
		__this->set_yaw_8(((float)il2cpp_codegen_add((float)L_2, (float)((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)))));
		// this.pitch -= this.lookSpeedV * Input.GetAxis("Mouse Y");
		float L_5 = __this->get_pitch_9();
		float L_6 = __this->get_lookSpeedV_5();
		float L_7;
		L_7 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, /*hidden argument*/NULL);
		__this->set_pitch_9(((float)il2cpp_codegen_subtract((float)L_5, (float)((float)il2cpp_codegen_multiply((float)L_6, (float)L_7)))));
		// this.transform.eulerAngles = new Vector3(this.pitch, this.yaw, 0f);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_9 = __this->get_pitch_9();
		float L_10 = __this->get_yaw_8();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_11), L_9, L_10, (0.0f), /*hidden argument*/NULL);
		Transform_set_eulerAngles_mFDCBC6282E4B1196AA26BF01008B2AAA2DD2969E(L_8, L_11, /*hidden argument*/NULL);
	}

IL_0074:
	{
		// if (Input.GetMouseButton(2))
		bool L_12;
		L_12 = Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1(2, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00bc;
		}
	}
	{
		// transform.Translate(-Input.GetAxisRaw("Mouse X") * Time.deltaTime * dragSpeed, -Input.GetAxisRaw("Mouse Y") * Time.deltaTime * dragSpeed, 0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_14;
		L_14 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, /*hidden argument*/NULL);
		float L_15;
		L_15 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_16 = __this->get_dragSpeed_7();
		float L_17;
		L_17 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, /*hidden argument*/NULL);
		float L_18;
		L_18 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		float L_19 = __this->get_dragSpeed_7();
		Transform_Translate_mC9343E1E646DA8FD42BE37137ACCBB4B52093F5C(L_13, ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((-L_14)), (float)L_15)), (float)L_16)), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)((-L_17)), (float)L_18)), (float)L_19)), (0.0f), /*hidden argument*/NULL);
	}

IL_00bc:
	{
		// if (Input.GetMouseButton(1))
		bool L_20;
		L_20 = Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1(1, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00f1;
		}
	}
	{
		// this.transform.Translate(0, 0, Input.GetAxisRaw("Mouse X") * this.zoomSpeed * .07f, Space.Self);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21;
		L_21 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_22;
		L_22 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, /*hidden argument*/NULL);
		float L_23 = __this->get_zoomSpeed_6();
		Transform_Translate_m22737F202DE67AAAAC408ADE91BD44F5BAF3DD6B(L_21, (0.0f), (0.0f), ((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)L_22, (float)L_23)), (float)(0.0700000003f))), 1, /*hidden argument*/NULL);
	}

IL_00f1:
	{
		// this.transform.Translate(0, 0, Input.GetAxis("Mouse ScrollWheel") * this.zoomSpeed, Space.Self);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_24;
		L_24 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		float L_25;
		L_25 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteralFC6687DC37346CD2569888E29764F727FAF530E0, /*hidden argument*/NULL);
		float L_26 = __this->get_zoomSpeed_6();
		Transform_Translate_m22737F202DE67AAAAC408ADE91BD44F5BAF3DD6B(L_24, (0.0f), (0.0f), ((float)il2cpp_codegen_multiply((float)L_25, (float)L_26)), 1, /*hidden argument*/NULL);
	}

IL_0118:
	{
		// }
		return;
	}
}
// System.Void CameraMovement::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA (CameraMovement_t0177164EDAE233E0E1B1A163202CCA98852C2F35 * __this, const RuntimeMethod* method)
{
	{
		// private float lookSpeedH = 2f;
		__this->set_lookSpeedH_4((2.0f));
		// private float lookSpeedV = 2f;
		__this->set_lookSpeedV_5((2.0f));
		// private float zoomSpeed = 2f;
		__this->set_zoomSpeed_6((2.0f));
		// private float dragSpeed = 3f;
		__this->set_dragSpeed_7((3.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Character_control::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Character_control_Start_m97E67AD16B8148D6F8F5DC3C45086B1C922B3CDD (Character_control_tEB187B80DE857A5EE58CD99757188637F71C8EF2 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralD139F540024B68AE2F2EECE8C05093886A02BAB5);
		s_Il2CppMethodInitialized = true;
	}
	{
		// this.animator = this.GetComponent<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0;
		L_0 = Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F(__this, /*hidden argument*/Component_GetComponent_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m56C584BE9A3B866D54FAEE0529E28C8D1E57989F_RuntimeMethod_var);
		__this->set_animator_5(L_0);
		// this.animator.SetBool("animation_" + this.animation_num, true);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1 = __this->get_animator_5();
		int32_t* L_2 = __this->get_address_of_animation_num_4();
		String_t* L_3;
		L_3 = Int32_ToString_m340C0A14D16799421EFDF8A81C8A16FA76D48411((int32_t*)L_2, /*hidden argument*/NULL);
		String_t* L_4;
		L_4 = String_Concat_m4B4AB72618348C5DFBFBA8DED84B9E2EBDB55E1B(_stringLiteralD139F540024B68AE2F2EECE8C05093886A02BAB5, L_3, /*hidden argument*/NULL);
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_1, L_4, (bool)1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Character_control::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Character_control_Update_m90B2CF7791F02193A8CFFF54B4548C2A8A781D74 (Character_control_tEB187B80DE857A5EE58CD99757188637F71C8EF2 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void Character_control::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Character_control__ctor_mDD12E650AA7D0B7DEF4B99D157D5755D6D6292FD (Character_control_tEB187B80DE857A5EE58CD99757188637F71C8EF2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ChargeManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChargeManager_Awake_m50D7F099C3A9FDDBAC1ECCCE56FD551D99E1F8B0 (ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * __this, const RuntimeMethod* method)
{
	{
		// chargeCounter = 100;
		__this->set_chargeCounter_4((100.0f));
		// }
		return;
	}
}
// System.Void ChargeManager::UpdateChargeCounter(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChargeManager_UpdateChargeCounter_m55E422F13E6183B524F031B4455E9374F0B8CB8F (ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * __this, bool ___isGrounded0, const RuntimeMethod* method)
{
	{
		// if (isGrounded)
		bool L_0 = ___isGrounded0;
		if (!L_0)
		{
			goto IL_0029;
		}
	}
	{
		// if (chargeCounter < 100)
		float L_1 = __this->get_chargeCounter_4();
		if ((!(((float)L_1) < ((float)(100.0f)))))
		{
			goto IL_0029;
		}
	}
	{
		// chargeCounter += chargeSpeed * Time.deltaTime;
		float L_2 = __this->get_chargeCounter_4();
		float L_3 = __this->get_chargeSpeed_5();
		float L_4;
		L_4 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_chargeCounter_4(((float)il2cpp_codegen_add((float)L_2, (float)((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)))));
	}

IL_0029:
	{
		// slider.value = chargeCounter / 100;
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_5 = __this->get_slider_8();
		float L_6 = __this->get_chargeCounter_4();
		VirtActionInvoker1< float >::Invoke(47 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_5, ((float)((float)L_6/(float)(100.0f))));
		// }
		return;
	}
}
// System.Void ChargeManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ChargeManager__ctor_m80FE8C0DE5AA7ECAEC4C03F7195E9C0C947673DE (ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * __this, const RuntimeMethod* method)
{
	{
		// public float chargeSpeed = 10;
		__this->set_chargeSpeed_5((10.0f));
		// public float drainSpeed = 5;
		__this->set_drainSpeed_6((5.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void GravityOrbit::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GravityOrbit_FixedUpdate_m6635C617B4B753B7C2762BF0A9A2A048BE0D437C (GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void GravityOrbit::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GravityOrbit_OnTriggerEnter_mC4D11D4D6E0E89BD5362D141F6BE80A080060DC1 (GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisGravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2_m7275FAA26EDDE37A27FC6C9AB643DF22D79B8789_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.GetComponent<PlayerLocomotion>())
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_1;
		L_1 = Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5(L_0, /*hidden argument*/Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		// other.GetComponent<PlayerLocomotion>().currentGravity = GetComponent<GravityOrbit>();
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_3 = ___other0;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_4;
		L_4 = Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5(L_3, /*hidden argument*/Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_5;
		L_5 = Component_GetComponent_TisGravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2_m7275FAA26EDDE37A27FC6C9AB643DF22D79B8789(__this, /*hidden argument*/Component_GetComponent_TisGravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2_m7275FAA26EDDE37A27FC6C9AB643DF22D79B8789_RuntimeMethod_var);
		L_4->set_currentGravity_22(L_5);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.Void GravityOrbit::OnTriggerExit(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GravityOrbit_OnTriggerExit_m56A083FFF3C8E3A02ADCCF5BD96538F437EDED4C (GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.GetComponent<PlayerLocomotion>())
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_1;
		L_1 = Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5(L_0, /*hidden argument*/Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_2;
		L_2 = Object_op_Implicit_mC8214E4F028CC2F036CC82BDB81D102A02893499(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		// other.GetComponent<PlayerLocomotion>().currentGravity = null;
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_3 = ___other0;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_4;
		L_4 = Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5(L_3, /*hidden argument*/Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		L_4->set_currentGravity_22((GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 *)NULL);
	}

IL_0019:
	{
		// }
		return;
	}
}
// System.Void GravityOrbit::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GravityOrbit__ctor_mE1D15D8342056DB05CC6E0E267D5B8CB895AE74F (GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void InputManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_Awake_m56AAFE728DF63B4B922FC31449C82EC6CFB0D946 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m76E4330A66259F770A0F73017C967C9EFAFE9993_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animationManager = GetComponent<AnimationManager>();
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_0;
		L_0 = Component_GetComponent_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m76E4330A66259F770A0F73017C967C9EFAFE9993(__this, /*hidden argument*/Component_GetComponent_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m76E4330A66259F770A0F73017C967C9EFAFE9993_RuntimeMethod_var);
		__this->set_animationManager_4(L_0);
		// playerLocomotion = GetComponent<PlayerLocomotion>();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_1;
		L_1 = Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5(__this, /*hidden argument*/Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		__this->set_playerLocomotion_5(L_1);
		// }
		return;
	}
}
// System.Void InputManager::HandleAllInputs()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleAllInputs_mCDF44B0217785B2D7D4D6702B18DC531D7B0FC75 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	{
		// ReadInput();
		InputManager_ReadInput_mCC0FE5A9B269D19F1A7A3ED6BC6D3969D1076801(__this, /*hidden argument*/NULL);
		// HandleMovementInput();
		InputManager_HandleMovementInput_mBCE591A731DBF1E30D86801664DFA838CA788D79(__this, /*hidden argument*/NULL);
		// HandleSprintingInput();
		InputManager_HandleSprintingInput_m7DFEE167DC1AA1463421F97619E8F28438FD8BFB(__this, /*hidden argument*/NULL);
		// HandleJumpingInput();
		InputManager_HandleJumpingInput_m5CBBCEB3CA4F46A490AB6A66A081BA59FF55337D(__this, /*hidden argument*/NULL);
		// HandleShootInput();
		InputManager_HandleShootInput_m3C3B262CA06D3DD5A68CD85FB7C33CA7A5E0A7F0(__this, /*hidden argument*/NULL);
		// HandleAimInput();
		InputManager_HandleAimInput_mCD4043BE6196AE2B6C067B3F05C25AEFAD928ECC(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InputManager::ReadInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_ReadInput_mCC0FE5A9B269D19F1A7A3ED6BC6D3969D1076801 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA6A16FA6FFFE7CF75CED271F9B06FD471503AB8C);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralE16EFE13C5C08096A869677E0912595D5D6C1C03);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFBC1FBDF3F91C0637B6624C6C526B3718C7E46A2);
		s_Il2CppMethodInitialized = true;
	}
	{
		// movementInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
		float L_0;
		L_0 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral7F8C014BD4810CC276D0F9F81A1E759C7B098B1E, /*hidden argument*/NULL);
		float L_1;
		L_1 = Input_GetAxisRaw_mC07AC23FD8D04A69CDB07C6399C93FFFAEB0FC5B(_stringLiteral265E15F1F86F1C766555899D5771CF29055DE75A, /*hidden argument*/NULL);
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline((&L_2), L_0, L_1, /*hidden argument*/NULL);
		__this->set_movementInput_6(L_2);
		// cameraInput.x = Input.GetAxis("Mouse X");
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_3 = __this->get_address_of_cameraInput_7();
		float L_4;
		L_4 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral88BEE283254D7094E258B3A88730F4CC4F1E4AC7, /*hidden argument*/NULL);
		L_3->set_x_0(L_4);
		// cameraInput.y = Input.GetAxis("Mouse Y");
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_5 = __this->get_address_of_cameraInput_7();
		float L_6;
		L_6 = Input_GetAxis_m939297DEB2ECF8D8D09AD66EB69979AAD2B62326(_stringLiteral16DD21BE77B115D392226EB71A2D3A9FDC29E3F0, /*hidden argument*/NULL);
		L_5->set_y_1(L_6);
		// jumpPressed = Input.GetButton("Jump");
		bool L_7;
		L_7 = Input_GetButton_m95EE8314087068F3AA9CEF3C3F6A246D55C4734C(_stringLiteral70253F929BCE7F81DF1A5A1C0900BED744E86C9C, /*hidden argument*/NULL);
		__this->set_jumpPressed_14(L_7);
		// sprintPressed = Input.GetButton("Fire1");
		bool L_8;
		L_8 = Input_GetButton_m95EE8314087068F3AA9CEF3C3F6A246D55C4734C(_stringLiteralFBC1FBDF3F91C0637B6624C6C526B3718C7E46A2, /*hidden argument*/NULL);
		__this->set_sprintPressed_13(L_8);
		// shootPressed = Input.GetButtonDown("Fire2");
		bool L_9;
		L_9 = Input_GetButtonDown_m2001112EBCA3D5C7B0344EF62C896667F7E67DDF(_stringLiteralE16EFE13C5C08096A869677E0912595D5D6C1C03, /*hidden argument*/NULL);
		__this->set_shootPressed_15(L_9);
		// aimPressed = Input.GetButton("Fire3");
		bool L_10;
		L_10 = Input_GetButton_m95EE8314087068F3AA9CEF3C3F6A246D55C4734C(_stringLiteralA6A16FA6FFFE7CF75CED271F9B06FD471503AB8C, /*hidden argument*/NULL);
		__this->set_aimPressed_16(L_10);
		// }
		return;
	}
}
// System.Void InputManager::HandleMovementInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleMovementInput_mBCE591A731DBF1E30D86801664DFA838CA788D79 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	{
		// verticalInput = movementInput.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_0 = __this->get_address_of_movementInput_6();
		float L_1 = L_0->get_y_1();
		__this->set_verticalInput_9(L_1);
		// horizontalInput = movementInput.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_2 = __this->get_address_of_movementInput_6();
		float L_3 = L_2->get_x_0();
		__this->set_horizontalInput_10(L_3);
		// if (Input.GetKey(KeyCode.O))
		bool L_4;
		L_4 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(((int32_t)111), /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		// verticalInput = 0.5f;
		__this->set_verticalInput_9((0.5f));
	}

IL_0036:
	{
		// if (Input.GetKey(KeyCode.L))
		bool L_5;
		L_5 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(((int32_t)108), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_004a;
		}
	}
	{
		// verticalInput = -0.5f;
		__this->set_verticalInput_9((-0.5f));
	}

IL_004a:
	{
		// if (Input.GetKey(KeyCode.K))
		bool L_6;
		L_6 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(((int32_t)107), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_005e;
		}
	}
	{
		// horizontalInput = -0.5f;
		__this->set_horizontalInput_10((-0.5f));
	}

IL_005e:
	{
		// if (Input.GetKey(KeyCode.Semicolon))
		bool L_7;
		L_7 = Input_GetKey_mFDD450A4C61F2930928B12287FFBD1ACCB71E429(((int32_t)59), /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0072;
		}
	}
	{
		// horizontalInput = 0.5f;
		__this->set_horizontalInput_10((0.5f));
	}

IL_0072:
	{
		// cameraInputX = cameraInput.x;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_8 = __this->get_address_of_cameraInput_7();
		float L_9 = L_8->get_x_0();
		__this->set_cameraInputX_11(L_9);
		// cameraInputY = cameraInput.y;
		Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * L_10 = __this->get_address_of_cameraInput_7();
		float L_11 = L_10->get_y_1();
		__this->set_cameraInputY_12(L_11);
		// moveAmount = Mathf.Clamp01(Mathf.Abs(horizontalInput) + Mathf.Abs(verticalInput));
		float L_12 = __this->get_horizontalInput_10();
		float L_13;
		L_13 = fabsf(L_12);
		float L_14 = __this->get_verticalInput_9();
		float L_15;
		L_15 = fabsf(L_14);
		float L_16;
		L_16 = Mathf_Clamp01_m2296D75F0F1292D5C8181C57007A1CA45F440C4C(((float)il2cpp_codegen_add((float)L_13, (float)L_15)), /*hidden argument*/NULL);
		__this->set_moveAmount_8(L_16);
		// animationManager.UpdateAnimator(0, moveAmount);
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_17 = __this->get_animationManager_4();
		float L_18 = __this->get_moveAmount_8();
		AnimationManager_UpdateAnimator_m3F09B8D6E2E97AF94A0296FF32F5F23AEF011D94(L_17, (0.0f), L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void InputManager::HandleSprintingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleSprintingInput_m7DFEE167DC1AA1463421F97619E8F28438FD8BFB (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	{
		// if (sprintPressed && moveAmount > 0.5f)
		bool L_0 = __this->get_sprintPressed_13();
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		float L_1 = __this->get_moveAmount_8();
		if ((!(((float)L_1) > ((float)(0.5f)))))
		{
			goto IL_0022;
		}
	}
	{
		// playerLocomotion.isSprinting = true;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_2 = __this->get_playerLocomotion_5();
		L_2->set_isSprinting_17((bool)1);
		// }
		return;
	}

IL_0022:
	{
		// playerLocomotion.isSprinting = false;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_3 = __this->get_playerLocomotion_5();
		L_3->set_isSprinting_17((bool)0);
		// }
		return;
	}
}
// System.Void InputManager::HandleJumpingInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleJumpingInput_m5CBBCEB3CA4F46A490AB6A66A081BA59FF55337D (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	{
		// if (jumpPressed)
		bool L_0 = __this->get_jumpPressed_14();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		// jumpPressed = false;
		__this->set_jumpPressed_14((bool)0);
		// playerLocomotion.HandleJump();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_1 = __this->get_playerLocomotion_5();
		PlayerLocomotion_HandleJump_m067C8500AFA029CCFC1A96A37B10A6A05B7FF88C(L_1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void InputManager::HandleShootInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleShootInput_m3C3B262CA06D3DD5A68CD85FB7C33CA7A5E0A7F0 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	{
		// if (shootPressed)
		bool L_0 = __this->get_shootPressed_15();
		if (!L_0)
		{
			goto IL_001a;
		}
	}
	{
		// shootPressed = false;
		__this->set_shootPressed_15((bool)0);
		// playerLocomotion.HandleShoot();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_1 = __this->get_playerLocomotion_5();
		PlayerLocomotion_HandleShoot_mC851DE55C6118DBCF3AC0CDEC10DAD7DF6A3A23D(L_1, /*hidden argument*/NULL);
	}

IL_001a:
	{
		// }
		return;
	}
}
// System.Void InputManager::HandleAimInput()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager_HandleAimInput_mCD4043BE6196AE2B6C067B3F05C25AEFAD928ECC (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralA6A16FA6FFFE7CF75CED271F9B06FD471503AB8C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// playerLocomotion.HandleAim();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_0 = __this->get_playerLocomotion_5();
		PlayerLocomotion_HandleAim_m332863775AA4450513DA1775494A88D6F571EA27(L_0, /*hidden argument*/NULL);
		// if (Input.GetButtonUp("Fire3"))
		bool L_1;
		L_1 = Input_GetButtonUp_m15AA6B42BD0DDCC7802346E49F30653D750260DD(_stringLiteralA6A16FA6FFFE7CF75CED271F9B06FD471503AB8C, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		// aimPressed = false;
		__this->set_aimPressed_16((bool)0);
	}

IL_001e:
	{
		// }
		return;
	}
}
// System.Void InputManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7 (InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void LevelManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_Start_m9B7C3BAF98CDAC3BADCE1790AA5ED55654B41172 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void LevelManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_Update_m559DC5A714E51E73375E147ED363559B97ED514A (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.R))
		bool L_0;
		L_0 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)27), /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		bool L_1;
		L_1 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)114), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0077;
		}
	}

IL_0012:
	{
		// GameObject.FindGameObjectWithTag("Player").transform.parent = null;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2;
		L_2 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_2, /*hidden argument*/NULL);
		Transform_set_parent_mEAE304E1A804E8B83054CEECB5BF1E517196EC13(L_3, (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)NULL, /*hidden argument*/NULL);
		// GameObject.FindGameObjectWithTag("Player").transform.position = new Vector3(0, 2, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4;
		L_4 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), (0.0f), (2.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_5, L_6, /*hidden argument*/NULL);
		// GameObject.FindGameObjectWithTag("Player").transform.rotation = Quaternion.Euler(0,0,0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7;
		L_7 = GameObject_FindGameObjectWithTag_mFC215979EDFED361F88C336BF9E187F24434C63F(_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_7, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_9;
		L_9 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_8, L_9, /*hidden argument*/NULL);
	}

IL_0077:
	{
		// if (Input.GetKeyDown(KeyCode.Alpha1))
		bool L_10;
		L_10 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)49), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0086;
		}
	}
	{
		// SceneManager.LoadScene(0);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(0, /*hidden argument*/NULL);
	}

IL_0086:
	{
		// if (Input.GetKeyDown(KeyCode.Alpha2))
		bool L_11;
		L_11 = Input_GetKeyDown_m476116696E71771641BBECBAB1A4C55E69018220(((int32_t)50), /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_0095;
		}
	}
	{
		// SceneManager.LoadScene(1);
		IL2CPP_RUNTIME_CLASS_INIT(SceneManager_tEC9D10ECC0377F8AE5AEEB5A789FFD24364440FA_il2cpp_TypeInfo_var);
		SceneManager_LoadScene_m5550E6368A6D0E37DACEDA3C5E4BA331836BC3C5(1, /*hidden argument*/NULL);
	}

IL_0095:
	{
		// }
		return;
	}
}
// System.Void LevelManager::OnTriggerEnter(UnityEngine.Collider)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager_OnTriggerEnter_m2E3BA05ED2BD87F197E46E8CCF79A3B3CED45984 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * ___other0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (other.CompareTag("Player"))
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_0 = ___other0;
		bool L_1;
		L_1 = Component_CompareTag_m17D74EDCC81A10B18A0A588519F522E8DF1D7879(L_0, _stringLiteralCAF8804297181FF007CA835529DD4477CFD94A70, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002c;
		}
	}
	{
		// other.transform.position = new Vector3(0,2,0);
		Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02 * L_2 = ___other0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_2, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_4), (0.0f), (2.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_3, L_4, /*hidden argument*/NULL);
	}

IL_002c:
	{
		// }
		return;
	}
}
// System.Void LevelManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LevelManager__ctor_mD6FAECFAF24E1996EC8147344018498B20E3DE49 (LevelManager_t010B312A2B35B45291F58195216ABB5673174961 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlanetOrbit::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlanetOrbit_Start_m28FAEB2D84FC5E817749F543FE067EF7ECCA2D23 (PlanetOrbit_tDB68D6FC93B55A78AC923A046D6E61E722E2542A * __this, const RuntimeMethod* method)
{
	{
		// OriginPoint = transform.parent.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Transform_get_parent_m7D06005D9CB55F90F39D42F6A2AF9C7BC80745C9(L_0, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		__this->set_OriginPoint_4(L_2);
		// }
		return;
	}
}
// System.Void PlanetOrbit::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlanetOrbit_Update_m991AA39550E82311387FBD95A91027B9F7903D3D (PlanetOrbit_tDB68D6FC93B55A78AC923A046D6E61E722E2542A * __this, const RuntimeMethod* method)
{
	{
		// transform.RotateAround(OriginPoint, Vector3.up, orbitSpeed * Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = __this->get_OriginPoint_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		float L_3 = __this->get_orbitSpeed_5();
		float L_4;
		L_4 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Transform_RotateAround_m1F93A7A1807BE407BD23EC1BA49F03AD22FCE4BE(L_0, L_1, L_2, ((float)il2cpp_codegen_multiply((float)L_3, (float)L_4)), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlanetOrbit::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlanetOrbit__ctor_m66D19B3300F913664542F845FB6A9E1D08238C1E (PlanetOrbit_tDB68D6FC93B55A78AC923A046D6E61E722E2542A * __this, const RuntimeMethod* method)
{
	{
		// public float orbitSpeed = 20f;
		__this->set_orbitSpeed_5((20.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerEffectManager::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerEffectManager_Start_m6F21E07EC7AB386EAB625DC100C4BE94F5CCC1A8 (PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void PlayerEffectManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerEffectManager_Update_m21A13B26E68B8E926E04D19686ADD39A71554DB2 (PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void PlayerEffectManager::PlayLandEffect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerEffectManager_PlayLandEffect_m926080EC179C8C53EF8EB69F9BD22B1C3C3174C3 (PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// GameObject effect = Instantiate(LandEffect, new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z),
		//     transform.rotation);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_LandEffect_4();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_1;
		L_1 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_1, /*hidden argument*/NULL);
		float L_3 = L_2.get_x_2();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		float L_6 = L_5.get_y_3();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_7, /*hidden argument*/NULL);
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10;
		memset((&L_10), 0, sizeof(L_10));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_10), L_3, ((float)il2cpp_codegen_subtract((float)L_6, (float)(0.5f))), L_9, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_12;
		L_12 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_13;
		L_13 = Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B(L_0, L_10, L_12, /*hidden argument*/Object_Instantiate_TisGameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319_m81B599A0051F8F4543E5C73A11585E96E940943B_RuntimeMethod_var);
		// Destroy(effect, 2);
		Object_Destroy_mAAAA103F4911E9FA18634BF9605C28559F5E2AC7(L_13, (2.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerEffectManager::ToggleShootingEffects(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerEffectManager_ToggleShootingEffects_m3091CE05E2C37AF30BF81F8DAA06E40B5A8FCE85 (PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * __this, bool ___isShooting0, const RuntimeMethod* method)
{
	{
		// windTrails.SetActive(isShooting);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_windTrails_5();
		bool L_1 = ___isShooting0;
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, L_1, /*hidden argument*/NULL);
		// jetPropulsion.SetActive(isShooting);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_jetPropulsion_6();
		bool L_3 = ___isShooting0;
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerEffectManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerEffectManager__ctor_mAD6A0FCB97903109B780BF412E8EB698F11E9D46 (PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerLocomotion::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_Awake_m90E3D43E029E1D3C9F093D5EB4FABD2FF61568C5 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m67C66D5AA4AA16A3448B21AF2EC03FFEAD4F03B9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0_m14E499548B982EF24D66C845F258B527D0319A41_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m6CCCFD6651416AD5F042C75E8B8CF01C82631CA9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m58B42920D67789AB6096BAB8D957FB64B4DDAA16_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisCameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_m1BDBA1D013C9E2C431C864109ADEA485E4716CF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// playerManager = GetComponent<PlayerManager>();
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_0;
		L_0 = Component_GetComponent_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m58B42920D67789AB6096BAB8D957FB64B4DDAA16(__this, /*hidden argument*/Component_GetComponent_TisPlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48_m58B42920D67789AB6096BAB8D957FB64B4DDAA16_RuntimeMethod_var);
		__this->set_playerManager_4(L_0);
		// inputManager = GetComponent<InputManager>();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_1;
		L_1 = Component_GetComponent_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m6CCCFD6651416AD5F042C75E8B8CF01C82631CA9(__this, /*hidden argument*/Component_GetComponent_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m6CCCFD6651416AD5F042C75E8B8CF01C82631CA9_RuntimeMethod_var);
		__this->set_inputManager_6(L_1);
		// animationManager = GetComponentInChildren<AnimationManager>();
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_2;
		L_2 = Component_GetComponentInChildren_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m67C66D5AA4AA16A3448B21AF2EC03FFEAD4F03B9(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_m67C66D5AA4AA16A3448B21AF2EC03FFEAD4F03B9_RuntimeMethod_var);
		__this->set_animationManager_5(L_2);
		// chargeManager = GetComponent<ChargeManager>();
		ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * L_3;
		L_3 = Component_GetComponent_TisChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0_m14E499548B982EF24D66C845F258B527D0319A41(__this, /*hidden argument*/Component_GetComponent_TisChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0_m14E499548B982EF24D66C845F258B527D0319A41_RuntimeMethod_var);
		__this->set_chargeManager_7(L_3);
		// cameraManager = FindObjectOfType<CameraManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_4;
		L_4 = Object_FindObjectOfType_TisCameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_m1BDBA1D013C9E2C431C864109ADEA485E4716CF2(/*hidden argument*/Object_FindObjectOfType_TisCameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_m1BDBA1D013C9E2C431C864109ADEA485E4716CF2_RuntimeMethod_var);
		__this->set_cameraManager_8(L_4);
		// rb = GetComponent<Rigidbody>();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_5;
		L_5 = Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A_m9DC24AA806B0B65E917751F7A3AFDB58861157CE_RuntimeMethod_var);
		__this->set_rb_11(L_5);
		// cameraObject = Camera.main.transform;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_6;
		L_6 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_6, /*hidden argument*/NULL);
		__this->set_cameraObject_10(L_7);
		// }
		return;
	}
}
// System.Void PlayerLocomotion::HandleAllMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleAllMovement_m3AA12244C0EBE67CD8D4A92D077D8058849BD2F1 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	{
		// HandleGravity();
		PlayerLocomotion_HandleGravity_m945C350309BD0A0A68A43DA7F145D45FCE41089E(__this, /*hidden argument*/NULL);
		// if (!isShooting)
		bool L_0 = __this->get_isShooting_20();
		if (L_0)
		{
			goto IL_0014;
		}
	}
	{
		// HandleFallingAndLanding();
		PlayerLocomotion_HandleFallingAndLanding_m6CE12DB3EA39F414944DE9E69BC427D9B9269F6E(__this, /*hidden argument*/NULL);
	}

IL_0014:
	{
		// HandleMovement();
		PlayerLocomotion_HandleMovement_m7DF7D7FE2A854C2DE4D9C275CFEC16E5432ED84D(__this, /*hidden argument*/NULL);
		// HandleRotation();
		PlayerLocomotion_HandleRotation_mD5676E3D897F7D7943BD5FA06043F9224DCABB50(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerLocomotion::HandleMovement()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleMovement_m7DF7D7FE2A854C2DE4D9C275CFEC16E5432ED84D (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * V_1 = NULL;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (playerManager.isInteracting)
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_0 = __this->get_playerManager_4();
		bool L_1 = L_0->get_isInteracting_10();
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// return;
		return;
	}

IL_000e:
	{
		// if (isJumping)
		bool L_2 = __this->get_isJumping_19();
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		// return;
		return;
	}

IL_0017:
	{
		// if (currentGravity != null && !inputManager.aimPressed) 
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_3 = __this->get_currentGravity_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0090;
		}
	}
	{
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_5 = __this->get_inputManager_6();
		bool L_6 = L_5->get_aimPressed_16();
		if (L_6)
		{
			goto IL_0090;
		}
	}
	{
		// moveDirection = new Vector3(inputManager.horizontalInput, 0, inputManager.verticalInput).normalized;
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_7 = __this->get_inputManager_6();
		float L_8 = L_7->get_horizontalInput_10();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_9 = __this->get_inputManager_6();
		float L_10 = L_9->get_verticalInput_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		memset((&L_11), 0, sizeof(L_11));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_11), L_8, (0.0f), L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		__this->set_moveDirection_9(L_12);
		// if (moveDirection != Vector3.zero)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = __this->get_moveDirection_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_15;
		L_15 = Vector3_op_Inequality_m15190A795B416EB699E69E6190DE6F1C1F208710(L_13, L_14, /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_007e;
		}
	}
	{
		// RotateOnPlanet(moveDirection);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16 = __this->get_moveDirection_9();
		PlayerLocomotion_RotateOnPlanet_m3E3C7A8C083C82F729DDBC075D4F61397E51CB61(__this, L_16, /*hidden argument*/NULL);
	}

IL_007e:
	{
		// transform.Translate(moveDirection);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18 = __this->get_moveDirection_9();
		Transform_Translate_m24A8CB13E2AAB0C17EE8FE593266CF463E0B02D0(L_17, L_18, /*hidden argument*/NULL);
		// } else
		return;
	}

IL_0090:
	{
		// if (!inputManager.aimPressed)
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_19 = __this->get_inputManager_6();
		bool L_20 = L_19->get_aimPressed_16();
		if (L_20)
		{
			goto IL_00a6;
		}
	}
	{
		// targetObject = cameraObject;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21 = __this->get_cameraObject_10();
		V_1 = L_21;
		// }
		goto IL_00ad;
	}

IL_00a6:
	{
		// targetObject = transform;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		V_1 = L_22;
	}

IL_00ad:
	{
		// moveDirection = targetObject.forward * inputManager.verticalInput;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		L_24 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_23, /*hidden argument*/NULL);
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_25 = __this->get_inputManager_6();
		float L_26 = L_25->get_verticalInput_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_24, L_26, /*hidden argument*/NULL);
		__this->set_moveDirection_9(L_27);
		// moveDirection = moveDirection + targetObject.right * inputManager.horizontalInput;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28 = __this->get_moveDirection_9();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_29 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_30;
		L_30 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_29, /*hidden argument*/NULL);
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_31 = __this->get_inputManager_6();
		float L_32 = L_31->get_horizontalInput_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_33;
		L_33 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_30, L_32, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_34;
		L_34 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_28, L_33, /*hidden argument*/NULL);
		__this->set_moveDirection_9(L_34);
		// moveDirection.y = 0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_35 = __this->get_address_of_moveDirection_9();
		L_35->set_y_3((0.0f));
		// moveDirection.Normalize();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_36 = __this->get_address_of_moveDirection_9();
		Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)L_36, /*hidden argument*/NULL);
		// Vector3 movementVelocity = ChangeSpeed(moveDirection);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37 = __this->get_moveDirection_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_38;
		L_38 = PlayerLocomotion_ChangeSpeed_mEB80118161B66A84693381F16D6BAA3E58B4DC89(__this, L_37, /*hidden argument*/NULL);
		V_2 = L_38;
		// rb.velocity = new Vector3(movementVelocity.x, rb.velocity.y, movementVelocity.z);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_39 = __this->get_rb_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_40 = V_2;
		float L_41 = L_40.get_x_2();
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_42 = __this->get_rb_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_43;
		L_43 = Rigidbody_get_velocity_mCFB033F3BD14C2BA68E797DFA4950F9307EC8E2C(L_42, /*hidden argument*/NULL);
		float L_44 = L_43.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45 = V_2;
		float L_46 = L_45.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_47;
		memset((&L_47), 0, sizeof(L_47));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_47), L_41, L_44, L_46, /*hidden argument*/NULL);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_39, L_47, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.Vector3 PlayerLocomotion::ChangeSpeed(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  PlayerLocomotion_ChangeSpeed_mEB80118161B66A84693381F16D6BAA3E58B4DC89 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___direction0, const RuntimeMethod* method)
{
	{
		// if (isSprinting)
		bool L_0 = __this->get_isSprinting_17();
		if (!L_0)
		{
			goto IL_0018;
		}
	}
	{
		// direction = direction * sprintingSpeed;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1 = ___direction0;
		float L_2 = __this->get_sprintingSpeed_26();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3;
		L_3 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_1, L_2, /*hidden argument*/NULL);
		___direction0 = L_3;
		// }
		goto IL_0048;
	}

IL_0018:
	{
		// if (inputManager.moveAmount > 0.5f)
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_4 = __this->get_inputManager_6();
		float L_5 = L_4->get_moveAmount_8();
		if ((!(((float)L_5) > ((float)(0.5f)))))
		{
			goto IL_003a;
		}
	}
	{
		// direction = direction * runningspeed;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___direction0;
		float L_7 = __this->get_runningspeed_25();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_6, L_7, /*hidden argument*/NULL);
		___direction0 = L_8;
		// }
		goto IL_0048;
	}

IL_003a:
	{
		// direction = direction * walkingspeed;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9 = ___direction0;
		float L_10 = __this->get_walkingspeed_24();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_9, L_10, /*hidden argument*/NULL);
		___direction0 = L_11;
	}

IL_0048:
	{
		// return direction;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = ___direction0;
		return L_12;
	}
}
// System.Void PlayerLocomotion::HandleRotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleRotation_mD5676E3D897F7D7943BD5FA06043F9224DCABB50 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (playerManager.isInteracting)
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_0 = __this->get_playerManager_4();
		bool L_1 = L_0->get_isInteracting_10();
		if (!L_1)
		{
			goto IL_000e;
		}
	}
	{
		// return;
		return;
	}

IL_000e:
	{
		// if (isJumping)
		bool L_2 = __this->get_isJumping_19();
		if (!L_2)
		{
			goto IL_0017;
		}
	}
	{
		// return;
		return;
	}

IL_0017:
	{
		// if (currentGravity != null || inputManager.aimPressed)
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_3 = __this->get_currentGravity_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_4;
		L_4 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_3, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_0032;
		}
	}
	{
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_5 = __this->get_inputManager_6();
		bool L_6 = L_5->get_aimPressed_16();
		if (!L_6)
		{
			goto IL_0033;
		}
	}

IL_0032:
	{
		// return;
		return;
	}

IL_0033:
	{
		// Vector3 targetDirection = Vector3.zero;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		V_0 = L_7;
		// targetDirection = cameraObject.forward * inputManager.verticalInput;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8 = __this->get_cameraObject_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_8, /*hidden argument*/NULL);
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_10 = __this->get_inputManager_6();
		float L_11 = L_10->get_verticalInput_9();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		L_12 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_9, L_11, /*hidden argument*/NULL);
		V_0 = L_12;
		// targetDirection = targetDirection + cameraObject.right * inputManager.horizontalInput;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_14 = __this->get_cameraObject_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15;
		L_15 = Transform_get_right_mA8EB1882CCE8759766544448308C88D9200F06CE(L_14, /*hidden argument*/NULL);
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_16 = __this->get_inputManager_6();
		float L_17 = L_16->get_horizontalInput_10();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_15, L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline(L_13, L_18, /*hidden argument*/NULL);
		V_0 = L_19;
		// targetDirection.Normalize();
		Vector3_Normalize_m2258C159121FC81954C301DEE631BC24FCEDE780((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_0), /*hidden argument*/NULL);
		// targetDirection.y = 0;
		(&V_0)->set_y_3((0.0f));
		// if (targetDirection == Vector3.zero)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		bool L_22;
		L_22 = Vector3_op_Equality_m8A98C7F38641110A2F90445EF8E98ECE14B08296(L_20, L_21, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00a3;
		}
	}
	{
		// targetDirection = transform.forward;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_23;
		L_23 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_24;
		L_24 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_23, /*hidden argument*/NULL);
		V_0 = L_24;
	}

IL_00a3:
	{
		// Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25 = V_0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_26;
		L_26 = Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF(L_25, /*hidden argument*/NULL);
		V_1 = L_26;
		// Quaternion playerRotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_27;
		L_27 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_28;
		L_28 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_27, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_29 = V_1;
		float L_30 = __this->get_rotationSpeed_27();
		float L_31;
		L_31 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_32;
		L_32 = Quaternion_Slerp_m6D2BD18286254E28D2288B51962EC71F85C7B5C8(L_28, L_29, ((float)il2cpp_codegen_multiply((float)L_30, (float)L_31)), /*hidden argument*/NULL);
		V_2 = L_32;
		// transform.rotation = playerRotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_33;
		L_33 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_34 = V_2;
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_33, L_34, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerLocomotion::HandleFallingAndLanding()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleFallingAndLanding_m6CE12DB3EA39F414944DE9E69BC427D9B9269F6E (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D_mE88BF86CEFAE820AB756B8FACF1C9CF3E50142AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral7598EFBF4A029BF0B3B286271D8C3B865F7A66FA);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral99654BA65919C1D4F705ADE00AD912C711685A9F);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1915CC31B6BC62A90E9FD4F876A00AA4B73C4ED);
		s_Il2CppMethodInitialized = true;
	}
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// Vector3 raycastOrigin = transform.position;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0;
		L_0 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_1;
		L_1 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		// raycastOrigin.y = raycastOrigin.y + raycastHeightOffset;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = V_1;
		float L_3 = L_2.get_y_3();
		float L_4 = __this->get_raycastHeightOffset_15();
		(&V_1)->set_y_3(((float)il2cpp_codegen_add((float)L_3, (float)L_4)));
		// if (!isGrounded && !isJumping)
		bool L_5 = __this->get_isGrounded_18();
		if (L_5)
		{
			goto IL_00ca;
		}
	}
	{
		bool L_6 = __this->get_isJumping_19();
		if (L_6)
		{
			goto IL_00ca;
		}
	}
	{
		// if (!playerManager.isInteracting)
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_7 = __this->get_playerManager_4();
		bool L_8 = L_7->get_isInteracting_10();
		if (L_8)
		{
			goto IL_0055;
		}
	}
	{
		// animationManager.PlayTargetAnimation("fall", true);
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_9 = __this->get_animationManager_5();
		AnimationManager_PlayTargetAnimation_m47BDB6536609430A331D8E9D0FBFCC56F1DD9DEE(L_9, _stringLiteral7598EFBF4A029BF0B3B286271D8C3B865F7A66FA, (bool)1, (bool)0, /*hidden argument*/NULL);
	}

IL_0055:
	{
		// animationManager.animator.SetBool("isUsingRootMotion", false);
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_10 = __this->get_animationManager_5();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_11 = L_10->get_animator_4();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_11, _stringLiteralB1915CC31B6BC62A90E9FD4F876A00AA4B73C4ED, (bool)0, /*hidden argument*/NULL);
		// inAirTimer = inAirTimer + Time.deltaTime;
		float L_12 = __this->get_inAirTimer_12();
		float L_13;
		L_13 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		__this->set_inAirTimer_12(((float)il2cpp_codegen_add((float)L_12, (float)L_13)));
		// rb.AddForce(transform.forward * leapingVelocity);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_14 = __this->get_rb_11();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_15, /*hidden argument*/NULL);
		float L_17 = __this->get_leapingVelocity_13();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_16, L_17, /*hidden argument*/NULL);
		Rigidbody_AddForce_mDFB0D57C25682B826999B0074F5C0FD399C6401D(L_14, L_18, /*hidden argument*/NULL);
		// rb.AddForce(-localUp * fallingVelocity * inAirTimer);
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_19 = __this->get_rb_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20 = __this->get_localUp_23();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_21;
		L_21 = Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline(L_20, /*hidden argument*/NULL);
		float L_22 = __this->get_fallingVelocity_14();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23;
		L_23 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_21, L_22, /*hidden argument*/NULL);
		float L_24 = __this->get_inAirTimer_12();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_25;
		L_25 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_23, L_24, /*hidden argument*/NULL);
		Rigidbody_AddForce_mDFB0D57C25682B826999B0074F5C0FD399C6401D(L_19, L_25, /*hidden argument*/NULL);
	}

IL_00ca:
	{
		// if (Physics.SphereCast(raycastOrigin, 0.2f, -localUp, out hit, groundLayer))
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_26 = V_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = __this->get_localUp_23();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline(L_27, /*hidden argument*/NULL);
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_29 = __this->get_groundLayer_16();
		int32_t L_30;
		L_30 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_29, /*hidden argument*/NULL);
		bool L_31;
		L_31 = Physics_SphereCast_m2E200DFD71A597968D4F50B8A6284F2E622B4C57(L_26, (0.200000003f), L_28, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), ((float)((float)L_30)), /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0159;
		}
	}
	{
		// if (!isGrounded)
		bool L_32 = __this->get_isGrounded_18();
		if (L_32)
		{
			goto IL_0146;
		}
	}
	{
		// animationManager.PlayTargetAnimation("land", true);
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_33 = __this->get_animationManager_5();
		AnimationManager_PlayTargetAnimation_m47BDB6536609430A331D8E9D0FBFCC56F1DD9DEE(L_33, _stringLiteral99654BA65919C1D4F705ADE00AD912C711685A9F, (bool)1, (bool)0, /*hidden argument*/NULL);
		// if( inAirTimer > 2)
		float L_34 = __this->get_inAirTimer_12();
		if ((!(((float)L_34) > ((float)(2.0f)))))
		{
			goto IL_0122;
		}
	}
	{
		// GetComponent<PlayerEffectManager>().PlayLandEffect();
		PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * L_35;
		L_35 = Component_GetComponent_TisPlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D_mE88BF86CEFAE820AB756B8FACF1C9CF3E50142AA(__this, /*hidden argument*/Component_GetComponent_TisPlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D_mE88BF86CEFAE820AB756B8FACF1C9CF3E50142AA_RuntimeMethod_var);
		PlayerEffectManager_PlayLandEffect_m926080EC179C8C53EF8EB69F9BD22B1C3C3174C3(L_35, /*hidden argument*/NULL);
	}

IL_0122:
	{
		// if(currentGravity != null)
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_36 = __this->get_currentGravity_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_37;
		L_37 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_36, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_0146;
		}
	}
	{
		// transform.SetParent(currentGravity.transform);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_38;
		L_38 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_39 = __this->get_currentGravity_22();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_40;
		L_40 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_39, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_38, L_40, /*hidden argument*/NULL);
	}

IL_0146:
	{
		// inAirTimer = 0;
		__this->set_inAirTimer_12((0.0f));
		// isGrounded = true;
		__this->set_isGrounded_18((bool)1);
		// }
		return;
	}

IL_0159:
	{
		// isGrounded = false;
		__this->set_isGrounded_18((bool)0);
		// }
		return;
	}
}
// System.Void PlayerLocomotion::HandleGravity()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleGravity_m945C350309BD0A0A68A43DA7F145D45FCE41089E (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  V_1;
	memset((&V_1), 0, sizeof(V_1));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_2;
	memset((&V_2), 0, sizeof(V_2));
	{
		// if (currentGravity != null)
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_0 = __this->get_currentGravity_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_00c8;
		}
	}
	{
		// if (!isShooting)
		bool L_2 = __this->get_isShooting_20();
		if (L_2)
		{
			goto IL_00d3;
		}
	}
	{
		// Vector3 gravityUp = (transform.position - currentGravity.transform.position).normalized;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4;
		L_4 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_3, /*hidden argument*/NULL);
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_5 = __this->get_currentGravity_22();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_5, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_4, L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_2), /*hidden argument*/NULL);
		V_0 = L_9;
		// localUp = transform.up;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_10;
		L_10 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11;
		L_11 = Transform_get_up_mAB753D250A30C78924D5D22B0821F1D254525C31(L_10, /*hidden argument*/NULL);
		__this->set_localUp_23(L_11);
		// Quaternion targetRotation = Quaternion.FromToRotation(localUp, gravityUp) * transform.rotation;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12 = __this->get_localUp_23();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_14;
		L_14 = Quaternion_FromToRotation_mD0EBB9993FC7C6A45724D0365B09F11F1CEADB80(L_12, L_13, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_15;
		L_15 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_16;
		L_16 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_15, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_17;
		L_17 = Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0(L_14, L_16, /*hidden argument*/NULL);
		V_1 = L_17;
		// transform.rotation = targetRotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_19 = V_1;
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_18, L_19, /*hidden argument*/NULL);
		// transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, 10 * Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_20;
		L_20 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_21;
		L_21 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_22;
		L_22 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_21, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_23 = V_1;
		float L_24;
		L_24 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_25;
		L_25 = Quaternion_Slerp_m6D2BD18286254E28D2288B51962EC71F85C7B5C8(L_22, L_23, ((float)il2cpp_codegen_multiply((float)(10.0f), (float)L_24)), /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_20, L_25, /*hidden argument*/NULL);
		// rb.AddForce((-gravityUp * currentGravity.gravityForce));
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_26 = __this->get_rb_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27 = V_0;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_28;
		L_28 = Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline(L_27, /*hidden argument*/NULL);
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_29 = __this->get_currentGravity_22();
		float L_30 = L_29->get_gravityForce_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31;
		L_31 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_28, L_30, /*hidden argument*/NULL);
		Rigidbody_AddForce_mDFB0D57C25682B826999B0074F5C0FD399C6401D(L_26, L_31, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00c8:
	{
		// localUp = Vector3.up;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Vector3_get_up_m38AECA68388D446CFADDD022B0B867293044EA50(/*hidden argument*/NULL);
		__this->set_localUp_23(L_32);
	}

IL_00d3:
	{
		// }
		return;
	}
}
// System.Void PlayerLocomotion::HandleJump()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleJump_m067C8500AFA029CCFC1A96A37B10A6A05B7FF88C (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral62012883D3A13EAB8473757C089740CE05DD45CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral6C23924C4F221C5F81332E79B4BD5A5AF61B9AF7);
		s_Il2CppMethodInitialized = true;
	}
	float V_0 = 0.0f;
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// if (isGrounded)
		bool L_0 = __this->get_isGrounded_18();
		if (!L_0)
		{
			goto IL_0070;
		}
	}
	{
		// animationManager.animator.SetBool("isJumping", true);
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_1 = __this->get_animationManager_5();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_2 = L_1->get_animator_4();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_2, _stringLiteral62012883D3A13EAB8473757C089740CE05DD45CC, (bool)1, /*hidden argument*/NULL);
		// animationManager.PlayTargetAnimation("jump", false);
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_3 = __this->get_animationManager_5();
		AnimationManager_PlayTargetAnimation_m47BDB6536609430A331D8E9D0FBFCC56F1DD9DEE(L_3, _stringLiteral6C23924C4F221C5F81332E79B4BD5A5AF61B9AF7, (bool)0, (bool)0, /*hidden argument*/NULL);
		// transform.SetParent(null);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_SetParent_m24E34EBEF76528C99AFA017F157EE8B3E3116B1E(L_4, (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 *)NULL, /*hidden argument*/NULL);
		// float jumpVelocity = Mathf.Sqrt(-4 * gravityIntensity * jumpHeight);
		float L_5 = __this->get_gravityIntensity_29();
		float L_6 = __this->get_jumpHeight_28();
		float L_7;
		L_7 = sqrtf(((float)il2cpp_codegen_multiply((float)((float)il2cpp_codegen_multiply((float)(-4.0f), (float)L_5)), (float)L_6)));
		V_0 = L_7;
		// Vector3 playerVelocity = moveDirection;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = __this->get_moveDirection_9();
		V_1 = L_8;
		// playerVelocity.y = jumpVelocity;
		float L_9 = V_0;
		(&V_1)->set_y_3(L_9);
		// rb.velocity = playerVelocity;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_10 = __this->get_rb_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_11 = V_1;
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_10, L_11, /*hidden argument*/NULL);
	}

IL_0070:
	{
		// }
		return;
	}
}
// System.Void PlayerLocomotion::HandleShoot()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleShoot_mC851DE55C6118DBCF3AC0CDEC10DAD7DF6A3A23D (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// if (!isShooting)
		bool L_0 = __this->get_isShooting_20();
		if (L_0)
		{
			goto IL_0016;
		}
	}
	{
		// if (!inputManager.aimPressed)
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_1 = __this->get_inputManager_6();
		bool L_2 = L_1->get_aimPressed_16();
		if (L_2)
		{
			goto IL_0016;
		}
	}
	{
		// return;
		return;
	}

IL_0016:
	{
		// if (Physics.Raycast(cameraObject.transform.position, cameraObject.transform.forward, out hit, shootCheckDistance, groundLayer))
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = __this->get_cameraObject_10();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4;
		L_4 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_3, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_5;
		L_5 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_4, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6 = __this->get_cameraObject_10();
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_6, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8;
		L_8 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_7, /*hidden argument*/NULL);
		float L_9 = __this->get_shootCheckDistance_21();
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_10 = __this->get_groundLayer_16();
		int32_t L_11;
		L_11 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_10, /*hidden argument*/NULL);
		bool L_12;
		L_12 = Physics_Raycast_mCBD5F7D498C246713EDDBB446E97205DA206C49C(L_5, L_8, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), L_9, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0064;
		}
	}
	{
		// StartCoroutine(ShootTowards(hit.point));
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = RaycastHit_get_point_m32F7282CBB2E13393A33BAD046BDA218E48DD21E((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_0), /*hidden argument*/NULL);
		RuntimeObject* L_14;
		L_14 = PlayerLocomotion_ShootTowards_mB986A2FD3364C113CDDAA80D800C4B812976E6B3(__this, L_13, /*hidden argument*/NULL);
		Coroutine_t899D5232EF542CB8BA70AF9ECEECA494FAA9CCB7 * L_15;
		L_15 = MonoBehaviour_StartCoroutine_m3E33706D38B23CDD179E99BAD61E32303E9CC719(__this, L_14, /*hidden argument*/NULL);
	}

IL_0064:
	{
		// }
		return;
	}
}
// System.Void PlayerLocomotion::HandleAim()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_HandleAim_m332863775AA4450513DA1775494A88D6F571EA27 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral1963A02C9DEBD760AC7D980330C0CC094F7B3211);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral440FACB401E8D64062B80C3C27A6A7C79F132229);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9E4CF29136E2EF978D3759CB6AAA6935843FC102);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (inputManager.aimPressed && !isShooting)
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_0 = __this->get_inputManager_6();
		bool L_1 = L_0->get_aimPressed_16();
		if (!L_1)
		{
			goto IL_0060;
		}
	}
	{
		bool L_2 = __this->get_isShooting_20();
		if (L_2)
		{
			goto IL_0060;
		}
	}
	{
		// if (cameraManager.currentCamera != "ShoulderCam")
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_3 = __this->get_cameraManager_8();
		String_t* L_4 = L_3->get_currentCamera_27();
		bool L_5;
		L_5 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_4, _stringLiteral1963A02C9DEBD760AC7D980330C0CC094F7B3211, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_00ed;
		}
	}
	{
		// cameraManager.pivotAngle = 0;
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_6 = __this->get_cameraManager_8();
		L_6->set_pivotAngle_19((0.0f));
		// cameraManager.lookAngle = 0;
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_7 = __this->get_cameraManager_8();
		L_7->set_lookAngle_18((0.0f));
		// cameraManager.SwitchCameras("ShoulderCam");
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_8 = __this->get_cameraManager_8();
		CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05(L_8, _stringLiteral1963A02C9DEBD760AC7D980330C0CC094F7B3211, /*hidden argument*/NULL);
		// }
		return;
	}

IL_0060:
	{
		// if (currentGravity != null)
		GravityOrbit_t8EEC8205B9A7323EC06CF263B59C50B566C22DE2 * L_9 = __this->get_currentGravity_22();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_9, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_00c6;
		}
	}
	{
		// if (!isShooting)
		bool L_11 = __this->get_isShooting_20();
		if (L_11)
		{
			goto IL_009e;
		}
	}
	{
		// if (cameraManager.currentCamera != "TopDownCam")
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_12 = __this->get_cameraManager_8();
		String_t* L_13 = L_12->get_currentCamera_27();
		bool L_14;
		L_14 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_13, _stringLiteral9E4CF29136E2EF978D3759CB6AAA6935843FC102, /*hidden argument*/NULL);
		if (!L_14)
		{
			goto IL_00ed;
		}
	}
	{
		// cameraManager.SwitchCameras("TopDownCam");
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_15 = __this->get_cameraManager_8();
		CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05(L_15, _stringLiteral9E4CF29136E2EF978D3759CB6AAA6935843FC102, /*hidden argument*/NULL);
		// } else if (cameraManager.currentCamera != "FollowCam")
		return;
	}

IL_009e:
	{
		// } else if (cameraManager.currentCamera != "FollowCam")
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_16 = __this->get_cameraManager_8();
		String_t* L_17 = L_16->get_currentCamera_27();
		bool L_18;
		L_18 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_17, _stringLiteral440FACB401E8D64062B80C3C27A6A7C79F132229, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_00ed;
		}
	}
	{
		// cameraManager.SwitchCameras("FollowCam");
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_19 = __this->get_cameraManager_8();
		CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05(L_19, _stringLiteral440FACB401E8D64062B80C3C27A6A7C79F132229, /*hidden argument*/NULL);
		// }
		return;
	}

IL_00c6:
	{
		// if (cameraManager.currentCamera != "FollowCam")
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_20 = __this->get_cameraManager_8();
		String_t* L_21 = L_20->get_currentCamera_27();
		bool L_22;
		L_22 = String_op_Inequality_mDDA2DDED3E7EF042987EB7180EE3E88105F0AAE2(L_21, _stringLiteral440FACB401E8D64062B80C3C27A6A7C79F132229, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_00ed;
		}
	}
	{
		// cameraManager.SwitchCameras("FollowCam");
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_23 = __this->get_cameraManager_8();
		CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05(L_23, _stringLiteral440FACB401E8D64062B80C3C27A6A7C79F132229, /*hidden argument*/NULL);
	}

IL_00ed:
	{
		// }
		return;
	}
}
// System.Void PlayerLocomotion::RotateOnPlanet(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion_RotateOnPlanet_m3E3C7A8C083C82F729DDBC075D4F61397E51CB61 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___MoveDirection0, const RuntimeMethod* method)
{
	float V_0 = 0.0f;
	{
		// if (moveDirection.x > 0f && moveDirection.z > 0)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_0 = __this->get_address_of_moveDirection_9();
		float L_1 = L_0->get_x_2();
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_2 = __this->get_address_of_moveDirection_9();
		float L_3 = L_2->get_z_4();
		if ((!(((float)L_3) > ((float)(0.0f)))))
		{
			goto IL_002f;
		}
	}
	{
		// rotAmount = 45;
		V_0 = (45.0f);
		// }
		goto IL_0146;
	}

IL_002f:
	{
		// else if (moveDirection.x > 0f && moveDirection.z == 0)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_4 = __this->get_address_of_moveDirection_9();
		float L_5 = L_4->get_x_2();
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_005e;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_6 = __this->get_address_of_moveDirection_9();
		float L_7 = L_6->get_z_4();
		if ((!(((float)L_7) == ((float)(0.0f)))))
		{
			goto IL_005e;
		}
	}
	{
		// rotAmount = 90;
		V_0 = (90.0f);
		// }
		goto IL_0146;
	}

IL_005e:
	{
		// else if (moveDirection.x > 0f && moveDirection.z < 0)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_8 = __this->get_address_of_moveDirection_9();
		float L_9 = L_8->get_x_2();
		if ((!(((float)L_9) > ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_10 = __this->get_address_of_moveDirection_9();
		float L_11 = L_10->get_z_4();
		if ((!(((float)L_11) < ((float)(0.0f)))))
		{
			goto IL_008d;
		}
	}
	{
		// rotAmount = 135;
		V_0 = (135.0f);
		// }
		goto IL_0146;
	}

IL_008d:
	{
		// else if (moveDirection.x == 0f && moveDirection.z < 0)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_12 = __this->get_address_of_moveDirection_9();
		float L_13 = L_12->get_x_2();
		if ((!(((float)L_13) == ((float)(0.0f)))))
		{
			goto IL_00bc;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_14 = __this->get_address_of_moveDirection_9();
		float L_15 = L_14->get_z_4();
		if ((!(((float)L_15) < ((float)(0.0f)))))
		{
			goto IL_00bc;
		}
	}
	{
		// rotAmount = 180;
		V_0 = (180.0f);
		// }
		goto IL_0146;
	}

IL_00bc:
	{
		// else if (moveDirection.x < 0f && moveDirection.z < 0)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_16 = __this->get_address_of_moveDirection_9();
		float L_17 = L_16->get_x_2();
		if ((!(((float)L_17) < ((float)(0.0f)))))
		{
			goto IL_00e8;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_18 = __this->get_address_of_moveDirection_9();
		float L_19 = L_18->get_z_4();
		if ((!(((float)L_19) < ((float)(0.0f)))))
		{
			goto IL_00e8;
		}
	}
	{
		// rotAmount = -135;
		V_0 = (-135.0f);
		// }
		goto IL_0146;
	}

IL_00e8:
	{
		// else if (moveDirection.x < 0f && moveDirection.z == 0)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_20 = __this->get_address_of_moveDirection_9();
		float L_21 = L_20->get_x_2();
		if ((!(((float)L_21) < ((float)(0.0f)))))
		{
			goto IL_0114;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_22 = __this->get_address_of_moveDirection_9();
		float L_23 = L_22->get_z_4();
		if ((!(((float)L_23) == ((float)(0.0f)))))
		{
			goto IL_0114;
		}
	}
	{
		// rotAmount = -90;
		V_0 = (-90.0f);
		// }
		goto IL_0146;
	}

IL_0114:
	{
		// else if (moveDirection.x < 0f && moveDirection.z > 0)
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_24 = __this->get_address_of_moveDirection_9();
		float L_25 = L_24->get_x_2();
		if ((!(((float)L_25) < ((float)(0.0f)))))
		{
			goto IL_0140;
		}
	}
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * L_26 = __this->get_address_of_moveDirection_9();
		float L_27 = L_26->get_z_4();
		if ((!(((float)L_27) > ((float)(0.0f)))))
		{
			goto IL_0140;
		}
	}
	{
		// rotAmount = -45;
		V_0 = (-45.0f);
		// } else
		goto IL_0146;
	}

IL_0140:
	{
		// rotAmount = 0;
		V_0 = (0.0f);
	}

IL_0146:
	{
		// transform.GetChild(0).localRotation = Quaternion.Euler(0, rotAmount, 0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_29;
		L_29 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_28, 0, /*hidden argument*/NULL);
		float L_30 = V_0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_31;
		L_31 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), L_30, (0.0f), /*hidden argument*/NULL);
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_29, L_31, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Collections.IEnumerator PlayerLocomotion::ShootTowards(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* PlayerLocomotion_ShootTowards_mB986A2FD3364C113CDDAA80D800C4B812976E6B3 (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___point0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * L_0 = (U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F *)il2cpp_codegen_object_new(U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F_il2cpp_TypeInfo_var);
		U3CShootTowardsU3Ed__37__ctor_mE9BC491319E93765A987C60044EB75C14949AD81(L_0, 0, /*hidden argument*/NULL);
		U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * L_1 = L_0;
		L_1->set_U3CU3E4__this_2(__this);
		U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * L_2 = L_1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___point0;
		L_2->set_point_3(L_3);
		return L_2;
	}
}
// System.Void PlayerLocomotion::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerLocomotion__ctor_mA06AA9ED0E94FEAB3ED537367315965BB1D09B5F (PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * __this, const RuntimeMethod* method)
{
	{
		// public float raycastHeightOffset = 0.5f;
		__this->set_raycastHeightOffset_15((0.5f));
		// public float shootCheckDistance = 1000f;
		__this->set_shootCheckDistance_21((1000.0f));
		// public float walkingspeed = 5f;
		__this->set_walkingspeed_24((5.0f));
		// public float runningspeed = 10;
		__this->set_runningspeed_25((10.0f));
		// public float sprintingSpeed = 14;
		__this->set_sprintingSpeed_26((14.0f));
		// public float rotationSpeed = 15;
		__this->set_rotationSpeed_27((15.0f));
		// public float jumpHeight = 3;
		__this->set_jumpHeight_28((3.0f));
		// public float gravityIntensity = -15;
		__this->set_gravityIntensity_29((-15.0f));
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerManager::Awake()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerManager_Awake_m4E0FE19C5F34114E28F28F04CCF33C2E34C743E7 (PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponentInChildren_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m654193278BB56948AB820313E9D35E6D6A89798F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0_m14E499548B982EF24D66C845F258B527D0319A41_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m6CCCFD6651416AD5F042C75E8B8CF01C82631CA9_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D_mE88BF86CEFAE820AB756B8FACF1C9CF3E50142AA_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_FindObjectOfType_TisCameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_m1BDBA1D013C9E2C431C864109ADEA485E4716CF2_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// inputManager = GetComponent<InputManager>();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_0;
		L_0 = Component_GetComponent_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m6CCCFD6651416AD5F042C75E8B8CF01C82631CA9(__this, /*hidden argument*/Component_GetComponent_TisInputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_m6CCCFD6651416AD5F042C75E8B8CF01C82631CA9_RuntimeMethod_var);
		__this->set_inputManager_4(L_0);
		// animator = GetComponentInChildren<Animator>();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1;
		L_1 = Component_GetComponentInChildren_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m654193278BB56948AB820313E9D35E6D6A89798F(__this, /*hidden argument*/Component_GetComponentInChildren_TisAnimator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149_m654193278BB56948AB820313E9D35E6D6A89798F_RuntimeMethod_var);
		__this->set_animator_6(L_1);
		// cameraManager = FindObjectOfType<CameraManager>();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_2;
		L_2 = Object_FindObjectOfType_TisCameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_m1BDBA1D013C9E2C431C864109ADEA485E4716CF2(/*hidden argument*/Object_FindObjectOfType_TisCameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A_m1BDBA1D013C9E2C431C864109ADEA485E4716CF2_RuntimeMethod_var);
		__this->set_cameraManager_5(L_2);
		// playerLocomotion = GetComponent<PlayerLocomotion>();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_3;
		L_3 = Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5(__this, /*hidden argument*/Component_GetComponent_TisPlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795_m97CFEEA029CF0F3B33ED1ED5EA03B5095D59E1B5_RuntimeMethod_var);
		__this->set_playerLocomotion_7(L_3);
		// chargeManager = GetComponent<ChargeManager>();
		ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * L_4;
		L_4 = Component_GetComponent_TisChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0_m14E499548B982EF24D66C845F258B527D0319A41(__this, /*hidden argument*/Component_GetComponent_TisChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0_m14E499548B982EF24D66C845F258B527D0319A41_RuntimeMethod_var);
		__this->set_chargeManager_8(L_4);
		// playerEffectManager = GetComponent<PlayerEffectManager>();
		PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * L_5;
		L_5 = Component_GetComponent_TisPlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D_mE88BF86CEFAE820AB756B8FACF1C9CF3E50142AA(__this, /*hidden argument*/Component_GetComponent_TisPlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D_mE88BF86CEFAE820AB756B8FACF1C9CF3E50142AA_RuntimeMethod_var);
		__this->set_playerEffectManager_9(L_5);
		// }
		return;
	}
}
// System.Void PlayerManager::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerManager_Update_mA0EE0A117D34258508935E958CF715FD930D066A (PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * __this, const RuntimeMethod* method)
{
	{
		// inputManager.HandleAllInputs();
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_0 = __this->get_inputManager_4();
		InputManager_HandleAllInputs_mCDF44B0217785B2D7D4D6702B18DC531D7B0FC75(L_0, /*hidden argument*/NULL);
		// chargeManager.UpdateChargeCounter(playerLocomotion.isGrounded);
		ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * L_1 = __this->get_chargeManager_8();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_2 = __this->get_playerLocomotion_7();
		bool L_3 = L_2->get_isGrounded_18();
		ChargeManager_UpdateChargeCounter_m55E422F13E6183B524F031B4455E9374F0B8CB8F(L_1, L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerManager::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerManager_FixedUpdate_mE0318652322E39A7492C925BC16F8B5D92F88788 (PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * __this, const RuntimeMethod* method)
{
	{
		// playerLocomotion.HandleAllMovement();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_0 = __this->get_playerLocomotion_7();
		PlayerLocomotion_HandleAllMovement_m3AA12244C0EBE67CD8D4A92D077D8058849BD2F1(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerManager::LateUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerManager_LateUpdate_m88A1B42842248AF2665C754B4FA56F3E25567AA4 (PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral54ADB4B256E220AC1660EEB0B81E40D259DD1203);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral62012883D3A13EAB8473757C089740CE05DD45CC);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB1915CC31B6BC62A90E9FD4F876A00AA4B73C4ED);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDF953B1BAAE56A3E1998B6A1DC98AC472A4DAF75);
		s_Il2CppMethodInitialized = true;
	}
	{
		// cameraManager.HandleAllCameraMovement();
		CameraManager_t8E36175FB066C9E1F863A5D7B3A300EAC805D01A * L_0 = __this->get_cameraManager_5();
		CameraManager_HandleAllCameraMovement_m58737EA7EFF6EFEECC7F56CB66B7254E5BBB48F7(L_0, /*hidden argument*/NULL);
		// isUsingRootMotion = animator.GetBool("isUsingRootMotion");
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_1 = __this->get_animator_6();
		bool L_2;
		L_2 = Animator_GetBool_m69AFEA8176E7FB312C264773784D6D6B08A80C0A(L_1, _stringLiteralB1915CC31B6BC62A90E9FD4F876A00AA4B73C4ED, /*hidden argument*/NULL);
		__this->set_isUsingRootMotion_11(L_2);
		// isInteracting = animator.GetBool("isInteracting");
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3 = __this->get_animator_6();
		bool L_4;
		L_4 = Animator_GetBool_m69AFEA8176E7FB312C264773784D6D6B08A80C0A(L_3, _stringLiteral54ADB4B256E220AC1660EEB0B81E40D259DD1203, /*hidden argument*/NULL);
		__this->set_isInteracting_10(L_4);
		// playerLocomotion.isJumping = animator.GetBool("isJumping");
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_5 = __this->get_playerLocomotion_7();
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_6 = __this->get_animator_6();
		bool L_7;
		L_7 = Animator_GetBool_m69AFEA8176E7FB312C264773784D6D6B08A80C0A(L_6, _stringLiteral62012883D3A13EAB8473757C089740CE05DD45CC, /*hidden argument*/NULL);
		L_5->set_isJumping_19(L_7);
		// animator.SetBool("isGrounded", playerLocomotion.isGrounded);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_8 = __this->get_animator_6();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_9 = __this->get_playerLocomotion_7();
		bool L_10 = L_9->get_isGrounded_18();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_8, _stringLiteralDF953B1BAAE56A3E1998B6A1DC98AC472A4DAF75, L_10, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void PlayerManager::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void PlayerManager__ctor_m4C7CA12A8243D6CA73C1EA65B361E7B717070471 (PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ResetBool::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResetBool_OnStateEnter_mE8C32A0F0CA360AE9778EE55B323845FD797EE5B (ResetBool_t4D03DEE6EEDE94D9EA59BF9BA8FE231B8BF18894 * __this, Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator0, AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA  ___stateInfo1, int32_t ___layerIndex2, const RuntimeMethod* method)
{
	{
		// animator.SetBool(isInteractingBool, isInteractingStatus);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = ___animator0;
		String_t* L_1 = __this->get_isInteractingBool_4();
		bool L_2 = __this->get_isInteractingStatus_5();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_0, L_1, L_2, /*hidden argument*/NULL);
		// animator.SetBool(isUsingRootMotionBool, isUsingRootMotionStatus);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_3 = ___animator0;
		String_t* L_4 = __this->get_isUsingRootMotionBool_6();
		bool L_5 = __this->get_isUsingRootMotionStatus_7();
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_3, L_4, L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ResetBool::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResetBool__ctor_m0063AB38B1D283936F6762CB900B96A554416C2E (ResetBool_t4D03DEE6EEDE94D9EA59BF9BA8FE231B8BF18894 * __this, const RuntimeMethod* method)
{
	{
		StateMachineBehaviour__ctor_mDB0650FD738799E5880150E656D4A88524D0EBE0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ResetIsJumping::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResetIsJumping_OnStateExit_mD0A4B7ECB184C90C720789B24D7EAD23DDD460F9 (ResetIsJumping_t41CFA6F9D05CFC07776121B3C64616CBF95F9D59 * __this, Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * ___animator0, AnimatorStateInfo_t052E146D2DB1EC155950ECA45734BF57134258AA  ___stateInfo1, int32_t ___layerIndex2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral62012883D3A13EAB8473757C089740CE05DD45CC);
		s_Il2CppMethodInitialized = true;
	}
	{
		// animator.SetBool("isJumping", false);
		Animator_t9DD1D43680A61D65A3C98C6EFF559709DC9CE149 * L_0 = ___animator0;
		Animator_SetBool_m34E2E9785A47A3AE94E804004425C333C36CCD43(L_0, _stringLiteral62012883D3A13EAB8473757C089740CE05DD45CC, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ResetIsJumping::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ResetIsJumping__ctor_mEF667F0D15C7665F3BBD2AB1BEAE9EAECBCB654E (ResetIsJumping_t41CFA6F9D05CFC07776121B3C64616CBF95F9D59 * __this, const RuntimeMethod* method)
{
	{
		StateMachineBehaviour__ctor_mDB0650FD738799E5880150E656D4A88524D0EBE0(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Rotatator::get_Randomize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Rotatator_get_Randomize_m72AB81666EFF860D4401A6284EF8226FA6225A2D (Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D * __this, const RuntimeMethod* method)
{
	{
		// return randomize;
		bool L_0 = __this->get_randomize_7();
		return L_0;
	}
}
// System.Void Rotatator::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotatator_Start_mD617A7EB63C9EDB6AE7D775828E445553FD7718F (Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4FA73C839BE532F9F38159AA62F306192A226448);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral781843FF3C5C4AE705E16F522171558E9CB73B4C);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(meshObject == null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_meshObject_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0048;
		}
	}
	{
		// meshObject = transform.Find("planet");
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2;
		L_2 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1(L_2, _stringLiteral781843FF3C5C4AE705E16F522171558E9CB73B4C, /*hidden argument*/NULL);
		__this->set_meshObject_5(L_3);
		// if (meshObject == null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_4 = __this->get_meshObject_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_5;
		L_5 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_4, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		// meshObject = transform.Find("w2");
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Transform_Find_mB1687901A4FB0D562C44A93CC67CD35DCFCAABA1(L_6, _stringLiteral4FA73C839BE532F9F38159AA62F306192A226448, /*hidden argument*/NULL);
		__this->set_meshObject_5(L_7);
	}

IL_0048:
	{
		// if(randomize)
		bool L_8 = __this->get_randomize_7();
		if (!L_8)
		{
			goto IL_0084;
		}
	}
	{
		// rotation = new Vector3(RandFloat(), RandFloat(), RandFloat());
		float L_9;
		L_9 = Rotatator_RandFloat_m2C7FA1F9E460B203C0185389414813034AF1103F(__this, /*hidden argument*/NULL);
		float L_10;
		L_10 = Rotatator_RandFloat_m2C7FA1F9E460B203C0185389414813034AF1103F(__this, /*hidden argument*/NULL);
		float L_11;
		L_11 = Rotatator_RandFloat_m2C7FA1F9E460B203C0185389414813034AF1103F(__this, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), L_9, L_10, L_11, /*hidden argument*/NULL);
		__this->set_rotation_4(L_12);
		// rotationSpeed = Random.Range(minSpeed,maxSpeed);
		float L_13 = __this->get_minSpeed_9();
		float L_14 = __this->get_maxSpeed_8();
		float L_15;
		L_15 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2(L_13, L_14, /*hidden argument*/NULL);
		__this->set_rotationSpeed_6(L_15);
	}

IL_0084:
	{
		// }
		return;
	}
}
// System.Single Rotatator::RandFloat()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR float Rotatator_RandFloat_m2C7FA1F9E460B203C0185389414813034AF1103F (Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D * __this, const RuntimeMethod* method)
{
	{
		// return Random.Range(0f,1.01f);
		float L_0;
		L_0 = Random_Range_mC15372D42A9ABDCAC3DE82E114D60A40C9C311D2((0.0f), (1.00999999f), /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void Rotatator::FixedUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotatator_FixedUpdate_m812453B2007FE7533F85A583A27C7B642389D38F (Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if(meshObject != null)
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = __this->get_meshObject_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_1;
		L_1 = Object_op_Inequality_mE1F187520BD83FB7D86A6D850710C4D42B864E90(L_0, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		// meshObject.Rotate(rotation, rotationSpeed * Time.deltaTime);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_2 = __this->get_meshObject_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = __this->get_rotation_4();
		float L_4 = __this->get_rotationSpeed_6();
		float L_5;
		L_5 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		Transform_Rotate_m2AA745C4A796363462642A13251E8971D5C7F4DC(L_2, L_3, ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), /*hidden argument*/NULL);
	}

IL_002b:
	{
		// }
		return;
	}
}
// System.Void Rotatator::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Rotatator__ctor_m5C149D4546B83274AC5072782FA5A45B26B03059 (Rotatator_t6ECBE238B8AD208543F7D96A3C09D08D2070025D * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void SliderScripts::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SliderScripts_Start_mB80441D3AF218ACA367485631119961BEF34F55F (SliderScripts_t01CD33323C1390AABFEA3597355DD017703D404F * __this, const RuntimeMethod* method)
{
	{
		// FillSlider();
		SliderScripts_FillSlider_m427E4FF54E88039DF974427DED78E5DCC46439C6(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SliderScripts::FillSlider()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SliderScripts_FillSlider_m427E4FF54E88039DF974427DED78E5DCC46439C6 (SliderScripts_t01CD33323C1390AABFEA3597355DD017703D404F * __this, const RuntimeMethod* method)
{
	{
		// fill.fillAmount = slider.value;
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_0 = __this->get_fill_5();
		Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * L_1 = __this->get_slider_4();
		float L_2;
		L_2 = VirtFuncInvoker0< float >::Invoke(46 /* System.Single UnityEngine.UI.Slider::get_value() */, L_1);
		Image_set_fillAmount_m1D28CFC9B15A45AB6C561AA42BD8F305605E9E3C(L_0, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void SliderScripts::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SliderScripts__ctor_mB77386C370B17B07B48C60928EC809FF4CC4E84E (SliderScripts_t01CD33323C1390AABFEA3597355DD017703D404F * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void PlayerLocomotion/<ShootTowards>d__37::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CShootTowardsU3Ed__37__ctor_mE9BC491319E93765A987C60044EB75C14949AD81 (U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * __this, int32_t ___U3CU3E1__state0, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___U3CU3E1__state0;
		__this->set_U3CU3E1__state_0(L_0);
		return;
	}
}
// System.Void PlayerLocomotion/<ShootTowards>d__37::System.IDisposable.Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CShootTowardsU3Ed__37_System_IDisposable_Dispose_m337C2BA4C24F8FD0A81AC2A9DF8A3135D11C8305 (U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * __this, const RuntimeMethod* method)
{
	{
		return;
	}
}
// System.Boolean PlayerLocomotion/<ShootTowards>d__37::MoveNext()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool U3CShootTowardsU3Ed__37_MoveNext_mF5A6C9315BF2D1C229E4010B3E11E3A9D8B4C053 (U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9C39D1EEC1890B28711700FDEE2AC9BED5AB7172);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * V_1 = NULL;
	RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89  V_2;
	memset((&V_2), 0, sizeof(V_2));
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_3;
	memset((&V_3), 0, sizeof(V_3));
	PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * G_B8_0 = NULL;
	PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * G_B7_0 = NULL;
	int32_t G_B9_0 = 0;
	PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * G_B9_1 = NULL;
	{
		int32_t L_0 = __this->get_U3CU3E1__state_0();
		V_0 = L_0;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_1 = __this->get_U3CU3E4__this_2();
		V_1 = L_1;
		int32_t L_2 = V_0;
		switch (L_2)
		{
			case 0:
			{
				goto IL_0026;
			}
			case 1:
			{
				goto IL_00eb;
			}
			case 2:
			{
				goto IL_0198;
			}
			case 3:
			{
				goto IL_0206;
			}
		}
	}
	{
		return (bool)0;
	}

IL_0026:
	{
		__this->set_U3CU3E1__state_0((-1));
		// playerManager.playerEffectManager.ToggleShootingEffects(true);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_3 = V_1;
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_4 = L_3->get_playerManager_4();
		PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * L_5 = L_4->get_playerEffectManager_9();
		PlayerEffectManager_ToggleShootingEffects_m3091CE05E2C37AF30BF81F8DAA06E40B5A8FCE85(L_5, (bool)1, /*hidden argument*/NULL);
		// transform.GetChild(0).localRotation = Quaternion.Euler(0, 0, 0);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_6 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_7;
		L_7 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_6, /*hidden argument*/NULL);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = Transform_GetChild_mA7D94BEFF0144F76561D9B8FED61C5C939EC1F1C(L_7, 0, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_9;
		L_9 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		Transform_set_localRotation_m1A9101457EC4653AFC93FCC4065A29F2C78FA62C(L_8, L_9, /*hidden argument*/NULL);
		// animationManager.PlayTargetAnimation("shoot", true, true);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_10 = V_1;
		AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709 * L_11 = L_10->get_animationManager_5();
		AnimationManager_PlayTargetAnimation_m47BDB6536609430A331D8E9D0FBFCC56F1DD9DEE(L_11, _stringLiteral9C39D1EEC1890B28711700FDEE2AC9BED5AB7172, (bool)1, (bool)1, /*hidden argument*/NULL);
		// inputManager.aimPressed = false;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_12 = V_1;
		InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A * L_13 = L_12->get_inputManager_6();
		L_13->set_aimPressed_16((bool)0);
		// isShooting = true;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_14 = V_1;
		L_14->set_isShooting_20((bool)1);
		// Vector3 direction = (point - transform.position).normalized;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_15 = __this->get_point_3();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_16 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_17;
		L_17 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_16, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_18;
		L_18 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_17, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_19;
		L_19 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_15, L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_20;
		L_20 = Vector3_get_normalized_m2FA6DF38F97BDA4CCBDAE12B9FE913A241DAC8D5((Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E *)(&V_3), /*hidden argument*/NULL);
		__this->set_U3CdirectionU3E5__2_4(L_20);
		// transform.rotation = Quaternion.LookRotation(direction);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_21 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_21, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_23 = __this->get_U3CdirectionU3E5__2_4();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_24;
		L_24 = Quaternion_LookRotation_m1B0BEBEBCC384324A6771B9EAC89761F73E1D6BF(L_23, /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_22, L_24, /*hidden argument*/NULL);
		// rb.velocity = Vector3.zero;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_25 = V_1;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_26 = L_25->get_rb_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_27;
		L_27 = Vector3_get_zero_m1A8F7993167785F750B6B01762D22C2597C84EF6(/*hidden argument*/NULL);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_26, L_27, /*hidden argument*/NULL);
		// yield return new WaitForSeconds(0.3f);
		WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 * L_28 = (WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013 *)il2cpp_codegen_object_new(WaitForSeconds_t8F9189BE6E467C98C99177038881F8982E0E4013_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_mD298C4CB9532BBBDE172FC40F3397E30504038D4(L_28, (0.300000012f), /*hidden argument*/NULL);
		__this->set_U3CU3E2__current_1(L_28);
		__this->set_U3CU3E1__state_0(1);
		return (bool)1;
	}

IL_00eb:
	{
		__this->set_U3CU3E1__state_0((-1));
		// rb.velocity = (direction * 50);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_29 = V_1;
		Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * L_30 = L_29->get_rb_11();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_31 = __this->get_U3CdirectionU3E5__2_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_32;
		L_32 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_31, (50.0f), /*hidden argument*/NULL);
		Rigidbody_set_velocity_m8DC0988916EB38DFD7D4584830B41D79140BF18D(L_30, L_32, /*hidden argument*/NULL);
		goto IL_019f;
	}

IL_0112:
	{
		// if (isGrounded)
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_33 = V_1;
		bool L_34 = L_33->get_isGrounded_18();
		if (L_34)
		{
			goto IL_01b4;
		}
	}
	{
		// chargeManager.chargeCounter -= chargeManager.drainSpeed * Time.deltaTime;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_35 = V_1;
		ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * L_36 = L_35->get_chargeManager_7();
		ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * L_37 = L_36;
		float L_38 = L_37->get_chargeCounter_4();
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_39 = V_1;
		ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * L_40 = L_39->get_chargeManager_7();
		float L_41 = L_40->get_drainSpeed_6();
		float L_42;
		L_42 = Time_get_deltaTime_mCC15F147DA67F38C74CE408FB5D7FF4A87DA2290(/*hidden argument*/NULL);
		L_37->set_chargeCounter_4(((float)il2cpp_codegen_subtract((float)L_38, (float)((float)il2cpp_codegen_multiply((float)L_41, (float)L_42)))));
		// if (Physics.SphereCast(transform.position, 5, transform.forward, out hit, groundLayer))
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_43 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_44;
		L_44 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_43, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_45;
		L_45 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_44, /*hidden argument*/NULL);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_46 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_47;
		L_47 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_46, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_48;
		L_48 = Transform_get_forward_mD850B9ECF892009E3485408DC0D375165B7BF053(L_47, /*hidden argument*/NULL);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_49 = V_1;
		LayerMask_t5FA647D8C300EA0621360CA4424717C3C73190A8  L_50 = L_49->get_groundLayer_16();
		int32_t L_51;
		L_51 = LayerMask_op_Implicit_mD89E9970822613D8D19B2EBCA36C79391C287BE0(L_50, /*hidden argument*/NULL);
		bool L_52;
		L_52 = Physics_SphereCast_m2E200DFD71A597968D4F50B8A6284F2E622B4C57(L_45, (5.0f), L_48, (RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), ((float)((float)L_51)), /*hidden argument*/NULL);
		if (!L_52)
		{
			goto IL_0188;
		}
	}
	{
		// isGrounded = (hit.distance < 2) ? true : false;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_53 = V_1;
		float L_54;
		L_54 = RaycastHit_get_distance_m85FCA98D7957C3BF1D449CA1B48C116CCD6226FA((RaycastHit_t59E5AEC8FE13BFA2ACBB6FFBDB7585FFB7288F89 *)(&V_2), /*hidden argument*/NULL);
		G_B7_0 = L_53;
		if ((((float)L_54) < ((float)(2.0f))))
		{
			G_B8_0 = L_53;
			goto IL_0182;
		}
	}
	{
		G_B9_0 = 0;
		G_B9_1 = G_B7_0;
		goto IL_0183;
	}

IL_0182:
	{
		G_B9_0 = 1;
		G_B9_1 = G_B8_0;
	}

IL_0183:
	{
		G_B9_1->set_isGrounded_18((bool)G_B9_0);
	}

IL_0188:
	{
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(2);
		return (bool)1;
	}

IL_0198:
	{
		__this->set_U3CU3E1__state_0((-1));
	}

IL_019f:
	{
		// while (chargeManager.chargeCounter > 0)
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_55 = V_1;
		ChargeManager_t2FCDB220A91289DF20A084AC0F1E1A03F181D0F0 * L_56 = L_55->get_chargeManager_7();
		float L_57 = L_56->get_chargeCounter_4();
		if ((((float)L_57) > ((float)(0.0f))))
		{
			goto IL_0112;
		}
	}

IL_01b4:
	{
		// playerManager.playerEffectManager.ToggleShootingEffects(false);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_58 = V_1;
		PlayerManager_tA626E96D79E0168DAC909A130EC9A26A533AAD48 * L_59 = L_58->get_playerManager_4();
		PlayerEffectManager_t9D66AB87C5004FE4B5E97241AA84EB1F8E2B797D * L_60 = L_59->get_playerEffectManager_9();
		PlayerEffectManager_ToggleShootingEffects_m3091CE05E2C37AF30BF81F8DAA06E40B5A8FCE85(L_60, (bool)0, /*hidden argument*/NULL);
		// isShooting = false;
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_61 = V_1;
		L_61->set_isShooting_20((bool)0);
		// transform.rotation = Quaternion.Euler(0, transform.localEulerAngles.y, 0);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_62 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_63;
		L_63 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_62, /*hidden argument*/NULL);
		PlayerLocomotion_tCED5A5FAFCBB2C25B308DC37F93CA3675820C795 * L_64 = V_1;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_65;
		L_65 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_64, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_66;
		L_66 = Transform_get_localEulerAngles_m4C442107F523737ADAB54855FDC1777A9B71D545(L_65, /*hidden argument*/NULL);
		float L_67 = L_66.get_y_3();
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_68;
		L_68 = Quaternion_Euler_m37BF99FFFA09F4B3F83DC066641B82C59B19A9C3((0.0f), L_67, (0.0f), /*hidden argument*/NULL);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_63, L_68, /*hidden argument*/NULL);
		// yield return null;
		__this->set_U3CU3E2__current_1(NULL);
		__this->set_U3CU3E1__state_0(3);
		return (bool)1;
	}

IL_0206:
	{
		__this->set_U3CU3E1__state_0((-1));
		// }
		return (bool)0;
	}
}
// System.Object PlayerLocomotion/<ShootTowards>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CShootTowardsU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m519592771F59C1DBF16A8D149B9E59D99C841530 (U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
// System.Void PlayerLocomotion/<ShootTowards>d__37::System.Collections.IEnumerator.Reset()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CShootTowardsU3Ed__37_System_Collections_IEnumerator_Reset_mD79DF504A1584917D6E9E7BF7988BCC1FE6670E8 (U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * __this, const RuntimeMethod* method)
{
	{
		NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 * L_0 = (NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339 *)il2cpp_codegen_object_new(((RuntimeClass*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&NotSupportedException_tB9D89F0E9470A2C423D239D7C68EE0CFD77F9339_il2cpp_TypeInfo_var)));
		NotSupportedException__ctor_m3EA81A5B209A87C3ADA47443F2AFFF735E5256EE(L_0, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_0, ((RuntimeMethod*)il2cpp_codegen_initialize_runtime_metadata_inline((uintptr_t*)&U3CShootTowardsU3Ed__37_System_Collections_IEnumerator_Reset_mD79DF504A1584917D6E9E7BF7988BCC1FE6670E8_RuntimeMethod_var)));
	}
}
// System.Object PlayerLocomotion/<ShootTowards>d__37::System.Collections.IEnumerator.get_Current()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * U3CShootTowardsU3Ed__37_System_Collections_IEnumerator_get_Current_m09758056EC4B6852822125181871F7A312EDA594 (U3CShootTowardsU3Ed__37_t316495FA6BD10B822CB3D981389204CFC763F66F * __this, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = __this->get_U3CU3E2__current_1();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector2__ctor_m9F1F2D5EB5D1FF7091BB527AC8A72CBB309D115E_inline (Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * __this, float ___x0, float ___y1, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_0(L_0);
		float L_1 = ___y1;
		__this->set_y_1(L_1);
		return;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Addition_mEE4F672B923CCB184C39AABCA33443DB218E50E0_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_add((float)L_1, (float)L_3)), ((float)il2cpp_codegen_add((float)L_5, (float)L_7)), ((float)il2cpp_codegen_add((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_UnaryNegation_m362EA356F4CADEDB39F965A0DBDED6EA890925F7_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___a0;
		float L_3 = L_2.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), ((-L_1)), ((-L_3)), ((-L_5)), /*hidden argument*/NULL);
		V_0 = L_6;
		goto IL_001e;
	}

IL_001e:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7 = V_0;
		return L_7;
	}
}
