﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Boolean Rotatator::get_Randomize()
extern void Rotatator_get_Randomize_m72AB81666EFF860D4401A6284EF8226FA6225A2D (void);
// 0x00000002 System.Void Rotatator::Start()
extern void Rotatator_Start_mD617A7EB63C9EDB6AE7D775828E445553FD7718F (void);
// 0x00000003 System.Single Rotatator::RandFloat()
extern void Rotatator_RandFloat_m2C7FA1F9E460B203C0185389414813034AF1103F (void);
// 0x00000004 System.Void Rotatator::FixedUpdate()
extern void Rotatator_FixedUpdate_m812453B2007FE7533F85A583A27C7B642389D38F (void);
// 0x00000005 System.Void Rotatator::.ctor()
extern void Rotatator__ctor_m5C149D4546B83274AC5072782FA5A45B26B03059 (void);
// 0x00000006 System.Void SliderScripts::Start()
extern void SliderScripts_Start_mB80441D3AF218ACA367485631119961BEF34F55F (void);
// 0x00000007 System.Void SliderScripts::FillSlider()
extern void SliderScripts_FillSlider_m427E4FF54E88039DF974427DED78E5DCC46439C6 (void);
// 0x00000008 System.Void SliderScripts::.ctor()
extern void SliderScripts__ctor_mB77386C370B17B07B48C60928EC809FF4CC4E84E (void);
// 0x00000009 System.Void AnimationManager::Awake()
extern void AnimationManager_Awake_m627DE07938651E02F0A44463872756F99CEBE1A0 (void);
// 0x0000000A System.Void AnimationManager::PlayTargetAnimation(System.String,System.Boolean,System.Boolean)
extern void AnimationManager_PlayTargetAnimation_m47BDB6536609430A331D8E9D0FBFCC56F1DD9DEE (void);
// 0x0000000B System.Void AnimationManager::UpdateAnimator(System.Single,System.Single)
extern void AnimationManager_UpdateAnimator_m3F09B8D6E2E97AF94A0296FF32F5F23AEF011D94 (void);
// 0x0000000C System.Void AnimationManager::OnAnimatorMove()
extern void AnimationManager_OnAnimatorMove_mAC9F008B4AD7DF4D2A31F87D4D9044CED4677F00 (void);
// 0x0000000D System.Void AnimationManager::.ctor()
extern void AnimationManager__ctor_m0C4622326DC614221E503860BBCC73F91BFB9F20 (void);
// 0x0000000E System.Void ResetBool::OnStateEnter(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void ResetBool_OnStateEnter_mE8C32A0F0CA360AE9778EE55B323845FD797EE5B (void);
// 0x0000000F System.Void ResetBool::.ctor()
extern void ResetBool__ctor_m0063AB38B1D283936F6762CB900B96A554416C2E (void);
// 0x00000010 System.Void ResetIsJumping::OnStateExit(UnityEngine.Animator,UnityEngine.AnimatorStateInfo,System.Int32)
extern void ResetIsJumping_OnStateExit_mD0A4B7ECB184C90C720789B24D7EAD23DDD460F9 (void);
// 0x00000011 System.Void ResetIsJumping::.ctor()
extern void ResetIsJumping__ctor_mEF667F0D15C7665F3BBD2AB1BEAE9EAECBCB654E (void);
// 0x00000012 System.Void CameraManager::Awake()
extern void CameraManager_Awake_m9FA8A0FE2F4DE2E70076A4A15C9885BE5E4775A4 (void);
// 0x00000013 System.Void CameraManager::Start()
extern void CameraManager_Start_m3A8F66B836B9A9BC419B62EF88451549BBC9CCD0 (void);
// 0x00000014 System.Void CameraManager::HandleAllCameraMovement()
extern void CameraManager_HandleAllCameraMovement_m58737EA7EFF6EFEECC7F56CB66B7254E5BBB48F7 (void);
// 0x00000015 System.Void CameraManager::FollowTarget()
extern void CameraManager_FollowTarget_m3B321F6088D72138EB27285283F8E849613ED10E (void);
// 0x00000016 System.Void CameraManager::RotateCamera()
extern void CameraManager_RotateCamera_m2CDBD3C9BA768CE220369FD4956F27583D5D7066 (void);
// 0x00000017 System.Void CameraManager::HandleCameraCollisions()
extern void CameraManager_HandleCameraCollisions_mBC0FF6768EBC02EDDE8575444230061EDEC7C2FA (void);
// 0x00000018 System.Void CameraManager::HandleShoulderCam()
extern void CameraManager_HandleShoulderCam_mD187D8FB9D6CD4C27FF02E76F8C1843D1D5E91BB (void);
// 0x00000019 System.Void CameraManager::HandleTopDownCam()
extern void CameraManager_HandleTopDownCam_m9F9BE9182993AABDE5F0B9B0E97E0E42B05EA8C7 (void);
// 0x0000001A System.Void CameraManager::SwitchCameras(System.String)
extern void CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05 (void);
// 0x0000001B UnityEngine.GameObject CameraManager::GetCurrentCamera()
extern void CameraManager_GetCurrentCamera_m69A5FB8797F2209A65B33A8179AD7925F5ECCDD6 (void);
// 0x0000001C System.Void CameraManager::.ctor()
extern void CameraManager__ctor_m932EDBCBAE89115380AA929ED5BC9823B9371185 (void);
// 0x0000001D System.Void ChargeManager::Awake()
extern void ChargeManager_Awake_m50D7F099C3A9FDDBAC1ECCCE56FD551D99E1F8B0 (void);
// 0x0000001E System.Void ChargeManager::UpdateChargeCounter(System.Boolean)
extern void ChargeManager_UpdateChargeCounter_m55E422F13E6183B524F031B4455E9374F0B8CB8F (void);
// 0x0000001F System.Void ChargeManager::.ctor()
extern void ChargeManager__ctor_m80FE8C0DE5AA7ECAEC4C03F7195E9C0C947673DE (void);
// 0x00000020 System.Void GravityOrbit::FixedUpdate()
extern void GravityOrbit_FixedUpdate_m6635C617B4B753B7C2762BF0A9A2A048BE0D437C (void);
// 0x00000021 System.Void GravityOrbit::OnTriggerEnter(UnityEngine.Collider)
extern void GravityOrbit_OnTriggerEnter_mC4D11D4D6E0E89BD5362D141F6BE80A080060DC1 (void);
// 0x00000022 System.Void GravityOrbit::OnTriggerExit(UnityEngine.Collider)
extern void GravityOrbit_OnTriggerExit_m56A083FFF3C8E3A02ADCCF5BD96538F437EDED4C (void);
// 0x00000023 System.Void GravityOrbit::.ctor()
extern void GravityOrbit__ctor_mE1D15D8342056DB05CC6E0E267D5B8CB895AE74F (void);
// 0x00000024 System.Void InputManager::Awake()
extern void InputManager_Awake_m56AAFE728DF63B4B922FC31449C82EC6CFB0D946 (void);
// 0x00000025 System.Void InputManager::HandleAllInputs()
extern void InputManager_HandleAllInputs_mCDF44B0217785B2D7D4D6702B18DC531D7B0FC75 (void);
// 0x00000026 System.Void InputManager::ReadInput()
extern void InputManager_ReadInput_mCC0FE5A9B269D19F1A7A3ED6BC6D3969D1076801 (void);
// 0x00000027 System.Void InputManager::HandleMovementInput()
extern void InputManager_HandleMovementInput_mBCE591A731DBF1E30D86801664DFA838CA788D79 (void);
// 0x00000028 System.Void InputManager::HandleSprintingInput()
extern void InputManager_HandleSprintingInput_m7DFEE167DC1AA1463421F97619E8F28438FD8BFB (void);
// 0x00000029 System.Void InputManager::HandleJumpingInput()
extern void InputManager_HandleJumpingInput_m5CBBCEB3CA4F46A490AB6A66A081BA59FF55337D (void);
// 0x0000002A System.Void InputManager::HandleShootInput()
extern void InputManager_HandleShootInput_m3C3B262CA06D3DD5A68CD85FB7C33CA7A5E0A7F0 (void);
// 0x0000002B System.Void InputManager::HandleAimInput()
extern void InputManager_HandleAimInput_mCD4043BE6196AE2B6C067B3F05C25AEFAD928ECC (void);
// 0x0000002C System.Void InputManager::.ctor()
extern void InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7 (void);
// 0x0000002D System.Void LevelManager::Start()
extern void LevelManager_Start_m9B7C3BAF98CDAC3BADCE1790AA5ED55654B41172 (void);
// 0x0000002E System.Void LevelManager::Update()
extern void LevelManager_Update_m559DC5A714E51E73375E147ED363559B97ED514A (void);
// 0x0000002F System.Void LevelManager::OnTriggerEnter(UnityEngine.Collider)
extern void LevelManager_OnTriggerEnter_m2E3BA05ED2BD87F197E46E8CCF79A3B3CED45984 (void);
// 0x00000030 System.Void LevelManager::.ctor()
extern void LevelManager__ctor_mD6FAECFAF24E1996EC8147344018498B20E3DE49 (void);
// 0x00000031 System.Void PlanetOrbit::Start()
extern void PlanetOrbit_Start_m28FAEB2D84FC5E817749F543FE067EF7ECCA2D23 (void);
// 0x00000032 System.Void PlanetOrbit::Update()
extern void PlanetOrbit_Update_m991AA39550E82311387FBD95A91027B9F7903D3D (void);
// 0x00000033 System.Void PlanetOrbit::.ctor()
extern void PlanetOrbit__ctor_m66D19B3300F913664542F845FB6A9E1D08238C1E (void);
// 0x00000034 System.Void PlayerEffectManager::Start()
extern void PlayerEffectManager_Start_m6F21E07EC7AB386EAB625DC100C4BE94F5CCC1A8 (void);
// 0x00000035 System.Void PlayerEffectManager::Update()
extern void PlayerEffectManager_Update_m21A13B26E68B8E926E04D19686ADD39A71554DB2 (void);
// 0x00000036 System.Void PlayerEffectManager::PlayLandEffect()
extern void PlayerEffectManager_PlayLandEffect_m926080EC179C8C53EF8EB69F9BD22B1C3C3174C3 (void);
// 0x00000037 System.Void PlayerEffectManager::ToggleShootingEffects(System.Boolean)
extern void PlayerEffectManager_ToggleShootingEffects_m3091CE05E2C37AF30BF81F8DAA06E40B5A8FCE85 (void);
// 0x00000038 System.Void PlayerEffectManager::.ctor()
extern void PlayerEffectManager__ctor_mAD6A0FCB97903109B780BF412E8EB698F11E9D46 (void);
// 0x00000039 System.Void PlayerLocomotion::Awake()
extern void PlayerLocomotion_Awake_m90E3D43E029E1D3C9F093D5EB4FABD2FF61568C5 (void);
// 0x0000003A System.Void PlayerLocomotion::HandleAllMovement()
extern void PlayerLocomotion_HandleAllMovement_m3AA12244C0EBE67CD8D4A92D077D8058849BD2F1 (void);
// 0x0000003B System.Void PlayerLocomotion::HandleMovement()
extern void PlayerLocomotion_HandleMovement_m7DF7D7FE2A854C2DE4D9C275CFEC16E5432ED84D (void);
// 0x0000003C UnityEngine.Vector3 PlayerLocomotion::ChangeSpeed(UnityEngine.Vector3)
extern void PlayerLocomotion_ChangeSpeed_mEB80118161B66A84693381F16D6BAA3E58B4DC89 (void);
// 0x0000003D System.Void PlayerLocomotion::HandleRotation()
extern void PlayerLocomotion_HandleRotation_mD5676E3D897F7D7943BD5FA06043F9224DCABB50 (void);
// 0x0000003E System.Void PlayerLocomotion::HandleFallingAndLanding()
extern void PlayerLocomotion_HandleFallingAndLanding_m6CE12DB3EA39F414944DE9E69BC427D9B9269F6E (void);
// 0x0000003F System.Void PlayerLocomotion::HandleGravity()
extern void PlayerLocomotion_HandleGravity_m945C350309BD0A0A68A43DA7F145D45FCE41089E (void);
// 0x00000040 System.Void PlayerLocomotion::HandleJump()
extern void PlayerLocomotion_HandleJump_m067C8500AFA029CCFC1A96A37B10A6A05B7FF88C (void);
// 0x00000041 System.Void PlayerLocomotion::HandleShoot()
extern void PlayerLocomotion_HandleShoot_mC851DE55C6118DBCF3AC0CDEC10DAD7DF6A3A23D (void);
// 0x00000042 System.Void PlayerLocomotion::HandleAim()
extern void PlayerLocomotion_HandleAim_m332863775AA4450513DA1775494A88D6F571EA27 (void);
// 0x00000043 System.Void PlayerLocomotion::RotateOnPlanet(UnityEngine.Vector3)
extern void PlayerLocomotion_RotateOnPlanet_m3E3C7A8C083C82F729DDBC075D4F61397E51CB61 (void);
// 0x00000044 System.Collections.IEnumerator PlayerLocomotion::ShootTowards(UnityEngine.Vector3)
extern void PlayerLocomotion_ShootTowards_mB986A2FD3364C113CDDAA80D800C4B812976E6B3 (void);
// 0x00000045 System.Void PlayerLocomotion::.ctor()
extern void PlayerLocomotion__ctor_mA06AA9ED0E94FEAB3ED537367315965BB1D09B5F (void);
// 0x00000046 System.Void PlayerLocomotion/<ShootTowards>d__37::.ctor(System.Int32)
extern void U3CShootTowardsU3Ed__37__ctor_mE9BC491319E93765A987C60044EB75C14949AD81 (void);
// 0x00000047 System.Void PlayerLocomotion/<ShootTowards>d__37::System.IDisposable.Dispose()
extern void U3CShootTowardsU3Ed__37_System_IDisposable_Dispose_m337C2BA4C24F8FD0A81AC2A9DF8A3135D11C8305 (void);
// 0x00000048 System.Boolean PlayerLocomotion/<ShootTowards>d__37::MoveNext()
extern void U3CShootTowardsU3Ed__37_MoveNext_mF5A6C9315BF2D1C229E4010B3E11E3A9D8B4C053 (void);
// 0x00000049 System.Object PlayerLocomotion/<ShootTowards>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootTowardsU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m519592771F59C1DBF16A8D149B9E59D99C841530 (void);
// 0x0000004A System.Void PlayerLocomotion/<ShootTowards>d__37::System.Collections.IEnumerator.Reset()
extern void U3CShootTowardsU3Ed__37_System_Collections_IEnumerator_Reset_mD79DF504A1584917D6E9E7BF7988BCC1FE6670E8 (void);
// 0x0000004B System.Object PlayerLocomotion/<ShootTowards>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CShootTowardsU3Ed__37_System_Collections_IEnumerator_get_Current_m09758056EC4B6852822125181871F7A312EDA594 (void);
// 0x0000004C System.Void PlayerManager::Awake()
extern void PlayerManager_Awake_m4E0FE19C5F34114E28F28F04CCF33C2E34C743E7 (void);
// 0x0000004D System.Void PlayerManager::Update()
extern void PlayerManager_Update_mA0EE0A117D34258508935E958CF715FD930D066A (void);
// 0x0000004E System.Void PlayerManager::FixedUpdate()
extern void PlayerManager_FixedUpdate_mE0318652322E39A7492C925BC16F8B5D92F88788 (void);
// 0x0000004F System.Void PlayerManager::LateUpdate()
extern void PlayerManager_LateUpdate_m88A1B42842248AF2665C754B4FA56F3E25567AA4 (void);
// 0x00000050 System.Void PlayerManager::.ctor()
extern void PlayerManager__ctor_m4C7CA12A8243D6CA73C1EA65B361E7B717070471 (void);
// 0x00000051 System.Void CameraMovement::Start()
extern void CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41 (void);
// 0x00000052 System.Void CameraMovement::Update()
extern void CameraMovement_Update_m02A5D16421ED8C060C06A72144FEA9250C55CE2B (void);
// 0x00000053 System.Void CameraMovement::.ctor()
extern void CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA (void);
// 0x00000054 System.Void Character_control::Start()
extern void Character_control_Start_m97E67AD16B8148D6F8F5DC3C45086B1C922B3CDD (void);
// 0x00000055 System.Void Character_control::Update()
extern void Character_control_Update_m90B2CF7791F02193A8CFFF54B4548C2A8A781D74 (void);
// 0x00000056 System.Void Character_control::.ctor()
extern void Character_control__ctor_mDD12E650AA7D0B7DEF4B99D157D5755D6D6292FD (void);
static Il2CppMethodPointer s_methodPointers[86] = 
{
	Rotatator_get_Randomize_m72AB81666EFF860D4401A6284EF8226FA6225A2D,
	Rotatator_Start_mD617A7EB63C9EDB6AE7D775828E445553FD7718F,
	Rotatator_RandFloat_m2C7FA1F9E460B203C0185389414813034AF1103F,
	Rotatator_FixedUpdate_m812453B2007FE7533F85A583A27C7B642389D38F,
	Rotatator__ctor_m5C149D4546B83274AC5072782FA5A45B26B03059,
	SliderScripts_Start_mB80441D3AF218ACA367485631119961BEF34F55F,
	SliderScripts_FillSlider_m427E4FF54E88039DF974427DED78E5DCC46439C6,
	SliderScripts__ctor_mB77386C370B17B07B48C60928EC809FF4CC4E84E,
	AnimationManager_Awake_m627DE07938651E02F0A44463872756F99CEBE1A0,
	AnimationManager_PlayTargetAnimation_m47BDB6536609430A331D8E9D0FBFCC56F1DD9DEE,
	AnimationManager_UpdateAnimator_m3F09B8D6E2E97AF94A0296FF32F5F23AEF011D94,
	AnimationManager_OnAnimatorMove_mAC9F008B4AD7DF4D2A31F87D4D9044CED4677F00,
	AnimationManager__ctor_m0C4622326DC614221E503860BBCC73F91BFB9F20,
	ResetBool_OnStateEnter_mE8C32A0F0CA360AE9778EE55B323845FD797EE5B,
	ResetBool__ctor_m0063AB38B1D283936F6762CB900B96A554416C2E,
	ResetIsJumping_OnStateExit_mD0A4B7ECB184C90C720789B24D7EAD23DDD460F9,
	ResetIsJumping__ctor_mEF667F0D15C7665F3BBD2AB1BEAE9EAECBCB654E,
	CameraManager_Awake_m9FA8A0FE2F4DE2E70076A4A15C9885BE5E4775A4,
	CameraManager_Start_m3A8F66B836B9A9BC419B62EF88451549BBC9CCD0,
	CameraManager_HandleAllCameraMovement_m58737EA7EFF6EFEECC7F56CB66B7254E5BBB48F7,
	CameraManager_FollowTarget_m3B321F6088D72138EB27285283F8E849613ED10E,
	CameraManager_RotateCamera_m2CDBD3C9BA768CE220369FD4956F27583D5D7066,
	CameraManager_HandleCameraCollisions_mBC0FF6768EBC02EDDE8575444230061EDEC7C2FA,
	CameraManager_HandleShoulderCam_mD187D8FB9D6CD4C27FF02E76F8C1843D1D5E91BB,
	CameraManager_HandleTopDownCam_m9F9BE9182993AABDE5F0B9B0E97E0E42B05EA8C7,
	CameraManager_SwitchCameras_m8ECD34369EB5D0FB36E8AF84C30776B21EB98C05,
	CameraManager_GetCurrentCamera_m69A5FB8797F2209A65B33A8179AD7925F5ECCDD6,
	CameraManager__ctor_m932EDBCBAE89115380AA929ED5BC9823B9371185,
	ChargeManager_Awake_m50D7F099C3A9FDDBAC1ECCCE56FD551D99E1F8B0,
	ChargeManager_UpdateChargeCounter_m55E422F13E6183B524F031B4455E9374F0B8CB8F,
	ChargeManager__ctor_m80FE8C0DE5AA7ECAEC4C03F7195E9C0C947673DE,
	GravityOrbit_FixedUpdate_m6635C617B4B753B7C2762BF0A9A2A048BE0D437C,
	GravityOrbit_OnTriggerEnter_mC4D11D4D6E0E89BD5362D141F6BE80A080060DC1,
	GravityOrbit_OnTriggerExit_m56A083FFF3C8E3A02ADCCF5BD96538F437EDED4C,
	GravityOrbit__ctor_mE1D15D8342056DB05CC6E0E267D5B8CB895AE74F,
	InputManager_Awake_m56AAFE728DF63B4B922FC31449C82EC6CFB0D946,
	InputManager_HandleAllInputs_mCDF44B0217785B2D7D4D6702B18DC531D7B0FC75,
	InputManager_ReadInput_mCC0FE5A9B269D19F1A7A3ED6BC6D3969D1076801,
	InputManager_HandleMovementInput_mBCE591A731DBF1E30D86801664DFA838CA788D79,
	InputManager_HandleSprintingInput_m7DFEE167DC1AA1463421F97619E8F28438FD8BFB,
	InputManager_HandleJumpingInput_m5CBBCEB3CA4F46A490AB6A66A081BA59FF55337D,
	InputManager_HandleShootInput_m3C3B262CA06D3DD5A68CD85FB7C33CA7A5E0A7F0,
	InputManager_HandleAimInput_mCD4043BE6196AE2B6C067B3F05C25AEFAD928ECC,
	InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7,
	LevelManager_Start_m9B7C3BAF98CDAC3BADCE1790AA5ED55654B41172,
	LevelManager_Update_m559DC5A714E51E73375E147ED363559B97ED514A,
	LevelManager_OnTriggerEnter_m2E3BA05ED2BD87F197E46E8CCF79A3B3CED45984,
	LevelManager__ctor_mD6FAECFAF24E1996EC8147344018498B20E3DE49,
	PlanetOrbit_Start_m28FAEB2D84FC5E817749F543FE067EF7ECCA2D23,
	PlanetOrbit_Update_m991AA39550E82311387FBD95A91027B9F7903D3D,
	PlanetOrbit__ctor_m66D19B3300F913664542F845FB6A9E1D08238C1E,
	PlayerEffectManager_Start_m6F21E07EC7AB386EAB625DC100C4BE94F5CCC1A8,
	PlayerEffectManager_Update_m21A13B26E68B8E926E04D19686ADD39A71554DB2,
	PlayerEffectManager_PlayLandEffect_m926080EC179C8C53EF8EB69F9BD22B1C3C3174C3,
	PlayerEffectManager_ToggleShootingEffects_m3091CE05E2C37AF30BF81F8DAA06E40B5A8FCE85,
	PlayerEffectManager__ctor_mAD6A0FCB97903109B780BF412E8EB698F11E9D46,
	PlayerLocomotion_Awake_m90E3D43E029E1D3C9F093D5EB4FABD2FF61568C5,
	PlayerLocomotion_HandleAllMovement_m3AA12244C0EBE67CD8D4A92D077D8058849BD2F1,
	PlayerLocomotion_HandleMovement_m7DF7D7FE2A854C2DE4D9C275CFEC16E5432ED84D,
	PlayerLocomotion_ChangeSpeed_mEB80118161B66A84693381F16D6BAA3E58B4DC89,
	PlayerLocomotion_HandleRotation_mD5676E3D897F7D7943BD5FA06043F9224DCABB50,
	PlayerLocomotion_HandleFallingAndLanding_m6CE12DB3EA39F414944DE9E69BC427D9B9269F6E,
	PlayerLocomotion_HandleGravity_m945C350309BD0A0A68A43DA7F145D45FCE41089E,
	PlayerLocomotion_HandleJump_m067C8500AFA029CCFC1A96A37B10A6A05B7FF88C,
	PlayerLocomotion_HandleShoot_mC851DE55C6118DBCF3AC0CDEC10DAD7DF6A3A23D,
	PlayerLocomotion_HandleAim_m332863775AA4450513DA1775494A88D6F571EA27,
	PlayerLocomotion_RotateOnPlanet_m3E3C7A8C083C82F729DDBC075D4F61397E51CB61,
	PlayerLocomotion_ShootTowards_mB986A2FD3364C113CDDAA80D800C4B812976E6B3,
	PlayerLocomotion__ctor_mA06AA9ED0E94FEAB3ED537367315965BB1D09B5F,
	U3CShootTowardsU3Ed__37__ctor_mE9BC491319E93765A987C60044EB75C14949AD81,
	U3CShootTowardsU3Ed__37_System_IDisposable_Dispose_m337C2BA4C24F8FD0A81AC2A9DF8A3135D11C8305,
	U3CShootTowardsU3Ed__37_MoveNext_mF5A6C9315BF2D1C229E4010B3E11E3A9D8B4C053,
	U3CShootTowardsU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m519592771F59C1DBF16A8D149B9E59D99C841530,
	U3CShootTowardsU3Ed__37_System_Collections_IEnumerator_Reset_mD79DF504A1584917D6E9E7BF7988BCC1FE6670E8,
	U3CShootTowardsU3Ed__37_System_Collections_IEnumerator_get_Current_m09758056EC4B6852822125181871F7A312EDA594,
	PlayerManager_Awake_m4E0FE19C5F34114E28F28F04CCF33C2E34C743E7,
	PlayerManager_Update_mA0EE0A117D34258508935E958CF715FD930D066A,
	PlayerManager_FixedUpdate_mE0318652322E39A7492C925BC16F8B5D92F88788,
	PlayerManager_LateUpdate_m88A1B42842248AF2665C754B4FA56F3E25567AA4,
	PlayerManager__ctor_m4C7CA12A8243D6CA73C1EA65B361E7B717070471,
	CameraMovement_Start_mCBBF91D02D609BDE618205B77F07B1FFD53C0A41,
	CameraMovement_Update_m02A5D16421ED8C060C06A72144FEA9250C55CE2B,
	CameraMovement__ctor_mD0F05084B2475AA8C0A35AFB6E4C88D0D6ACEAAA,
	Character_control_Start_m97E67AD16B8148D6F8F5DC3C45086B1C922B3CDD,
	Character_control_Update_m90B2CF7791F02193A8CFFF54B4548C2A8A781D74,
	Character_control__ctor_mDD12E650AA7D0B7DEF4B99D157D5755D6D6292FD,
};
static const int32_t s_InvokerIndices[86] = 
{
	1694,
	1708,
	1696,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	609,
	961,
	1708,
	1708,
	583,
	1708,
	583,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1440,
	1671,
	1708,
	1708,
	1457,
	1708,
	1708,
	1440,
	1440,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1440,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1457,
	1708,
	1708,
	1708,
	1708,
	1371,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1470,
	1168,
	1708,
	1427,
	1708,
	1694,
	1671,
	1708,
	1671,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
	1708,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	86,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
